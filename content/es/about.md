---
layout: page.pug
title: "Acerca de"
language: es
summary: "Acerca del Kit de Primeros Auxilios Digitales - DFAK"
date: 2023-04
permalink: /es/about/
parent: Home
---
El Kit de Primeros Auxilios Digitales es un esfuerzo colaborativo de la [Red de Respuesta Rápida (RaReNet) (en inglés)](https://www.rarenet.org/) y de [CiviCERT (en inglés)](https://www.civicert.org/).

<iframe src="https://archive.org/embed/dfak-tech-demo" width="640" height="480" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

La Red de Respuesta Rápida es una red internacional de personas que
ofrecen respuesta rápida y organizaciones referencia en seguridad
digital que incluyen a Access Now, Amnesty Tech, Center for Digital
Resilience, CIRCL, EFF, Freedom House, Front Line Defenders, Global
Voices, Greenhost, Hivos y Digital Defenders Partnership, Internews, La
Labomedia, Open Technology Fund, Virtualroad, así como otras personas
colaboradoras expertas que trabajan en este campo.

Algunas de estas organizaciones y particulares son parte de CiviCERT, una red internacional de mesas de soporte en seguridad digital y proveedores de infraestructura que están enfocados principalmente en apoyar a grupos y personas individuales que trabajan por la justicia social y por la defensa de los derechos humanos y digitales. CiviCERT es una figura profesional para los esfuerzos distribuidos de CERTs (Equipos de Respuesta de Emergencias Computacionales o *Computer Emergency Response Team*) en la comunidad. CiviCERT está acreditado por Trusted Introducer, la red europea de equipos de respuesta de emergencia computacional de confianza.

Gracias a [Metamorphosis Foundation (en inglés)](https://metamorphosis.org.mk/en/) por la [traducción albanesa](https://digitalfirstaid.org/sq/) y a [EngageMedia (en inglés)](https://engagemedia.org/) por la [birmana](https://digitalfirstaid.org/my/), [Indonesian](https://digitalfirstaid.org/id/), y [tailandesa](https://digitalfirstaid.org/th/) del Kit de Primeros Auxilios Digitales.

El Kit de Primeros Auxilios Digitales también es un [proyecto de código abierto que acepta contribuciones externas (en inglés)](https://gitlab.com/rarenet/dfak).

Si deseas utilizar el Kit de Primeros Auxilios Digitales en contextos
donde la conectividad a internet es limitada o difícil, puedes descargar
[la versión sin conexión](https://www.digitalfirstaid.org/dfak-offline.zip).

Para cualquier comentario, sugerencia o pregunta sobre el Kit de
Primeros Auxilios Digitales, puedes escribirnos a: dfak @ digitaldefenders . org

Huella PGP: 1759 8496 25C1 56EC 1EB4 1F06 6CC1 888F 5D75 706B

