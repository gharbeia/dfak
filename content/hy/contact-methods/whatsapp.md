---
layout: page
title: WhatsApp
author: mfc
language: hy
summary: Կապ հաստատելու միջոցներ
date: 2018-09
permalink: /hy/contact-methods/whatsapp.md
parent: /hy/
published: true
---

WhatsApp-ը երաշխավորում է, որ ձեր զրույցը հասանելի է միայն ձեզ և հասցեատիրոջը։ Սակայն այն փաստը, որ դուք կապ եք ունեցել հասցեատիրոջ հետ, կարող է տեսանելի լինել նաև իշխանությունների և օրենսդիր մարմինների համար։

Ռեսուրսներ. [Ինչպես օգտվել WhatsApp-ից Android-ով](https://ssd.eff.org/en/module/how-use-whatsapp-android) [Ինչպես օգտվել WhatsApp-ից iOS-ով](https://ssd.eff.org/en/module/how-use-whatsapp-ios)։
