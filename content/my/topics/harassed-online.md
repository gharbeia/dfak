---
layout: page
title: "သင်အွန်လိုင်းနှောင့်ယှက်မှု ပစ်မှတ်ထားခံနေရပါသလား?"
author: Flo Pagano, Natalia Krapiva
language: my
summary: "သင်အွန်လိုင်းနှောင့်ယှက်မှု ပစ်မှတ်ထားခံနေရပါသလား?"
date: 2020-11
permalink: /my/topics/harassed-online/
parent: Home
---

# Are you being targeted by online harassment?

အင်တာနက်နှင့် လူမှုမီဒီယာပလက်ဖောင်းများသည် အရပ်ဘက်လူ့အဖွဲ့အစည်းများ အထူးသဖြင့် အမျိုးသမီးများ၊ LGBTQIA+ ဟု ခံယူသူ များနှင့် အခြားလူနည်းစုများအတွက် မိမိကိုယ်ကို ထုတ်ဖော်ပြောဆိုရန်နှင့် သူတို့၏စကားသံများကို ထုတ်ဖော်ပြောဆိုရန်  နေရာတစ်ခုဖြစ်လာသည်။ သို့သော် တချိန်တည်းမှာပင် ၎င်းတို့၏အမြင်များကို ထုတ်ဖော်ပြောဆိုမှုကြောင့် အလွယ်တကူ ပစ်မှတ်ထား နှောင့်ယှက်ခံရသည့် နေရာများလည်း ဖြစ်လာသည်။ အွန်လိုင်းအကြမ်းဖက်မှုနှင့် အလွဲသုံးစားလုပ်မှု လုပ်ရပ်များသည် အမျိုးသမီးများ၊ LGBTQIA+ ဟု ခံယူသူ များနှင့် အခြားလူနည်းစုများ၏ ကြောက်ရွံ့ခြင်းမရှိဘဲ တန်းတူညီမျှ၍ လွတ်လပ်စွာထုတ်ဖော်ပြောဆိုနိုင်ခွင့်ကို ပိတ်ပင်ချိုးဖောက်ပါသည်။

အွန်လိုင်းအကြမ်းဖက်မှုနှင့် အလွဲသုံးစားမှုများသည်ပုံစံအမျိုးမျိုးရှိနိုင်ပါသည်။ နိုင်ငံအများတွင် နှောင့်ယှက် အနိုင်ကျင့်ခံရသူများအား အကာအကွယ်ပေးသည့် ဥပဒေများမရှိခြင်းကြောင့် တိုက်ခိုက်သူများသည် သူတို့လုပ်ရပ်အတွက် အပြစ်ပေးအရေးယူခြင်းခံရမည်မဟုတ်ဟု ယူဆကြသည်။ အားလုံးအပေါ် ညီတူညီမျှအကာအကွယ်ပေး အရေးယူနိုင်သည့် နည်းဗျူဟာများ မရှိသည့်အတွက် မည်ကဲ့သို့တိုက်ခိုက်မှုများဖြစ်ပွားသည် အပေါ်မူတည်၍ ပြောင်းလဲလုပ်ဆောင်ရသည်များ ရှိပါသည်။ ထို့ကြောင့် မည်ကဲ့သို့အရေးယူဆောင်ရွက်မှုများ ဆက်လက်လုပ်ဆောင်နိုင်မည်ကို ဆုံးဖြတ်နိုင်ရန် သင့်အား ပစ်မှတ်ထားတိုက်ခိုက်မှု အမျိုးအစားကို ဖော်ထုတ်ရန်အရေးကြီးသည်။

ဤ ဒစ်ဂျစ်တယ်ရှေးဦးပြုလမ်းညွှန်သည် သင်ကြုံတွေ့နေရသောတိုက်ခိုက်မှုအမျိုးအစားကို နားလည်နိုင်ရန်၊ မည်ကဲ့သို့ကာကွယ်ရမည်ဟု စီစဉ်နိုင်ရန် အခြေခံအဆင့်အချို့ကို ဖော်ပြထားပြီး သင့်ကို အကူအညီပေးနိုင်သည့် ဒီဂျစ်တယ်လုံခြုံရေး help desk ရှာဖွေရန်အတွက်လည်း ကူညီနိုင်ပါလိမ့်မည်။  

အကယ်၍ သင်သည် အွန်လိုင်းနှောင့်ယှက်မှုဖြင့် ပစ်မှတ်ထားခံနေရပါက [မိမိကိုယ်ကိုပြုစုစောင့်ရှောက်ခြင်း](/../../self-care) နှင့် [တိုက်ခိုက်မှုများကို မှတ်တမ်းတင်ခြင်း](/../../documentation) တို့ကို ဖတ်ကြည့်ရန် အကြံပြုပါသည်။ ထို့နောက် သင့်ပြဿနာ၏ သဘောသဘာဝကိုသိရှိနိုင်ရန်နှင့် ဖြစ်နိုင်သော ဖြေရှင်းနည်းများကိုရှာဖွေရန် ဤမေးခွန်းလွှာအတိုင်း ဆောင်ရွက်ပါ။

## Workflow

### physical-wellbeing

သင်၏ ရုပ်ပိုင်းဆိုင်ရာ သို့မဟုတ် စိတ်ပိုင်းဆိုင်ရာလုံခြုံရေး နှင့်ပတ်သတ်ပြီး ‌ကြောက်ရွံ့နေပါသလား?

- [ကြောက်ရွံ့ပါသည်](#physical-risk_end)
- [မကြောက်ရွံ့ပါ](#no-physical-risk)

### location

တိုက်ခိုက်သူသည် သင်ရှိနေသော နေရာကို သိထားပုံပေါ်ပါသလား?

- [သိထားပုံပေါ်သည်](#location_tips)
- [သိထားပုံမပေါ်ပါ](#device)

### location_tips

> သင့် လူမှုကွန်ယက်မီဒီယာပေါ်တွင် တင်ထားသော ပို့စ်များကို ပြန်ကြည့်၍ သင်ရှိနေသောနေရာကို ဖော်ပြထားသလား စစ်ဆေးပါ။ ဖော်ပြထားသည်ဆိုပါက သင် ပို့စ်အသစ်တင်တိုင်း သင့်နေရာကို ဖော်ပြခြင်းမပြုစေရန် ဖုန်းပေါ်ရှိ လူမှုကွန်ယက်မီဒီယာ အက်ပလီကေးရှင်းများနှင့် အခြားဝန်ဆောင်မှုများ၏ တည်နေရာပြ GPS အသုံးပြုခွင့်ကို ပိတ်လိုက်ပါ။
>
> သင် အွန်လိုင်းပေါ်တွင် တင်သည့် ဓာတ်ပုံများကို ပြန်ကြည့်၍ သင်ရောက်ရှိနေသောနေရာကို ကွက်ကွက်ကွင်းကွင်း မြင်သာစေမည့် အချက်အလက်များ ပါမပါ စစ်ဆေးပါ။ သင့်ကို ခြေရာခံလိုက်သူများရန်မှ ရှောင်ရှားရန်အတွက် ဓာတ်ပုံများ၊ ဗီဒီရိုများ တင်သည့်အချိန်တွင် နေရာအတိအကျမဖော်ပြမိစေရန် သတိပြုပါ။
>
> ပိုပြီးလုံခြုံစေရန်အတွက် မြေပုံပေါ်တွင် မိမိဘယ်နေရာရောက်နေသလဲဆိုသည်ကို သိရန်လိုသည့် အချိန်တခဏမှလွဲ၍ ကျန်သည့်အချိန်များတွင် GPS ကို ပိတ်ထားရန် အကြံပြုပါသည်။

တိုက်ခိုက်သူသည် သင်အွန်လိုင်းပေါ်တင်လိုက်သည့် အချက်အလက်များကို အသုံးပြု၍ သင့်နောက်လိုက်လာခဲ့သည်ဟု ထင်ပါသလား? သို့မဟုတ် တိုက်ခိုက်သူသည် တစ်ခြားနည်းဖြင့် သင့်တည်နေရာကို ရှာဖွေတွေ့ရှိထားသည်ဟု ထင်ပါသလား?

- [မထင်ပါ။ ပြဿနာပြေလည်သွားပါပြီ](#resolved_end)
- [ထင်ပါသည်။ တိုက်ခိုက်သူသည် ကျွန်ုပ်၏တည်နေရာကို သိရှိနေသည်ဟု ထင်ပါသေးသည်](#device)
- [မထင်ပါ။ ဒါပေမယ့် တစ်ခြားပြဿနာများ ကြုံတွေ့နေရပါသည်](#account)

### device

တိုက်ခိုက်သူသည် သင်၏ စက်ကိရိယာကို ဝင်ရောက်အသုံးပြုနေသည်ဟု ထင်ပါသလား?

 - [ထင်ပါသည်](#device-compromised)
 - [မထင်ပါ](#account-compromised)


### device-compromised

> သင်စက်သို့ဝင်ရောက်သည့် စကားဝှက်ကို ရှည်လျားပြီး ရှုပ်ထွေး ဆန်းပြားသော စကားဝှက်အသစ်ဖြင့် အစားထိုးပြောင်းပါ။
>
> - [Mac OS](https://support.apple.com/en-us/HT202860)
> - [Windows](https://support.microsoft.com/en-us/help/4490115/windows-change-or-reset-your-password)
> - [iOS - Apple ID](https://support.apple.com/en-us/HT201355)
> - [Android](https://support.google.com/accounts/answer/41078?co=GENIE.Platform%3DAndroid&hl=en)

တိုက်ခိုက်သူသည် သင့်စက်ကို ထိန်းချုပ်ထားဆဲဟု ထင်ပါသလား?

- [မထင်ပါ။ ပြဿနာပြေလည်သွားပါပြီ](#resolved_end)
- [မထင်ပါ။ ဒါပေမယ့် တစ်ခြားပြဿနာများ ကြုံတွေ့နေရပါသည်](#account)
- [မထင်ပါ](#info_stalkerware)

### info_stalkerware

> [Stalkerware](https://en.wikipedia.org/wiki/Stalkerware) ဆိုသည်မှာ လူတစ်ဦးတစ်ယောက်ကို ခြေရာခံလိုက်ရန် သို့မဟုတ် ထိန်းချုပ်ရန် ရည်ရွယ်ချက်ဖြင့် ၎င်း၏ လုပ်ငန်းဆောင်တာများ၊ တည်နေရာများကို စောင့်ကြည့်ထောက်လှမ်းနိုင်သည့် ဆော့ဝဲကို ဆိုလိုပါသည်။

အကယ်၍ တစ်စုံတစ်ယောက်သည် သင့်မိုဘိုင်းစက်ပေါ်တွင် အက်ပလီကေးရှင်းတစ်ခုကို တပ်ဆင်၍ သင့်ကို စုံစမ်းထောက်လှမ်းနေသည်ဟု သံသယရှိပါက [ဤဒီဂျစ်တယ်ရှေးဦးသူနာပြုလမ်းညွှန် လုပ်ငန်းစဉ်သည် သင့်စက်တွင် သူခိုးဆော့ဝဲ တပ်ဆင်ထားခြင်း ရှိမရှိနှင့် မည်သို့ ရှင်းလင်းရမည်ဆိုသည်ကို ဆုံးဖြတ်ရန် ကူညီနိုင်ပါလိမ့်မည်](../../../device-acting-suspiciously)

သင်ကြုံတွေ့နေရသော တိုက်ခိုက်မှုများကို ပိုမိုနားလည်နိုင်ရန် အကူအညီထပ်မံလိုအပ်ပါသေးလား?

- [မလိုအပ်ပါ။ ပြဿနာပြေလည်သွားပါပြီ](#resolved_end)
- [လိုပါသည်။ ဖြေရှင်းလိုသော ပြဿနာများ ရှိပါသေးသည်](#account)

### account

> တစ်စုံတစ်ယောက်သည် သင့်စက်ကိရိယာကို ဝင်ရောက်အသုံးပြုနိုင်သည်ဖြစ်စေ၊ အသုံးမပြုနိုင်သည်ဖြစ်စေ၊ သင့်အွန်လိုင်း အကောင့်များကို hack လုပ်ခြင်း၊ သင့်စကားဝှက်ကို ခိုးယူအသုံးပြုခြင်းတို့ဖြင့် သင့်အွန်လိုင်းအကောင့်များကို ဝင်ရောက် အသုံးပြုနိုင်ပါသည်။
>
> တစ်ယောက်ယောက်သည် သင့်အွန်လိုင်းအကောင့်များကို ဝင်ရောက်အသုံးပြုခွင့် ရသွားသည်ဆိုလျှင် သင့် ကိုယ်ရေးကိုယ်တာ ပေးပို့ထားသော စာများကို ဖတ်ရှုခြင်း၊ သင့်အဆက်အသွယ်များကို ရရှိခြင်း၊ သင့်ကိုယ်ရေးကိုယ်တာ ပို့စ်များ၊ ဓါတ်ပုံများ၊ ဗီဒီယိုများကို ထုတ်လွှင့်ခြင်း၊ သင်ကဲ့သို့ ဟန်ဆောင်ခြင်းတို့ ပြုလုပ်နိုင်ပါလိမ့်မည်။
>
> သင့်အွန်လိုင်းအကောင့် လုပ်ငန်းဆောင်တာများနှင့် (Sent နှင့် Trash folder များအပါတွင်) စာပေးပို့လက်ခံရရှိမှုတို့ကို စစ်ဆေး၍ သံသယရှိဖွယ် ကိစ္စရပ်များကို ရှာဖွေဖော်ထုတ်ပါ။

သင့်အကောင့်လုံခြုံရေး ကျိုးပေါက်နေနိုင်သည်ဟု ယူဆလို့ရနိုင်သော သံသယရှိဖွယ် လုပ်ငန်းဆောင်တာများ၊ ဥပမာ ပို့စ်များ၊ စာတိုများ မိမိမသိရှိလိုက်ဘဲ ပျောက်သွားခြင်းတို့ ဖြစ်ပွားသည်ကို သတိထားမိပါသလား?

- [သတိထားမိပါသည်](#account-compromised)
- [သတိမထားမိပါ](#private-contact)


### change passwords

> သင့်အွန်လိုင်းအကောင့်တစ်ခုချင်းစီတွင် လုံခြုံရေးအားကောင်းခိုင်ခံ့၍ တစ်ခုနှင့်တစ်ခုမတူညီသော စကားဝှက်များဖြင့် ပြောင်းလဲအသုံးပြုပါ။
>
> စကားဝှက်များ မည်ကဲ့သို့ ဖန်တီး၍ စီမံခန့်ခွဲရမည်ဆိုသည်ကို [Surveillance Self-Defense guide](https://ssd.eff.org/module/creating-strong-passwords) တွင် လေ့လာနိုင်ပါသည်။
>
> သင့်အွန်လိုင်းအကောင့်များ ပိုမိုလုံခြုံစေရန်အတွက် အဆင့်နှစ်ဆင့်ဖြင့် စစ်မှန်ကြောင်း သက်သေပြခြင်း (2FA) ကို ဖြစ်နိုင်သရွေ့ အသုံးပြုသင့်ပါသည်။  
>
> အဆင့်နှစ်ဆင့်ဖြင့် စစ်မှန်ကြောင်း သက်သေပြခြင်း (2FA) ကို မည်ကဲ့သို့ အသုံးပြုရမည်ကို သိရှိလိုပါက [Surveillance Self-Defense guide](https://ssd.eff.org/module/how-enable-two-factor-authentication) တွင် ဆက်လက်လေ့လာပါ။

သင့်အကောင့်ကို အခြားတစ်ယောက်က ဝင်ရောက်အသုံးပြုနေသေးသည်ဟု ထင်နေပါသေးလား?

- [ထင်ပါသည်](#hacked-account)
- [မထင်ပါ](#private-contact)


### hacked-account

> သင့်အကောင့်ထဲ ဝင်မရဘူးဆိုပါက သင့်အကောင့်ကို အခြားတစ်ယောက်က hack လုပ်၍ သင့်စကားဝှက် ကိုပြောင်းလိုက်ခြင်းကြောင့် ဖြစ်ကောင်းဖြစ်နိုင်သည်။

သင့်အကောင့်ကို hack အလုပ်ခံထားရသည်ဟု ထင်ပါက ဒီဂျစ်တယ်ရှေးဦးသူနာပြုလမ်းညွှန် လုပ်ငန်းစဉ်ကို အသုံးပြု၍ ပြဿနာများကို ဖြေရှင်းကြည့်ပါ။

 - [ပြဿနာများ ဖြေရှင်းရန် လုပ်ငန်းစဉ်ကို ဆက်သွားလိုပါသည်](../../../account-access-issues)
 - [ပြဿနာပြေလည်သွားပါပြီ](#resolved_end)
 - [အကောင့်တွင် ပြဿနာမရှိတော့ပေမယ့်လည်း အခြားပြဿနာများ ရှိနေပါသည်](#private-contact)


### private-contact

သင်မလိုချင်သော ဖုန်းခေါ်ဆိုမှုများ၊ စာတိုများကို စာတိုပေးပို့မှုအက်ပလီကေးရှင်းများမှ လက်ခံရရှိနေပါသလား?

 - [ရရှိပါသည်](#change_contact)
 - [မရရှိပါ](#threats)

### change_contact

> သင်မလိုချင်သော ဖုန်းခေါ်ဆိုမှုများ၊ SMS စာတိုများနှင့် စာတိုများကို သင့်ဖုန်းနံပါတ်၊ အီးမေးလ်၊ သို့မဟုတ် အခြား ဆက်သွယ်ရေးနည်းလမ်းများဖြင့် ချိတ်ဆက်ထားသော အက်ပလီကေးရှင်းများပေါ်တွင် လက်ခံရရှိနေသည် ဆိုပါက ယင်းအကောင့်နှင့် ချိတ်ဆက်ထားသည့် ဖုန်းနံပါတ်၊ SIM ကဒ်၊ အီးမေးလ်၊ နှင့် အခြားဆက်သွယ်ရေးနည်းလမ်းများကို အသစ်ပြောင်းလဲ အသုံးပြုနိုင်ပါသည်။  
>
> သင်မလိုချင်သော စာများနှင့် စာပေးပို့သည့် အကောင့်တို့ကို သက်ဆိုင်ရာ ပလက်ဖောင်းသို့ သတင်းပေးပို့၍ ပိတ်ပင်ရန်လည်း စဉ်းစားသင့်ပါသည်။

သင်မလိုချင်သော ဖုန်းခေါ်ဆိုမှုများ၊ စာတိုများ ရပ်တန့်သွားပါပြီလား?

 - [ရပ်တန့်သွားပါပြီ](#legal)
 - [မရပ်သေးပါ။ အခြားပြဿနာများ ရှိပါသေးသည်](#threats)
 - [မရပ်သေးပါ။ အကူအညီလိုအပ်ပါသည်](#harassment_end)

### threats

သင်သည် အီးမေးလ် သို့မဟုတ် လူမှုမီဒီယာအကောင့်များမှတစ်ဆင့်  စာပေးပို့၍ ခြိမ်းခြောက် အကြပ်ကိုင်မှုများ ကြုံတွေ့နေရပါသလား?

  - [ကြုံတွေ့နေရပါသည်](#threats_tips)
  - [မကြုံတွေ့ရပါ](#impersonation)

### threats_tips

> ရုပ်ပိုင်းဆိုင်ရာ၊ လိင်ပိုင်းဆိုင်ရာ အကြမ်းဖက်ရန် ခြိမ်းခြောက်မှုများ၊ အကြပ်ကိုင်ငွေညှစ်မှုများ အပါအဝင် ခြိမ်းခြောက်မှုများ ပါသည့် စာများကို လက်ခံရရှိနေသည်ဆိုပါက ဖြစ်ပွားခဲ့သည့် ကိစ္စမှန်သမျှ - လက်ခံရရှိသည့် လင့်ခ်များ၊ စကရင်ဓာတ်ပုံများကို အထောက်အထားအဖြစ်သိမ်းဆည်းခြင်း၊ ခြိမ်းခြောက်သူကို သက်ဆိုင်ရာ ပလက်ဖောင်း သို့မဟုတ် ဝန်ဆောင်မှုထံ တိုင်ကြားခြင်း၊ တိုက်ခိုက်သူကို မိမိအကောင့်တွင် ပိတ်ပစ်ခြင်း စသည်ဖြင့် - ကို တတ်နိုင်သမျှ မှတ်တမ်းအဖြစ် မှတ်သားထားသင့်ပါသည်။

ခြိမ်းခြောက်မှုများကို ရပ်တန့်နိုင်ခဲ့ပါသလား?

   - [ရပ်တန့်နိုင်ခဲ့ပါသည်](#legal)
   - [ရပ်တန့်နိုင်ခဲ့ပါသည်။ သို့ရာတွင် အခြားပြဿနာများ  ရှိနေပါသေးသည်](#impersonation)
   - [မရပ်တန့်နိုင်ပါ။ တရားရေးအရ ဆောင်ရွက်နိုင်ရန် အကူအညီလိုပါသည်](#legal-warning)

### impersonation

> သင်ကြုံတွေ့နိုင်သည့် အခြားသော နှောက်ယှက်မှုတစ်ခုမှာ တစ်စုံတစ်ယောက်က သင့်အယောင်ဆောင်ခြင်း ဖြစ်သည်။
>
> ဥပမာ တစ်စုံတစ်ယောက်သည် သင့်နာမည်အသုံးပြု၍ အကောင့်တစ်ခုဖန်တီးပြီး လူမှုမီဒီယာပလက်ဖောင်းများမှတစ်ဆင့် စာတိုများ ပေးပို့ခြင်း၊ အခြားသူများကို အများမြင်သာစွာ တိုက်ခိုက်ပြောဆိုခြင်း၊ မမှန်ကန်သော အချက်အလက်များနှင့် အမုန်းစကားများ ဖြန့်ဝေခြင်း၊ သင် သို့မဟုတ် သင့်အဖွဲ့အစည်း၊ သင်နှင့်ရင်းနှီးသူများ၏ ဂုဏ်သိက္ခာနှင့် လုံခြုံရေးကို ထိခိုက်စေနိုင်မည့် အပြုအမူများ ဆောင်ရွက်ခြင်းတို့ ဖြစ်ပေါ်နိုင်ပါသည်။
>
> အကယ်၍ တစ်စုံတစ်ယောက်က သင် သို့မဟုတ် သင့်အဖွဲ့အစည်းဟန်ဆောင်ထားသည်ဟု ယုံကြည်ပါက ဤ ဒစ်ဂျစ်တယ်ရှေးဦးသူနာပြု လုပ်ငန်းစဉ်အတိုင်း လိုက်ပါလုပ်ဆောင်ပါ။ အယောင်ဆောင်မှု ပုံစံအလိုက် ဖြေရှင်းရန် အကောင်းဆုံးနည်းလမ်းကို ရှာဖွေရန် လမ်းညွှန်ပေးပါလိမ့်မည်။  

သင်မည်ကဲ့သို့ ဆက်လက်လုပ်ဆောင်ချင်ပါသနည်း?

 - [အခြားတစ်ယောက်မှ မိမိအယောင်ဆောင်မှုကို ကြုံတွေ့နေရ၍ ဖြေရှင်းရန်နည်းလမ်းရှာလိုပါသည်](../../../impersonated)
 - [အခြားနည်းလမ်းဖြင့် နှောက်ယှက်မှု ကြုံတွေ့နေရပါသည်](#defamation)

### defamation

 တစ်စုံတစ်ယောက်က သင့်ဂုဏ်သိက္ခာကို ထိခိုက်စေရန် ရည်ရွယ်၍ သတင်းမှားများ လိုက်ဖြန့်နေပါသလား?  
  - [သတင်းမှားများ ဖြန့်နေပါသည်](#defamation-yes)
  - [အခြားနည်းလမ်းဖြင့် နှောက်ယှက်မှု ကြုံတွေ့နေရပါသည်](#doxing)

### defamation-yes

> အသရေဖျက်မှုဆိုသည်မှာ တစ်စုံတစ်ယောက်၏ သတင်းဂုဏ်သိက္ခာ ထိခိုက်စေရန် ရေးသားပြောဆိုမှု ပြုလုပ်ခြင်းဟု ယေဘုယျအားဖြင့် အဓိပ္ပါယ်ဖွင့်ဆိုနိုင်ပါသည်။ အသရေဖျက်မှုသည် စကားပြော (slander) သို့မဟုတ် စာ (libel) နှစ်မျိုးစလုံးဖြင့် ဖြစ်နိုင်ပါသည်။ နိုင်ငံအလိုက် ဥပဒေများပေါ်မူတည်၍ တရားရေးအရ အရေးယူနိုင်ရန် အသရေဖျက်မှုကို အဓိပ္ပါယ်သတ်မှတ်ပုံ ကွဲလွဲနိုင်ပါသည်။ ဥပမာ လွတ်လပ်စွာဖော်ထုတ်ပြောဆိုပိုင်ခွင့်ကို အလေးထားကာကွယ်သည့် နိုင်ငံများတွင် မီဒီယာနှင့် လူပုဂ္ဂိုလ်များအနေဖြင့် ရာထူးကြီး အရာရှိများနှင့် ပတ်သက်၍ အရှက်ရစေမည့် အကြောင်းအရာများ၊ ထိခိုက်စေမည့် အကြောင်းအရာများကို ဖော်ထုတ်ပြောဆိုရာတွင် သတင်းအချက်အလက်များ မှန်ကန်သည်ဟု ၎င်းတို့အနေဖြင့် ယုံကြည်သရွေ့ လွတ်လပ်စွာဖော်ထုတ်ပြောဆိုခွင့်ပေးထားသည်။ အခြားနိုင်ငံများတွင်တော့ သင်မကြိုက်သည့် အကြောင်းအရာများ ဖြန့်ဝေသည်နှင့် လွယ်လွယ်ကူကူ တရားစွဲဆိုပိုင်ခွင့် ပေးထားနိုင်သည်။
>
> တစ်စုံတစ်ယောက်သည် သင် သို့မဟုတ် သင့်အဖွဲ့အစည်း၏ ဂုဏ်သိက္ခာ ထိခိုက်စေရန် ကြိုးစားနေသည်ဆိုပါက  ဒစ်ဂျစ်တယ်ရှေးဦးသူနာပြု လမ်းညွှန်ပါ လုပ်ငန်းစဉ်ကို လိုက်ပါလုပ်ဆောင်ပါ။ အသရေဖျက်မှု ကြိုးပမ်းမှုများကို ကိုင်တွယ်ဖြေရှင်းရန် နည်းဗျူဟာများ လမ်းညွှန်ပြသပေးပါလိမ့်မည်။


သင်မည်ကဲ့သို့ ဆက်လက်လုပ်ဆောင်ချင်ပါသနည်း?

   - [အသရေဖျက်မှု ကြိုးပမ်းမှုများကို ကိုင်တွယ်ဖြေရှင်းရန် နည်းဗျူဟာ ရှာဖွေလိုပါသည်](../../../defamation)
   - [အခြားနည်းလမ်းဖြင့် နှောင့်ယှက်မှု ကြုံတွေ့နေရပါသည်](#doxing)

### doxing

တစ်စုံတစ်ယောက်က သင့်ရဲ့ခွင့်ပြုချက်မပါဘဲ ပုဂ္ဂလိကသတင်းအချက်အလက် သို့မဟုတ် ဓာတ်ပုံများကို‌ဝေမျှခဲ့ပါသလား?

- [ဝေမျှခဲပါသည်။](#doxing-yes)
- [မဝေမျှခဲ့ပါ](#hate-speech)

### doxing-yes

> တစ်စုံတစ်ယောက်သည် သင်၏ ကိုယ်ရေးကိုယ်တာ အချက်အလက်များကို ဝေမျှခဲ့သည် သို့မဟုတ် သင့်အကြောင်းပါသော ဗီဒီရိုများ၊ ဓာတ်ပုံများ၊ အခြားမီဒီယာအမျိုးအစားများကို ဖြန့်ဝေနေသည်ဆိုပါက ဖြစ်ပွားနေသည့် ကိစ္စများကို ပိုမိုနားလည်ရန်နှင့် တိုက်ခိုက်မှုကို ပြန်လည်တုံ့ပြန်ရာတွင် အထောက်အကူပြုနိုင်ရန် ဒစ်ဂျစ်တယ်ရှေးဦးသူနာပြု လမ်းညွှန်ပါ လုပ်ငန်းစဉ်ကို လိုက်ပါလုပ်ဆောင်ပါ။

သင်မည်ကဲ့သို့ ဆက်လက်လုပ်ဆောင်ချင်ပါသနည်း?

 - [ဖြစ်ပွားနေသည့်ကိစ္စများနှင့် မည်ကဲ့သို့တုံ့ပြန်နိုင်မည်ဆိုသည်ကို ပိုမိုနားလည်လိုပါသည်](../../../doxing)
 - [အကူအညီရယူလိုပါသည်](#harassment_end)

### hate-speech

တစ်စုံတစ်ယောက်သည် သင့် လူမျိုး၊ ကျားမဖြစ်တည်မှု၊ ဘာသာရေးသက်ဝင်ယုံကြည်မှု စသည့် ဖြစ်တည်မှုဆိုင်ရာ ကိစ္စများအပေါ်အခြေခံ၍ အမုန်းစကားဖြန့်ဝေမှု လုပ်ဆောင်နေပါသလား?

 - [လုပ်ဆောင်နေပါသည်](#one-more-persons)
 - [မလုပ်ဆောင်ပါ](#harassment_end)


### one-more-persons

သင့်ကို တိုက်ခိုက်သူသည် လူတစ်ဦးတည်းဖြစ်ပါသလား? လူတစ်ဦးထက်ပိုပါသလား?

- [လူတစ်ဦးသာဖြစ်သည်](#one-person)
- [လူတစ်ဦးထက်ပိုသည်](#more-persons)

### one-person

> အမုန်းစကားသည် လူတစ်ဦးတည်းထံမှလာသည်ဆိုပါက တိုက်ခိုက်မှုကို ထိန်းချုပ်ရန်နှင့် သင့်ထံ အမုန်းစကားမှား ပေးပို့မှု ရပ်တန့်သွားစေရန် အလွယ်ကူဆုံးနှင့် အမြန်ဆုံး ဖြေရှင်းနည်းသည် ၎င်းပုဂ္ဂိုလ်အား တိုင်ကြားသတင်းပို့၍ ပိတ်ပစ်ခြင်း ဖြစ်သည်။ သို့ရာတွင် သုံးစွဲသူကို မိမိအကောင့်မှ ပိတ်ပစ်လိုက်သည်ဆိုပါက ၎င်းပေးပို့သည့် အကြောင်းအရာများကို ထပ်မံ ဝင်ရောက်ကြည့်ရှု၍ သက်သေအထောက်အထား မှတ်ယူနိုင်တော့မည်မဟုတ်ပါ။ ထို့ကြောင့် မပိတ်ခင် [ဒီဂျစ်တယ်တိုက်ခိုက်မှုများကို သက်သေအထောက်အထားရယူရန် အကြံပြုချက်များ](/../../documentation) ကို အရင်လေ့လာပါ။
>
> သင်သည် နှောင့်ယှက်သူကို လူကိုယ်တိုင် သိသည်ဖြစ်စေ၊ မသိသည်ဖြစ်စေ လူမှုကွန်ယက်ပလက်ဖောင်းများပေါ်တွင် တတ်နိုင်သမျှ ပိတ်ပစ်ခြင်းက အကောင်းဆုံး ဖြစ်ပါလိမ့်မည်။
>
> - [Facebook](https://www.facebook.com/help/168009843260943)
> - [Google](https://support.google.com/accounts/answer/6388749?co=GENIE.Platform%3DDesktop&hl=en)
> - [Instagram](https://help.instagram.com/426700567389543)
> - [TikTok](https://support.tiktok.com/en/using-tiktok/followers-and-following/blocking-the-users)
> - [Twitter](https://help.twitter.com/en/using-twitter/blocking-and-unblocking-accounts)
> - [Whatsapp](https://faq.whatsapp.com/1142481766359885/?cms_platform=android)

နှောင့်ယှက်သူကို ထိထိရောက်ရောက် ပိတ်ဆို့လိုက်ပါပြီလား?

- [ပိတ်လိုက်ပါပြီ](#legal)
- [မပိတ်နိုင်ပါ](#campaign)


### legal

> သင့်အားနှောင့်ယှက်သူသည် မည်သူမည်ဝါဖြစ်သည်ဆိုသည်ကို သင်သိပါက သင်ရောက်ရှိနေသည့် အခြေအနေပေါ်မူတည်၍ သက်ဆိုင်ရာ တိုင်းပြည်ရှိ အာဏာပိုင်များထံ သတင်းပေးတိုင်ကြားနိုင်ပါသည်။ နိုင်ငံတိုင်းတွင် အွန်လိုင်းနှောင့်ယှက်မှုခံရသူများကို ကာကွယ်သည့် ဥပဒေများသည် ကွဲပြားသည်ဖြစ်ရာ သင့်တိုင်းပြည်၏ ဥပဒေအကြောင်း ပိုမိုလေ့လာခြင်း၊ ဆက်လက်အရေးယူဆောင်ရွက်မှုများ ဆောင်ရွက်ရန် ဥပဒေရေးရာ အကြံဉာဏ်တောင်းခံခြင်းတို့ လုပ်ဆောင်နိုင်ပါသည်။
>
> သင့်အားနှောင့်ယှက်သူသည် မည်သူမည်ဝါဖြစ်သည်ဆိုသည်ကို မသိပါက အချို့သောအခြေအနေများတွင် တိုက်ခိုက်သူချန်ခဲ့သည့် သဲလွန်စများကို စုံစမ်းစစ်ဆေး၍ တိုက်ခိုက်သူ မည်သူမည်ဝါဖြစ်သည်ကို ဖော်ထုတ်နိုင်ပါသည်။
>
> မည်သို့ပင်ဖြစ်စေ တရားရေးအရ အရေးယူဆောင်ရွက်ရန် စဉ်းစားနေပါက တိုက်ခိုက်မှုဆိုင်ရာ သက်သေအထောက်အထားများကို သေချာစွာ သိမ်းဆည်းထားခြင်းသည် အလွန်အရေးကြီးပါလိမ့်မည်။ ထို့ကြောင့် [ဒီဂျစ်တယ်ရှေးဦးသူနာပြုလမ်းညွှန် စာမျက်နှာရှိ တိုက်ခိုက်မှုဆိုင်ရာ သတင်းအချက်အလက်များကို မှတ်တမ်းတင်မှတ်သားခြင်းဆိုင်ရာ အကြံပြုချက်များ](/../../documentation) ကို အလေးထားလိုက်နာရန် တိုက်တွန်းပါသည်။

သင်မည်ကဲ့သို့ ဆက်လက်လုပ်ဆောင်ချင်ပါသနည်း?

 - [တိုက်ခိုက်သူအား တရားစွဲဆိုရန် ဥပဒေရေးရာ အကူအညီရယူလိုပါသည်](#legal_end)
 - [ပြဿနာပြေလည်သွားပါပြီ](#resolved_end)


### campaign

> အကယ်၍ သင်သည် လူတစ်ဦးထက်ပိုသော လူများ၏တိုက်ခိုက်မှုခံနေရပါက အမုန်းစကား သို့မဟုတ် အနှောင့်အယှက်ပေးမှု လှုပ်ရှားမှုများ၏ပစ်မှတ်ဖြစ်နေနိုင်သည်။ သင်ကြုံတွေ့နေရသည့် ကိစ္စနှင့် ပတ်သက်၍ အကောင်းဆုံးနည်းဗျူဟာက ဘာလဲဆိုသည်ကို သင်စဉ်းစားရန်လိုအပ်သည်။
>
> ဖြစ်နိုင်ချေရှိသော နည်းဗျူဟာများကို လေ့လာရန်အတွက် [Take Back The Tech ၏ အမုန်းစကား ဆန့်ကျင်ကာကွယ်ရေး စာမျက်နှာ](https://www.takebackthetech.net/be-safe/hate-speech-strategies) တွင် ဆက်၍လေ့လာပါ။

သင့်အတွက် အကောင်းဆုံးနည်းဗျူဟာကို ရှာဖွေပြီးပြီလား?

 - [ရှာဖွေပြီးပါပြီ](#resolved_end)
 - [ရှာမတွေ့ပါ။ ဆက်လက်ဖြေရှင်းလိုပါသည်](#harassment_end)
 - [ရှာမတွေ့ပါ။ ဥပဒေရေးရာ အကူအညီရယူလိုပါသည်](#legal_end)

### harassment_end

> သင့်အနေဖြင့် နှောင့်ယှက်မှုကို ဆက်လက်ကြုံတွေ့နေရ၍ သင့်ပြဿနာနှင့် ကိုက်ညီသော ဖြေရှင်းမှုများ လိုအပ်ပါက အောက်ဖော်ပြပါအဖွဲ့အစည်းများနှင့်ဆက်သွယ်၍ အကူအညီရယူပါ။

:[](organisations?services=harassment)


### physical-risk_end

> အကယ်၍ သင့်အပေါ် ကိုယ်ထိလက်ရောက်အန္တရာယ်ကျရောက်နိုင်ခြေရှိ၍ အရေးပေါ်အကူအညီလိုအပ်ပါက အောက်ဖော်ပြပါအဖွဲ့အစည်းများနှင့် ဆက်သွယ်၍ အကူအညီရယူပါ။

:[](organisations?services=physical_security)


### legal_end

> သင့်အနေဖြင့် ဥပဒေရေးရာအထောက်အပံ့လိုအပ်လျှင် ကျေးဇူးပြု၍ အောက်ပါအဖွဲ့အစည်းများနှင့်ဆက်သွယ်ပါ။
>
> တရားရေးအရ အရေးယူဆောင်ရွက်ရန် စဉ်းစားနေပါက တိုက်ခိုက်မှုဆိုင်ရာ သက်သေအထောက်အထားများကို သေချာစွာ သိမ်းဆည်းထားခြင်းသည် အလွန်အရေးကြီးပါလိမ့်မည်။ ထို့ကြောင့် [ဒီဂျစ်တယ်ရှေးဦးသူနာပြုလမ်းညွှန် စာမျက်နှာရှိ တိုက်ခိုက်မှုဆိုင်ရာ သတင်းအချက်အလက်များကို မှတ်တမ်းတင်မှတ်သားခြင်းဆိုင်ရာ အကြံပြုချက်များ](/../../documentation) ကို အလေးထားလိုက်နာရန် တိုက်တွန်းပါသည်။

:[](organisations?services=legal)

### resolved_end

ယခု လမ်းညွှန်ချက်သည် သင့်အတွက် အသုံးဝင်မည်ဟုမျှော်လင့်ပါသည်။ ဝေဖန်အကြံပြုချက်များကို [အီးမေးလ်မှတဆင့်](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com) ပေးပို့နိုင်ပါသည်။

### final_tips

- **နှောင့်ယှက်မှုများကို မှတ်တမ်းတင်ပါ။** သင်ကြုံတွေ့ရသော တိုက်ခိုက်မှုများ၊ အခြားဖြစ်စဉ်များကို မှတ်တမ်းတင်ခြင်းသည် အလွန်အထောက်အကူပြုပါသည်။ Screenshots များရိုက်ယူခြင်း၊ နှောင့်ယှက်သူများမှ ပေးပို့သည့် စာများကိုသိမ်းဆည်းခြင်း လုပ်ဆောင်သင့်ပါသည်။ ဖြစ်နိုင်လျှင် ဤမှတ်တမ်းမှတ်ရာများကို ရက်စွဲ၊ အချိန်၊ ပလက်ဖောင်း၊ URL၊ သုံးစွဲသူ ID၊ screenshots များ၊ ဖြစ်ပွားခဲ့သည်အခြင်းအရာ စသည်ဖြင့် တိတိကျကျမှတ်ထားနိုင်မည့်  ဂျာနယ်တစ်ခုဖန်တီးပါ။ ဂျာနယ်မှတ်ထားခြင်းဖြင့် ဖြစ်နိုင်ချေရှိသောတိုက်ခိုက်သူများ၏ လုပ်ဆောင်ပုံလုပ်ဆောင်နည်းများ၊ အခြားဆက်စပ်မှုများကို ဖော်ထုတ်ရန် အကူအညီပြုနိုင်ပါသည်။ အကယ်၍ သင့်အနေဖြင့် စိတ်ပျက်အားလျော့နေပြီဟုခံစားရလျှင်၊ အဖြစ်အပျက်များကို သင့်အစား မှတ်တမ်းတင်နိုင်သော သင်ယုံကြည်သူတစ်ဦးဦးကို ရှာဖွေပါ။ သင့်အကောင့်များ၏ ဝင်ရောက်အသုံးပြုခွင့်ကို အဆိုပါပုဂ္ဂိုလ်အား ပေးရမည် ဖြစ်သောကြောင့် သင်အမှန်တကယ်ယုံကြည်ရသည့်သူ ဖြစ်ရန်လိုပါသည်။  သင့်အစား မှတ်တမ်းယူမည့်သူအား သင်၏ကိုယ်ရေးကိုယ်တာအကောင့်များသို့ဝင်ရောက်ခွင့် စကားဝှက် မလွှဲပေးမီ ရှိရင်းစွဲ စကားဝှက်ကို အသစ်အရင်ပြောင်း၍  လုံခြုံစိတ်ချရသောနည်းလမ်းဖြင့်မျှဝေပါ။ ဥပမာအားဖြင့် [အစမှအဆုံးကုဒ်ဖြင့်ပြောင်းသည့်နည်းလမ်း](https://www.frontlinedefenders.org/en/resource-publication/guide-secure-group-chat-and-conferencing-tools) အသုံးပြု၍ မျှဝေနိုင်ပါသည်။ သင့်အကောင့်ကို ပြန်လည်ထိန်းချုပ်အသုံးပြုရန် အဆင်သင့်ဖြစ်ပြီဟု ခံစားရသည်နှင့် သင်တစ်‌ယောက်တည်း သိ၍ [လုံခြုံသော](https://ssd.eff.org/en/module/creating-strong-passwords) စကားဝှက်အသစ်သို့ ပြန်လည်ပြောင်းလဲပါ။

    - သင့်အပေါ်တိုက်ခိုက်မှုများကို အကောင်းဆုံး မှတ်သားထားနိုင်ရန် [ဒီဂျစ်တယ်ရှေးဦးသူနာပြုလမ်းညွှန် စာမျက်နှာရှိ တိုက်ခိုက်မှုဆိုင်ရာ သတင်းအချက်အလက်များကို မှတ်တမ်းတင်မှတ်သားခြင်းဆိုင်ရာ အကြံပြုချက်များ](/../../documentation) အတိုင်း လိုက်ပါဆောင်ရွက်နိုင်ပါသည်။

- **အဆင့်နှစ်ဆင့်ဖြင့် စစ်မှန်ကြောင်း သက်သေပြခြင်းနည်းလမ်းကို အသုံးပြုပါ။** သင့်အကောင့်အားလုံးတွင် အဆင့်နှစ်ဆင့်ဖြင့် စစ်မှန်ကြောင်း သက်သေပြခြင်းနည်းလမ်းကို အသုံးပြုပါ။ ဤနည်းလမ်းသည် သင်၏ခွင့်ပြုချက်မရှိပဲ သင်၏အကောင့်များသို့ အခြားသူများမှ ဝင်ရောက်ခြင်းကို တားဆီးရန် အလွန်ထိရောက်သည်။ ရွေးချယ်၍ရပါက SMS နှင့် အတည်ပြုမှုအား မရွေးချယ်ဘဲ ဖုန်းအက်ပ်တစ်ခု သို့မဟုတ် လုံခြုံရေးသော့ကို အခြေခံသည် နည်းလမ်းတစ်ခုကို ရွေးပါ။

    - အကယ်၍ သင့်အတွက်မည်သည့်အဖြေသည် အကောင်းဆုံးဖြစ်သည်ကိုမသိပါက [Access Now ၏ "သင့်အတွက် အသင့်တော်ဆုံး multi-factor authentication" သရုပ်ပြကားချပ်](https://www.accessnow.org/cms/assets/uploads/2017/09/Choose-the-Best-MFA-for-you.png) နှင့် [EFF၏ "အွန်လိုင်းပေါ်ရှိ သုံးလေ့သုံးထရှိသည့် အဆင့်နှစ်ဆင့်ဖြင့် စစ်မှန်ကြောင်း သက်သေပြခြင်း အမျိုးအစားများ](https://www.eff.org/deeplinks/2017/09/guide-common-types-two-factor-authentication-web) ကို ဝင်ရောက်လေ့လာနိုင်ပါသည်။
    - ပလက်ဖောင်းအများစုတွင် အဆင့်နှစ်ဆင့်ဖြင့် စစ်မှန်ကြောင်း သက်သေပြခြင်းနည်းလမ်းကို စတင်အသုံးပြုရန် ညွှန်ကြားချက်များကို [The 12 Days of 2FA: How to Enable Two-Factor Authentication For Your Online Accounts](https://www.eff.org/deeplinks/2016/12/12-days-2fa-how-enable-two-factor-authentication-your-online-accounts) တွင်တွေ့ရှိနိုင်ပါသည်။

- **အွန်လိုင်းပေါ်ရှိ သင့်အကြောင်းအရာများကို ရှာဖွေစစ်ဆေးပါ။** Self-doxing ဆိုသည်မှာ အခြားမသမာသူများက ရှာဖွေတွေ့ရှိ၍ သင့်အယောင်ဆောင်ရန် အသုံးပြုနိုင်သည့် သတင်းအချက်အလက်များ အွန်လိုင်းတွင် ရှိမရှိကို မိမိကိုယ်တိုင် ရှာဖွေစစ်ဆေးခြင်း ဖြစ်သည်။ အွန်လိုင်းပေါ်ရှိ သင့်အကြောင်းအရာများ၊ သဲလွန်စခြေရာများကို မည်ကဲ့သို့ရှာဖွေရမည်ကို [Access Now Helpline ၏ doxing တားဆီးခြင်း လမ်းညွှန်](https://guides.accessnow.org/self-doxing.html) တွင် ဆက်လက်လေ့လာနိုင်ပါသည်။
- **သင်မသိသူများထံမှ စာပေးပို့မှုများကို လက်မခံပါနှင့်။** WhatsApp၊ Signal၊  Facebook Messenger အစရှိသည့် အချို့သော စာတိုပေးပို့မှု ပလက်ဖောင်းများသည် သင်မသိသူထံမှ ပေးပို့သောစာများကို လက်မခံမီ အမြည်းကြည့်ရှုခွင့် ပေးထားပါသည်။ Apple ၏ iMessage တွင်လည်း သင်မသိသူထံမှ ပေးပို့သောစာများကို စစ်ချရန် settings တွင် ပြောင်းလဲခွင့်ပေးထားပါသည်။ သံသယရှိသည်ဟု ထင်ရလျှင်ဖြစ်စေ၊ သင်မသိသူထံမှ ပေးပို့လျှင်ဖြစ်စေ စာများ၊ အဆက်အသွယ်လုပ်ရန် ကြိုးပမ်းမှုများကို ဘယ်တော့မှ လက်မခံပါနှင့်။

#### resources

- [Access Now Helpline Community Documentation ၏ doxing တားဆီးခြင်း လမ်းညွှန်](https://guides.accessnow.org/self-doxing.html)
- [Access Now Helpline Community Documentation ၏ အမေးများသောမေးခွန်းများ - အရပ်ဖက်လူ့အဖွဲ့အစည်းတွင် ပါဝင်သူများကို ပစ်မှတ်ထားသည့် အွန်လိုင်းနှောင့်ယှက်မှုများ](https://communitydocs.accessnow.org/234-FAQ-Online_Harassment.html)​​​​​​​
- [PEN America ၏ အွန်လိုင်းနှောင့်ယှက်မှုလက်စွဲစာအုပ်](https://onlineharassmentfieldmanual.pen.org/)
- [Equality Labs ၏ Alt-Right မှ တိုက်ခိုက်ခံရသော တက်ကြွလှုပ်ရှားသူတွေအတွက် မိမိခွင့်ပြုချက်မပါပဲ ကိုယ်ရေးကိုယ်တာများဖြန့်ခံရခြင်း တိုက်ဖျက်ရေးဆိုင်ရာ လမ်းညွှန်](https://medium.com/@EqualityLabs/anti-doxing-guide-for-activists-facing-attacks-from-the-alt-right-ec6c290f543c)
- [FemTechNet ၏ ဒစ်ဂျစ်တယ်ကိုယ်ရေးကိုယ်တာများကိုလောခ့်ချခြင်း](https://femtechnet.org/csov/lock-down-your-digital-identity/)
- [National Network to End Domestic Violence ၏ နည်းပညာအလွဲသုံးစားမှု နှင့် နောက်ယောင်ခံမှု ခံစားထားရသူတွေအတွက် အချက်အလက်စုဆောင်းခြင်းလမ်းညွှန်](https://www.techsafety.org/documentationtips)
