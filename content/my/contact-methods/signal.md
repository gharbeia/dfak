---
layout: page
title: Signal
author: mfc
language: my
summary: ဆက်သွယ်ရန် နည်းလမ်းများ
date: 2021-03
permalink: /my/contact-methods/signal.md
parent: /my/
published: true
---

Signal သုံးခြင်းအားဖြင့် သင့် မက်ဆေ့ချ်အရာများသည် လက်ခံသူအဖွဲ့စီသို့သာ ကုဒ်ပြောင်းလဲပြီး ပေးပို့ပါသည်။ ဆက်သွယ်မှုဖြစ်ခဲ့စဥ်ကို သင် နှင့် လက်ခံသူသာ သိရှိပါမည်။ သတိထားရမှာက Signal သည် သင့်ဖုန်းနံပါတ်အား သင့်သုံးသူအမည်အတွက် အသုံးပြု၍ သင့်ဖုန်းနံပါတ်အား လက်ခံသူမှ တွေ့ရှိနိုင်ပါသည်။

လေ့လာရန် - [Signal ကို Android ပေါ်တွင်မည်သို့သုံးရန်](https://ssd.eff.org/en/module/how-use-signal-android), [Signal ကို iOS ပေါ်တွင်မည်သို့သုံးရန်](https://ssd.eff.org/en/module/how-use-signal-ios), [Signal ကို သင့်ဖုန်းနံပါတ် မထုတ်ဖေါ်ပဲ သုံးရန်](https://theintercept.com/2017/09/28/signal-tutorial-second-phone-number/)
