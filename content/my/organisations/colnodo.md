---
name: COLNODO - Escuela de Seguridad Digital
website: https://www.escueladeseguridaddigital.co/
logo: colnodo.png
languages: English, Español
services: in_person_training, triage, org_security, web_hosting, web_protection, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, harassment, individual_care, censorship
beneficiaries: journalists, hrds, activists, lgbti, land, women, youth, cso
hours: "တနင်္လာမှသောကြာ, 8-18, UTC -5"
response_time: "၁ ရက်"
contact_methods: email, pgp, mail, phone, whatsapp, signal, telegram
email: info@escueladeseguridaddigital.co
pgp_key: "https://escueladeseguridaddigital.co/ or https://keys.openpgp.org/search?q=0x5DF922C85DF1D91D"
pgp_key_fingerprint: 6FBB 99FC CED8 CFD6 B960 169A 5DF9 22C8 5DF1 D91D
phone: +57 315 602 1376
signal: +57 315 602 1376
whatsapp: +57 315 602 1376
telegram: +57 315 602 1376
mail: Diagonal 40A 14-75, Bogotá Colombia
initial_intake: yes
---

The Escuela de Seguridad Digital သည် အရပ်ဖက်လူ့အဖွဲ့အစည်းများ၊ စာနယ်ဇင်းသမားများ နှင့် တက်ကြွလှုပ်ရှားသူများ၏ လုံခြုံရေးဆိုင်ရာ လိုက်နာဆောင်ရွက်မှုများ ပိုမိုတိုးတက်လာစေရန် အတူတကွလိုက်ပါကူညီလမ်းညွှန်ရန် ရည်ရွယ်သည်။ ပညာပေးခြင်းလုပ်ငန်းစဉ်မှတစ်ဆင့် ၎င်းတို့၏ သတင်းအချက်အလက်နှင့် ဆက်သွယ်ရေးတို့အပေါ် သက်ရောက်မှုရှိသည် ဒစ်ဂျစ်တယ်ခြိမ်းခြောက်မှုများ နည်းနိုင်သမျှနည်းစေရန် ရည်ရွယ်သည်။
