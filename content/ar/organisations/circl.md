---
name: Computer Incident Response Center Luxembourg
website: https://circl.lu
logo: circl.png
languages: English, Deutsch, Français, Luxembourgish
services: org_security, vulnerabilities_malware, forensic
beneficiaries: activists, lgbti, women, youth, cso
hours: office_hours, UTC+2
response_time: 4 ساعات
contact_methods: web_form, email, pgp, mail, phone
web_form: https://www.circl.lu/contact
email: info@circl.lu
pgp_key_fingerprint: ‬CA57 2205 C002 4E06 BA70 BE89 EAAD CFFC 22BD 4CD5‬
phone: +352 247 88444‬
mail: 16, bd d'Avranches, L-1160 Luxembourg, Grand-Duchy of Luxembourg
initial_intake: yes
---

منظَّمة CIRCL مركزٌ للاستجابة لحوادث الحواسيب للقطاع الخاص و&nbsp;المجتمعات و&nbsp;المنظمات غير الحكومية مقرُّها لوكسمبورج. و&nbsp;هي بمثابة جهة للاتصال يمكن لمستخدمي الحواسيب و&nbsp;الشركات و&nbsp;المنظّمات الاعتماد عليها للمعاونة في تدارك الهجمات و&nbsp;الحوادث الأخرى. إذ تضمّ فريقًا من الخبراء يعمل كفرقة إنقاذ لديه القدرة على الاستجابة السريعة الكفؤة حين التحسّب لوقوع تهديدات أو عند وقوعها. هدف CIRCL جمع البيانات عن التهديدات السبرانية و&nbsp;دراستها و&nbsp;الاستجابة لها بمنهجية و&nbsp;بسرعة.
