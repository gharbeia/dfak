---
name: COLNODO - Escuela de Seguridad Digital
website: https://www.escueladeseguridaddigital.co/
logo: colnodo.png
languages: English, Español
services: in_person_training, triage, org_security, web_hosting, web_protection, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, harassment, individual_care, censorship
beneficiaries: journalists, hrds, activists, lgbti, land, women, youth, cso
hours: من الاثنين إلى الجمعة، من 08:00 إلى 18:00ء، UTC -5
response_time: يوم
contact_methods: email, pgp, mail, phone, whatsapp, signal, telegram
email: info@escueladeseguridaddigital.co
pgp_key: "https://escueladeseguridaddigital.co/ or https://keys.openpgp.org/search?q=0x5DF922C85DF1D91D"
pgp_key_fingerprint: 6FBB 99FC CED8 CFD6 B960 169A 5DF9 22C8 5DF1 D91D
phone: +57 315 602 1376
signal: +57 315 602 1376
whatsapp: +57 315 602 1376
telegram: +57 315 602 1376
mail: Diagonal 40A 14-75, Bogotá Colombia
initial_intake: yes
---

مدرسة الحقوق الرقمية خدمة من مؤسسة كولوندو تهدف إلى مرافقة مؤسّسة المجتمع المدني و&nbsp;الصحافيين و&nbsp;النشطاء بوجه عام تجاه تحسين ممارساتهم المتعلّقة بالأمان الرقمي من خلال صيرورات تعليمية تقلّل المخاطر الرقمية التي تتعرّض لها معلوماهم و&nbsp;اتّصالاتهم.