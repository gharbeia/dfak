---
layout: page
title: Contato Telefônico
author: mfc
language: pt
summary: Métodos de contato
date: 2018-09
permalink: /pt/contact-methods/phone-number.md
parent: /pt/
published: true
---

Comunicações através de celulares e linhas de telefonia fixa não são criptografadas, logo o conteúdo da conversa e a informação sobre quem está ligando estará acessível aos governos, autoridades, e outros atores com os devidos aparatos técnicos, possam ter acesso a essas ligações.
