---
name: Digital Defenders Partnership
website: https://www.digitaldefenders.org
logo: DDP_logo_zwart_RGB.png
languages: Español, English, Français, Português, Deutsch, Nederlands, Русский
services: grants_funding, in_person_training, org_security, equipment_replacement, assessment, secure_comms, device_security, vulnerabilities_malware
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso
hours: "Segunda à Quinta 9h às 17h CET"
response_time: 4 dias
contact_methods: email, phone, mail
email: team@digitaldefenders.org
mail: Raamweg 16, 2596 HL Den Haag
phone: +31 070 376 5500
---




A Digital Defenders Partnership oferece apoio para pessoas defensoras de direitos humanos sob ameaça digital, e atua para o fortalecimento de redes locais de resposta rápida. A DDP coordena fundos emergenciais para organizações da sociedade civil, jornalistas, produtores de conteúdo e pessoas atuantes defesa de direitos humanos.

A DDP possui também três diferentes fundos que incidem em situações de emergência, além de linhas de financiamento de maior duração focadas em construção de capacidades junto a outras organizações. Além disso, coordenam a Digital Integrity Fellowship no qual organizações recebem treinamentos personalizados em segurança e privacidade digital e, ainda, o programa da Rede de Resposta Rápida.
