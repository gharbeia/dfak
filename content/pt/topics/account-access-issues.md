---
layout: page
title: "Não consigo acessar minhas contas"
author: RaReNet
language: pt
summary: "Está tendo problemas ao acessar um email, rede social ou a conta de um site? Sua conta mostra acessos que você não sabe de onde vêm? Existem várias coisas que você pode fazer para mitigar estes problemas."
date: 2023-05
permalink: /pt/topics/account-access-issues/
parent: /pt/
---

# Não consigo acessar minhas contas

Contas de redes sociais, aplicativos de conversa e e-mail são frequentemente utilizadas pela sociedade civil para se comunicarem, trocarem conhecimentos e promover suas causas. Como consequência, estas contas são bastante visadas por pessoas mal intencionadas, que geralmente tentam prejudicar o acesso e as informações destas contas, causando prejuízos as pessoas e seus contatos.

Este guia existe para ajudar no caso de alguma de suas contas ter sido comprometida.

Siga o questionário para identificar a natureza do problema e achar possíveis soluções.

## Workflow

### Password-Typo

> Em alguns casos, o motivo de não conseguir entrar na sua conta pode ser simplesmente um erro de digitação momentâneo ou por uma configuração de teclado diferente da que você utiliza normalmente (seja por estar em outro dispositivo, ou por uso acidental de teclas de atalho), ou mesmo por pressionar acidentalmente a tecla CapsLock.
>
> Para verificar, abra um editor de texto ou bloco de notas e digite seu usuário e sua senha, confira se está correto, e cole no navegador ou aplicativo.

As sugestões acima ajudaram a entrar na sua conta?

- [Sim](#resolved_end)
- [Não](#account-disabled)

### account-disabled

> Algumas vezes, o motivo de você não conseguir entrar na sua conta pode ser por ela ter sido bloqueada ou desativada pela própria plataforma devido à violações dos Termos de Serviço ou das regras da plataforma. Isto pode acontecer quando a sua conta é muito denunciada ou quando os mecanismos de denúncia e apoio da plataforma são utilizados de forma abusiva com a intenção de censurar conteúdos online.
>
> Se você está recebendo uma mensagem dizendo que sua conta foi bloqueada, restringida, desativada ou suspensa e você acredita que isso foi um erro, siga as instruções de pedido de recurso que são fornecidos na mensagem. Você pode encontrar informações sobre como submeter recursos para reverter a situação nos links a seguir:
>
> - [Facebook](https://www.facebook.com/help/185747581553788)
> - [Instagram](https://help.instagram.com/366993040048856)
> - [Twitter](https://help.twitter.com/pt/forms/account-access/appeals/redirect)
> - [Youtube](https://support.google.com/youtube/answer/2802168)

As sugestões te ajudaram a fazer login na sua conta?

- [Sim](#resolved_end)
- [Não](#what-type-of-account-or-service)

### what-type-of-account-or-service

Qual tipo de conta você está tentando acessar?

- [Facebook](#Facebook)
- [Facebook Page](#Facebook-Page)
- [Twitter](#Twitter)
- [Google/Gmail](#Google)
- [Yahoo](#Yahoo)
- [Hotmail/Outlook/Live](#Hotmail)
- [ProtonMail](#ProtonMail)
- [Instagram](#Instagram)
- [TikTok](#Tiktok)
  <!--- - [AddOtherServiceLink](#service-Name) -->

### Facebook-Page

Sua página é administrada por outras pessoas além de você?

- [Sim](#Other-admins-exist)
- [Não](#Facebook-Page-recovery-form)

### Other-admins-exist

Estas pessoas estão com o mesmo problema?

- [Sim](#Facebook-Page-recovery-form)
- [Não](#Other-admin-can-help)

### Other-admin-can-help

> Peça para que estas pessoas te adicionem como admin da página novamente.

Isso corrigiu o problema?

- [Sim](#Fb-Page_end)
- [Não](#account_end)

### Facebook-Page-recovery-form

> Por favor, entre no Facebook e use [este formulário de recuperação de páginas](https://www.facebook.com/help/contact/164405897002583). Se você não consegue fazer login na sua conta do Facebook, por favor siga as instruções da sessão [Recuperação de conta do Facebook](#Facebook)
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento em favoritos, no seu navegador, para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

O procedimento de recuperação funcionou para você?

- [Sim](#resolved_end)
- [Não](#account_end)

<!---=========================================================
//GoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogle
=========================================================-->

### Google

Você tem acesso ao email ou celular configurado para recuperar sua conta?

- [Sim](#I-have-access-to-recovery-email-google)
- [Não](#Recovery-Form-google)

### I-have-access-to-recovery-email-google

> Confira seu e-mail de recuperação se você recebeu uma mensagem do Google dizendo "Alerta de segurança para sua Conta do Google vinculada", essa mensagem também pode vir via SMS.
>
> Ao verificar as mensagens de e-mail, tenha sempre cuidado com as tentativas de phishing. Se não tiver a certeza da legitimidade de alguma mensagem, consulte a seção [mensagens suspeitas](../../../suspicious-messages/).

Você recebeu algum e-mail de "Alerta de segurança" vinculada a sua Conta Google ou algum SMS do Google?

- [Sim](#Email-received-google)
- [Não](#Recovery-Form-google)

### Email-received-google

Depois de ter verificado a legitimidade do e-mail, reveja as informações fornecidas. No corpo do e-mail existe um link "Recuperar sua conta"?

- [Sim](#Recovery-Link-Found-google)
- [Não](#Recovery-Form-google)

### Recovery-Link-Found-google

> Utilize o link enviado neste email para iniciar o processo de recuperação da conta. Quando abrir o link, verifique se o endereço (URL) que você está de fato pertence a um domínio da Google, se é um endereço "google.com"

Você conseguiu recuperar sua conta?

- [Sim](#resolved_end)
- [Não](#Recovery-Form-google)

### Recovery-Form-google

> Por favor, siga as instruções em ["Como recuperar sua Conta do Google ou serviço do Gmail"](https://support.google.com/accounts/answer/7682439?hl=pt-br).
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos favoritos do seu navegador para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

O procedimento de recuperação funcionou para você?

- [Sim](#resolved_end)
- [Não](#account_end)

<!---=========================================================
//YahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahoo
=========================================================-->

### Yahoo

Você tem acesso ao email ou celular configurado para recuperar sua conta?

- [Sim](#I-have-access-to-recovery-email-yahoo)
- [Não](#Recovery-Form-Yahoo)

### I-have-access-to-recovery-email-yahoo

> Confira seu e-mail de recuperação se você recebeu um email do Yahoo dizendo "Alterar a senha da sua conta Yahoo"?
>
> Ao verificar as mensagens de e-mail, tenha sempre cuidado com as tentativas de phishing. Se não tiver a certeza da legitimidade de alguma mensagem, consulte a seção [mensagens suspeitas](../../../suspicious-messages/).

 Você recebeu do Yahoo um email "Alterar a senha da sua conta Yahoo"?

- [Sim](#Email-received-yahoo)
- [Não](#Recovery-Form-Yahoo)

### Email-received-yahoo

Depois verificar a legitimidade do e-mail, revise as informações fornecidas por ele. Dentro do e-mail existe um link "Recupere sua conta aqui"?

- [Sim](#Recovery-Link-Found-Yahoo)
- [Não](#Recovery-Form-Yahoo)

### Recovery-Link-Found-Yahoo

> Utilize o link enviado neste e-mail para iniciar o processo de recuperação da conta.

Você conseguiu recuperar sua conta?

- [Sim](#resolved_end)
- [Não](#Recovery-Form-Yahoo)

### Recovery-Form-Yahoo

> Por favor, siga as instruções em ["Corrigindo problemas de login na sua conta Yahoo"](https://br.ajuda.yahoo.com/kb/account/Corrigir-problemas-de-login-em-sua-conta-do-Yahoo-sln2051.html) para recuperar sua conta.
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos favoritos do seu navegador para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

O procedimento de recuperação funcionou para você?

- [Sim](#resolved_end)
- [Não](#account_end)

<!---=========================================================
TwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitter
//========================================================= -->

### Twitter

Você tem acesso ao email ou celular configurado para recuperar sua conta?

- [Sim](#I-have-access-to-recovery-email-Twitter)
- [Não](#Recovery-Form-Twitter)

### I-have-access-to-recovery-email-Twitter

> Confira seu e-mail de recuperação. Você recebeu um email do Twitter dizendo "Sua senha do Twitter foi alterada"?
>
> Ao verificar as mensagens de e-mail, tenha sempre cuidado com as tentativas de phishing. Se não tiver a certeza da legitimidade de alguma mensagem, consulte a seção [mensagens suspeitas](../../../suspicious-messages/).

Você recebeu um e-mail do Twitter com a mensagem "Sua senha do TWitter foi alterada"?

- [Sim](#Email-received-Twitter)
- [Não](#Recovery-Form-Twitter)

### Email-received-Twitter

Depois de verificar a legitimidade do e-mail, revise as informações fornecidas por ele. Dentro do e-mail existe um link "Recupere sua conta aqui"?

- [Sim](#Recovery-Link-Found-Twitter)
- [Não](#Recovery-Form-Twitter)

### Recovery-Link-Found-Twitter

> Utilize o link enviado neste email para iniciar o processo de recuperação da conta.

Você conseguiu recuperar sua conta?

- [Sim](#resolved_end)
- [Não](#Recovery-Form-Twitter)

### Recovery-Form-Twitter

> Se você acredita que sua conta no Twitter está comprometida, siga as instruções em ["Solicitar ajuda para restaurar minha conta"](https://help.twitter.com/pt/forms/account-access/reactivate-my-account).
>
> Se sua conta não estiver comprometida ou se você tiver outros problemas de acesso à sua conta, siga os passos em ["Pedir ajuda para restaurar sua conta"](https://help.twitter.com/forms/restore).
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos favoritos do seu navegador para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

O procedimento de recuperação funcionou para você?

- [Sim](#resolved_end)
- [Não](#account_end)

<!---=========================================================
//Protonmail
//========================================================= -->

### ProtonMail

> Por favor, siga [as instruções para resetar sua senha](https://proton.me/pt-br/support/reset-password) para recuperar sua conta.
>
> É importante que você saiba que ao redefinir a senha você não poderá ler os e-mails já existentes nem ver os contatos, isso porque o Protomail utiliza a sua senha como chave para criptografar e descriptografar as informações. Os dados antigos podem ser recuperados se você tiver acesso a um arquivo de recuperação ou a uma frase de recuperação. Siga as orientações em ["Recuperar mensagens e arquivos criptografados"](https://proton.me/pt-br/support/recover-encrypted-messages-files) para saber criá-los.

O procedimento de recuperação funcionou para você?

- [Sim](#resolved_end)
- [Não](#account_end)

<!---==================================================================
//MicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlook
//================================================================== -->

### Hotmail

Do you have access to the connected recovery email/mobile?

- [Yes](#I-have-access-to-recovery-email-Hotmail)
- [No](#Recovery-Form-Hotmail)

### I-have-access-to-recovery-email-Hotmail

> Confira seu e-mail de recuperação. Você recebeu um email da Microsoft dizendo "Alteração de senha da sua conta Microsoft"?
>
> Ao verificar as mensagens de e-mail, tenha sempre cuidado com as tentativas de phishing. Se não tiver a certeza da legitimidade de alguma mensagem, consulte a seção [mensagens suspeitas](../../../suspicious-messages/).

Você recebeu um email "Alteração de senha da sua conta Microsoft", enviado pela Microsoft?

- [Sim](#Email-received-Hotmail)
- [Não](#Recovery-Form-Hotmail)

### Email-received-Hotmail

Depois verificar a legitimidade do e-mail, revise as informações fornecidas por ele. Dentro do e-mail existe um link "Recupere sua conta aqui"?

- [Sim](#Recovery-Link-Found-Hotmail)
- [Não](#Recovery-Form-Hotmail)

### Recovery-Link-Found-Hotmail

> Utilize o link enviado neste email para iniciar o processo de recuperação da conta.

Você conseguiu recuperar sua conta?

- [Sim](#resolved_end)
- [Não](#Recovery-Form-Hotmail)

### Recovery-Form-Hotmail

> Experimente [a "Ferramenta de ajuda para iniciar sessão"] (https://go.microsoft.com/fwlink/?linkid=2214157). Siga as instruções na ferramenta inserindo a conta que você está tentando recuperar e respondendo as questões para recuperação.
>
> Note que esta solicitação, ***realizada através de formulário online***, pode levar algum tempo até ser respondida. Salve este procedimento nos favoritos do seu navegador para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

O procedimento de recuperação funcionou para você?

- [Sim](#resolved_end)
- [Não](#account_end)

<!---==================================================================
//FacebookMetaFacebookMetaFacebookMetaFacebookMetaFacebookMetaFacebook
//================================================================== -->


### Facebook

Você tem acesso ao e-mail ou celular configurado para recuperar sua conta?

- [Sim](#I-have-access-to-recovery-email-Facebook)
- [Não](#Recovery-Form-Facebook)

### I-have-access-to-recovery-email-Facebook

> Confira seu e-mail de recuperação. Você recebeu um e-mail do Facebook dizendo "Alteração de senha do Facebook"?
>
> Ao verificar as mensagens de e-mail, tenha sempre cuidado com as tentativas de phishing. Se não tiver a certeza da legitimidade de alguma mensagem, consulte a seção [mensagens suspeitas](../../../suspicious-messages/).

Você recebeu um email "Alteração de senha da sua conta do Facebook", enviado pelo Facebook?

- [Sim](#Email-received-Facebook)
- [Não](#Recovery-Form-Facebook)

### Email-received-Facebook

Depois verificar a legitimidade do e-mail, revise as informações fornecidas por ele. No corpo do e-mail existe uma mensagem que pergunta "Foi você?", com um link para tomar medidas de segurança?

- [Sim](#Recovery-Link-Found-Facebook)
- [Não](#Recovery-Form-Facebook)

### Recovery-Link-Found-Facebook

> Utilize o link "Não fui eu" na mensagem para recuperar a sua conta.

Você conseguiu recuperar sua conta clicando no link?

- [Sim](#resolved_end)
- [Não](#Recovery-Form-Facebook)

### Recovery-Form-Facebook

> Por favor, siga as instruções do [formulário para recuperação da conta](https://pt-br.facebook.com/help/105487009541643/).
>
> Note que esta solicitação, realizada através de formulário online, pode levar algum tempo até ser respondida. Salve este procedimento nos favoritos do seu navegador para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

O procedimento de recuperação funcionou para você?

- [Sim](#resolved_end)
- [Não](#account_end)

<!--- ==================================================================
InstagramInstagramInstagramInstagramInstagramInstagramInstagramInstagram
//================================================================== -->

### Instagram

Você tem acesso ao e-mail ou celular configurado para recuperar sua conta?

- [Sim](#I-have-access-to-recovery-email-Instagram)
- [Não](#Recovery-Form-Instagram)

### I-have-access-to-recovery-email-Instagram

> Confira seu e-mail de recuperação. Você recebeu um e-mail do Instagram dizendo "Sua senha do Instagram foi alterada"?
>
> Ao verificar as mensagens de e-mail, tenha sempre cuidado com as tentativas de phishing. Se não tiver a certeza da legitimidade de alguma mensagem, consulte a seção [mensagens suspeitas](../../../suspicious-messages/).

 Você recebeu do Instagram um e-mail ***"Sua senha no Instagram foi alterada"***?

- [Sim](#Email-received-Instagram)
- [Não](#Recovery-Form-Instagram)

### Email-received-Instagram

Depois verificar a legitimidade do e-mail, revise as informações fornecidas por ele. No corpo do e-mail existe uma mensagem "proteja sua conta aqui", com um link?

- [Sim](#Recovery-Link-Found-Instagram)
- [Não](#Recovery-Form-Instagram)

### Recovery-Link-Found-Instagram

> Utilize o link no e-mail enviado pelo Instagram para iniciar o processo de recuperação da conta.

Você conseguiu recuperar sua conta?

- [Sim](#resolved_end)
- [Não](#Recovery-Form-Instagram)

### Recovery-Form-Instagram

> Por favor, siga as instruções em ["Acho que minha conta do Instagram foi invadida"](https://help.instagram.com/149494825257596?helpref=search&sr=1&query=hacked) para recuperar sua conta.
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos favoritos do seu navegador para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

O procedimento de recuperação funcionou para você?

- [Sim](#resolved_end)
- [Não](#account_end)

<!--- ==================================================================
TiktokTiktokTiktokTiktokTiktokTiktokTiktokTiktokTiktokTiktokTiktokTiktok
//================================================================== -->

### Tiktok

Você tem acesso ao email ou celular configurado para recuperar sua conta?

- [Sim](#I-have-access-to-recovery-email-Tiktok)
- [Não](#Recovery-Form-Tiktok)

### I-have-access-to-recovery-email-Tiktok

> Se você tem acesso ao email de recuperação, redefina sua senha seguindo as orientações em [Processo para redefinir sua senha do TikTok](https://www.tiktok.com/login/email/forget-password).

Você conseguiu recuperar sua conta?

- [Sim](#resolved_end)
- [Não](#Recovery-Form-Tiktok)

### Recovery-Form-Tiktok

> Siga as instruções em ["Minha conta foi invadida"](https://support.tiktok.com/pt_BR/log-in-troubleshoot/log-in/my-account-has-been-hacked) para recuperar sua conta.

Você conseguiu recuperar sua conta?

- [Sim](#resolved_end)
- [Não](#account_end)


### Fb-Page_end

Que ótimo que conseguimos solucionar seu problema! Leia as recomendações a seguir para diminuir as possibilidades de perder acesso novamente no futuro:

- Ative a autenticação de dois fatores para todos os administradores na página.
- Atribua funções de administração apenas a pessoas em quem confia e que sejam responsivas.
- Se houver alguém em quem você possa confiar, considere ter mais de uma conta de administrador. Tenha em mente que você deve ativar a autenticação de dois fatores para todas as contas de administrador.  
- Reveja regularmente os privilégios e permissões na página. Sempre atribua o nível mínimo de privilégio necessário para que o usuário realize o seu trabalho.


### account_end

Se os procedimentos sugeridos neste diagnóstico não foram capazes de ajudar você a recuperar a conta, procure uma das seguintes organizações capazes de auxiliar com os próximos passos:

:[](organisations?services=account)

### resolved_end

Esperamos que o Kit tenha sido útil para você. Será um prazer receber suas considerações [através deste email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

Leia as recomendações a seguir para diminuir as possibilidades de perder acesso novamente no futuro:

- É sempre uma boa ideia ativar a autenticação de dois fatores (2FA) para suas contas em todas as plataformas que a suportem.
- Nunca utilize a mesma senha para mais de uma conta. Se você está fazendo isso atualmente, deve alterá-las, usando uma senha única para cada uma de suas contas.
- Usar um programa gerenciador de senhas vai te ajudar a guardar e criar senhas diferentes e mais fortes.
- Tenha cautela ao utilizar redes Wi-Fi públicas não confiáveis e se possível use uma VPN ou o navegador Tor ao se conectar por elas.

#### resources

- [Documentação da Linha de Ajuda da Access Now: Recomendações sobre gerenciadores de senhas para uso em equipa](https://accessnowhelpline.gitlab.io/community-documentation/295-Password_managers.html) (em inglês)
- [Autodefesa contra Vigilância: Como se proteger nas redes sociais](https://ssd.eff.org/pt-br/module/como-se-proteger-nas-redes-sociais)​​​​​​​
- [Autodefesa contra Vigilância: Criando senhas fortes](https://ssd.eff.org/pt-br/module/criando-senhas-fortes)

<!--- Se quiser criar um novo fluxo é só colar e editar o exemplo abaixo, substituindo o service_name pelo nome real do serviço utilizado:
#### service-name

Você tem acesso ao email ou celular configurado para recuperar sua conta?

- [Sim](#I-have-access-to-recovery-service-name)
- [Não](#Recovery-Form-service-name)

### I-have-access-to-recovery-email-service-name

Confira seu e-mail de recuperação. Você recebeu um email [nome do serviço] dizendo "[assunto do e-mail]"?

- [Sim](#Email-received-service-name)
- [Não](#Recovery-Form-service-name

### Email-received-service-name

> Dentro do e-mail existe um link "recuperar sua senha"?


- [Sim](#Recovery-Link-Found-service-name)
- [Não](#Recovery-Form-service-name)

### Recovery-Link-Found-service-name

> Utilize o link enviado neste email para iniciar o processo de recuperação da conta.

Você conseguiu recuperar sua conta?

- [Sim](#resolved_end)
- [Não](#Recovery-Form-service-name)

### Recovery-Form-service-name

> Por favor, tente seguir [este formulário para recuperar sua conta](link para o formulário de recuperação de senha do serviço).
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

O procedimento de recuperação funcionou para você?

- [Sim](#resolved_end)
- [Não](#account_end)

-->
