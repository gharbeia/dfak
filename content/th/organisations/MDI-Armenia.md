---
name: MDI Armenia
website: https://mdi.am/
logo: MDI_Armenia_Logo.png
languages: հայերեն, Русский, English
services: in_person_training, org_security, web_hosting, web_protection, digital_support, triage, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, censorship
beneficiaries: journalists, hrds, activists, lgbti, women, cso
hours: ตลอด 24 ชั่วโมง ทั้ง 7 วัน, โซนเวลา GMT+4
response_time: 3 ชั่วโมง
contact_methods: email, pgp, phone, whatsapp, signal, telegram
email: help@mdi.am
pgp_key_fingerprint: D63C EC9C D3AD 51BE EFCB  683F F2B8 6042 F9D9 23A8
phone: "+37494938363"
whatsapp: "+37494938363"
signal: "+37494938363"
telegram: "+37494938363"
initial_intake: yes
---

Media Diversity Institute - Armenia หรือ MDI Armenia เป็นองค์กรที่ไม่ใช่ภาครัฐและไม่แสวงหาผลกำไร ซึ่งมีเป้าหมายในการใช้ประโยชน์จากสื่อดั้งเดิม สื่อโซเชียล และเทคโนโลยีใหม่ๆ ในการคุ้มครองสิทธิมนุษยชน ช่วยสร้างสังคมที่มีอารยะและเป็นประชาธิปไตย มอบเสียงให้กับกลุ่มผู้ไร้เสียง และเสริมสร้างความเข้าใจอันลึกซึ้งให้คนทั่วไปเกี่ยวกับประเภทต่างๆของความหลากหลายในสังคม

MDI Armenia มีความเกี่ยวข้องกับ Media Diversity Institute ที่ตั้งอยู่ ณ กรุงลอนดอน แต่ถือเป็นหน่วยงานที่มีความเป็นอิสระจากกัน
