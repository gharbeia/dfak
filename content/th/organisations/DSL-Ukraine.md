---
name: Digital Security Lab Ukraine
website: https://dslua.org/
logo: DSL-Ukraine.png
languages: English, Українська, Русский, Français
services: in_person_training, web_protection, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, forensic, legal, individual_care, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso
hours: วันจันทร์ – วันพฤหัสบดี เวลา 9:00 – 17:00 น. โซนเวลาเวลายุโรปตะวันออก/ฤดูร้อนยุโรปตะวันออก (EET/EEST) หรือ UTC+5
response_time: 2 วันทำการ
contact_methods: email, pgp, signal, whatsapp
email: koushnir@gmail.com, gudvadym@gmail.com
pgp: koushnir@gmail.com - 000F08EA02CE0C81, gudvadym@gmail.com - B20B8DA28B2FA0C3
signal: +380987767783 +380990673853
whatsapp: +380987767783 +380990673853
---

Digital Security Lab Ukraine เป็นองค์กรที่ไม่ใช่ภาครัฐ ซึ่งตั้งอยู่ ณ กรุงเคียฟ และก่อตั้งขึ้นในปีค.ศ. 2017 มีจุดมุ่งหมายคือการสนับสนุนการนำหลักสิทธิมนุษยชนมาปฏิบัติใช้ในพื้นที่อินเทอร์เน็ต โดยเสริมสร้างศักยภาพให้กับองค์กรที่ไม่ใช่ภาครัฐและสื่ออิสระเพื่อช่วยจัดการกับข้อห่วงกังวลเรื่องความปลอดภัยทางดิจิทัลของพวกเขา และขับเคลื่อนนโยบายด้านสิทธิทางดิจิทัลของภาครัฐและภาคธุรกิจ