---
layout: page
title: "I think I'm being surveilled"
author: Carlos Guerra, Peter Steudtner, Ahmad Gharbeia
language: en
summary: "If you think you may be a target of digital surveillance, this workflow will guide you through questions to identify possible indicators of surveillance."
date: 2023-05
permalink: /en/topics/surveilled
parent: Home
---

# I think I'm being surveilled

Recently, we have seen a lot of emerging cases of surveillance to civic space actors, from the use of stalkerware by abusive partners and corporate surveillance of employees, to mass state surveillance and targeted spyware campaigns against activists and journalists. Given the broad spectrum of observed techniques and precedents, it is impossible to cover every aspect of surveillance, especially for emergency support.

That said, this workflow will focus on common cases seen in the digital sphere and related to end-user devices and online accounts that represent emergencies with actionable response. When there is not a feasible solution, then we won't consider it an emergency, and it won't be covered extensively (instead, references will be shared).

Some of the cases this workflow doesn't cover are:

- Physical surveillance. If you fear you are targeted by physical surveillance and want support, you can reach out to organizations focusing on physical security in the Digital First Aid Kit [support page](../../support).
- Massive surveillance with hardware like CCTV cameras in public, corporate, and private spaces
- Hardware bugs not dependent on an internet connection

A second relevant disclaimer is that in modern days everyone is being surveilled by default at some degree:

- Cellphone infrastructure is designed to gather a massive amount of data on ourselves, which can be used by several actors to learn details like our location, network of contacts, etc.
- Online services like social networks and email providers also gather a great ammount of data about us, which can be linked for instance to advertising campaigns to show us ads related to the topics of our posts or even one-to-one messages.
- More and more countries have many interconnected systems that use our identity and gather information on our interactions with various public institutions, enabling the creation of a "profile" based on document requests for tax or health information.

These examples of mass surveillance, plus the many precedents of targeted operations, create the sense of everyone (especially in the activist space) being surveilled, which can lead to paranoia and affect our emotional capacity to deal with actual threats we may face. That said, there are specific cases where - depending on our risk profile, the work we do, and the capacities of our potential adversaries - we might have founded suspicions that we are being surveilled. If that is your case, keep reading.

Aa a third disclaimer, many surveillance schemes (especially the most targeted ones), usually leave few or no indicators accessible to the victims, making detection difficult and sometimes impossible. Please keep this in mind while navigating the workflow, and if you still believe your case is not covered or more complex, please reach out to the organizations listed at the end of the workflow.

As a final consideration, please keep in mind that surveillance efforts can be massive or targeted. Massive surveillance operations usually cover entire populations or big parts of them, like all the users of a platform, all the people from a specific region, etc. These usually rely more on technology and are usually less dangerous than targeted surveillance, which targets one or a small number of indivuals, requires dedicated resources (money, time, and/or capacity), and is usually more dangerous than the massive surveillance given the motivations behind putting resources to monitor the activity and communications of specific persons.

If you found an indicator that you are beiong surveilled, click start to begin with the workflow.

## Workflow

### start

> If you are here, probably you have reasons to think your communications, location or other online activities might be surveilled. Below you will find a series of common indicators of suspicious activity related to surveillance. Click on your case to continue.

Given the information provided in the introduction, what kind of issue are you experiencing?

 - [I found a tracking device close to me](#tracking-device-intro)
 - [My device is acting suspiciously](#device-behaviour-intro)
 - [I'm being redirected in the browser to unprotected http-websites for downloading updates or apps](#suspicious-redirections-intro)
 - [I'm getting security messages and alerts from trusted services](#security-alerts-intro)
 - [I found a suspicious device linked to my computer or network](#suspicious-device-intro)
 - [Information that I shared privately via email, messenger or similar is known to the adversary](#leaked-internet-info-intro)
 - [Confidential information shared via phone calls or SMS seems to have leaked to the adversary (especially during sensitive events)](#leaked-phone-info-intro)
 - [People around you have been successfully targeted by surveillance measures](#peer-compromised-intro)
 - [Something else / I don't know](#other-intro)

### tracking-device-intro

> If you found an unexpected device close to you, and you believe it is a surveillance device, the first step would be confirming that indeed the device is there to track you, and it doesn't fulfill other relevant functions. With the introduction of Internet of Things (IoT) devices, it is more common to find devices doing a big array of actions that we can forget after using them for a while. One of the first steps to make sure it is a surveillance device is to inspect the device for information on brand, model, etc. Some common tracking devices that are available at low prices in the market include Apple Airtags, Samsung Galaxy SmartTag, or Tile trackers. These devices are sold for legitimate purposes like helping find personal articles. However, malicious actors might abuse them to track other people without consent.
>
> To check if we are being followed by an unexpected tracker, besides locating them physically, it could be useful to consider some specific solutions:
>
> - For Apple Airtags, in iOS [you should receive an automatic notification after the unknown device has been detected for some time](https://support.apple.com/en-us/HT212227).
> - For Android, there is an [app to check for nearby Airtags](https://play.google.com/store/apps/details?id=com.apple.trackerdetect).
> - For Tile devices, the [Tile app](https://www.tile.com/download) has a feature to check for unknown trackers.
> - For Samsung Galaxy SmartTags, you can use the [SmartThings app](https://www.samsung.com/us/smartthings/) for a similar feature.
>
> Another kind of device  you may find are GPS trackers. These log the location of the target frequently and in some cases can transmit the information through a phone line, or can store the locations in some form of internal storage that can be physically collected later to download and analyze. If you find a device like this, understanding the way the device operates is key to have a better sense of who planted the device and how they are obtaining the information.
>
> For any kind of surveillance device you find, you should document everything about it: brand, model, if it connects to the wireless network, where exactly you found it, any names of the device in the relevant application, etc.

After identifying the device, there are some strategies you might want to follow. What do you want to do with the device?

 - [I want to disable it](#tracking-disable-device)
 - [I would like to use it to mislead the operator](#tracking-use-device)

### tracking-disable-device

> Depending on the type and brand of the device you have found, you might be able to turn it off or disable it entirely. In some cases this can be done through tracker applications like the ones described in the previous step, but also through physical power switches, or even by damaging the device. Please keep in mind that if you want to conduct a deeper investigation around this incident, disabling the device might alter relevant evidence stored in the device.

Do you want to take further action to identify the owner of the device? (*please note you will have to keep the tracking device online to do so*)

 - [Yes](#identify-device-owner)
 - [No](#pre-closure)

### tracking-use-device

> One common strategy to deal with tracking devices is to make them track something different than what was intended. For instance, you can leave it in a static safe position while you go to other places, or you can even make it track a different target in movement unrelated to you. Please keep in mind that in some of these scenarios you might lose access to the device and making the tracker follow another person or track a specific location may have security and legal implications.

Do you want to take further action to identify the owner of the device? (*please note you will have to keep the tracking device online to do so*)

 - [Yes](#identify-device-owner)
 - [No](#pre-closure)

### identify-device-owner

> Identifying the owner of the tracking device might be difficult in some scenarios, and this will depend largely on the kind of device used, so we recommend researching specifically on the tracking device you have found. Some examples are:
>
> - For consumer-grade wireless trackers:
>     - How do they show in the checking applications?
>     - When did you receive a notification?
>     - Is there any way to extract more data from the device that might contain relevant information?
> - For more dedicated trackers, like GPS trackers:
>     - Do they have internal storage? Some models have an SD card that can be extracted to see when it started capturing data, or any other relevant information.
>     - Do they have a phone line? Can you extract a SIM card from the device and see if you can try it in another phone to obtain the number? With this information, can you find out who is the owner of that phone line?
> - In general
>     - Look for any physical cues, like names written in the device, inventory numbers, etc.
>     - If the device connects to the local WiFi network, does it have any specific device name that can include identification data?
>     - Who could access the place where you found the device? Knowing the place, when could it have been planted?

Do you want to take legal action against the tracking device owner?

 - [Yes](#legal-action-device-owner)
 - [No](#pre-closure)

### legal-action-device-owner

> In case you want to take legal action against the owner of the surveillance device, you will need to check what the process looks like in your jurisdiction. In some cases it will require a lawyer, in others it will suffice to approach a police station to start the process. The only advice common to all cases is that any case will be as strong as the evidence you gather around it. A good start is following the advice provided in the previous steps of this workflow on what to document. You can find more information and examples in the Digital First Aid Kit guide to [documenting digital emergencies](/../../documentation).

Do you need help to take legal action against the tracking device owner?

 - [Yes, I need legal support](#legal_end)
 - [No, I think I've solved my issues](#resolved_end)
 - [No, but I would like to consider other surveillance scenarios](#pre-closure)

### device-behaviour-intro

> One of the most common targeted surveillance techniques to date is to infect phones or computers with spyware - malware designed to monitor and transmit activity to others. Spyware can be planted by different actors through different methods, like for instance abusive partners manually downloading a spying app (commonly referred to as stalkerware or spouseware) in the device, or organized crime sending phishing links to download an app, or by utilizing exploits in the operating system and/or applications.
>
> Some examples of surveillance indicators might include suspicious apps you found on your device with permissions for microphone, camera, network access, etc. Noticing the webcam usage indicator while you are not using any application that should operate it, or having the content of files in the device "leaked".
>
> For such cases, you can refer to the Digital First Aid Kit workflow "My device is acting suspiciously".

What would you like to do?

 - [Take me to the device acting suspiciously workflow](../../../device-acting-suspiciously)
 - [Take me to the next step here](#pre-closure)

### suspicious-redirections-intro

> Sometimes the web-browser displays a security alert when we try to manually download updates for applications or the operating system of our device from the web. Such a warning would usually mention something about a certificate, such as "mismatching certificate for the domain", or "expired certificate" or simply, and vaguely, "failed authentication".

> There are various reasons for this, and they can be legitimate or malicious. Some guiding questions we should ask ourselves are:
>
> - Does my device have an accurate date and time? The way web-browsers establish trust with web-servers, in order to secure the connection between them, depends on checking security certificates that are valid within a specific timeframe. If, for instance, your computer is configured with an earlier year, these security checks will fail for a number of websites and services. If your date and time are accurate, it might also suggest the administrator of the website hadn't updated their certificates, or there is something suspicious going on. In any case, it is a good rule of thumb never to update or operate applications in your device if you see these security warnings or redirections.
> - Did the problem appear after an uncommon event, like clicking an ad, installing an application, or opening a document? Many attacks rely on impersonating processes like legitimate software installations and updates, so this might be your device figuring it out and giving a warning or stopping the proccess entirely.
> - Is the process you are having issues with happening in an unusual way, e.g. normally the update process of that application follows a different process than the one you are seeing?
>
> In any case, don't forget to document everything in the most detailed way possible, especially if you want to receive specialized help, conduct a deeper investigation, or conduct any legal action.
>
> In almost all cases, addressing the root cause of the problem will be enough before returning the device and the navigation back to normal. However, there might be further problems if you actually installed or updated an application (or the operating system) in suspicious circumstances.

Did you click and download, or install any software or updates in suspicious conditions?

 - [Yes](#pre-closure)
 - [No](#suspicious-software-installed)

### suspicious-software-installed

> In most cases, attacks hijacking the installation or update processes of applications or operating systems are aimed at installing malware in the system. If you suspect this is your scenario, we recommend going to the Digital First Aid Kit workflow "My device is acting suspiciously".

What would you like to do?

 - [Take me to the device acting suspiciously workflow](../../../device-acting-suspiciously)
 - [Take me to the next step here](#pre-closure)

### security-alerts-intro

> if you are receiving messages or notification alerts from email services, social networking platforms, or any other online service you actually use, and they are related to security issues like new suspicious logins from new devices or locations, you can first check if the message is legitimate or if it is a phishing message. To do this, we recommend checking the Digital First Aid Kit workflow [I Received a Suspicious Message](../../../suspicious-messages) before going forward.
>
> If the message is legitimate, the next question you should ask yourself is if there is a reason why this message might be legitimate. For instance, if you use a VPN service or Tor, and navigate to the web version of a service, it will think you are located in the country of your VPN server or Tor exit node, triggering an alert of a new suspicious login, when it is yourself just using a cimcurvention solution.
>
> Another useful thing to ask ourselves is what kind of security alert we are receiving: is it about someone trying to log into your account or about them succeeding? Depending on the answer, the response required might be very different.
>
> If we received a notification about unsuccesful login attempts, and we have a good password (long, not shared with other insecure services, etc.) and use multi-factor authentication (also known as MFA or 2FA), we shouldn't worry too much of getting the account compromised. Regardless of how serious the account compromise threat is, it is always good to check any logs on recent activity where we can usually review our login history, including location and type of device.
>
> If you see anything suspicious in your activity logs, the most common strategies, depending on the account, include:
>
> - Changing passwords
> - Enabling MFA
> - Trying to disconnect all devices from the account
> - Documenting indicators of the hacking attempt (in case we want to look for help, investigate further, or initiate a legal action)
>
> Another important question to help assess these notifications is if we share devices or access to the account with someone else who might have triggered the alert. If this is the case, be careful with the level of access you give to others to your information and repeat the steps above if you want to revoke access for this third person.
>

If you believe there are other potential threats listed at the beginning of the workflow that you want to check, or have a more complex case and want to look for help, click Next.

 - [Next](#pre-closure)


### suspicious-device-intro

> If we have found a suspicious device, the first thing we need to ask ourselves is if this device is malicious or if it is intended to be where it is. This is because in the past years the number of devices with communication capabilities in our homes and workplaces has grown considerably: printers, smart lights, washing machines, thermostats, etc. Given the great number of these devices, we might end up being concerned about a device we don't remember setting up or that was configured by someone else but isn't malicious.
>
> In case you don't know if the device you found is malicious, one of the first things to do is getting information about it: brand, model, if it is connected to the wired network or to the wireless one, if it says what it does, if it is powered on, etc. If you still don't know what the device does, you can use this information to search on the web and learn more about what the device is and does.

After reviewing the device, do you think it is a legit one?

 - [It was a legit device](#pre-closure)
 - [I checked but still don't know what the device does or if it is malicious](#suspicious-device2)


### suspicious-device2

> In case you are still concerned about the device after an initial review, you should consider the remote possibility of it being a surveillance device. Usually these devices monitor activity in the network they are connected to, or could even record and transmit audio and/or video just as spyware does. It is important to understand that this scenario is very uncommon and is usually tied to very high-risk profiles. However, if this is concerning to you, some things you can do include:
>
> - Disconnecting the device from the network/power and checking if all the intended services around work without problems. After that you can get the device analized by someone with experience.
> - Researching it some more and looking for help (this generally involves more advanced tests like mapping the network or looking for uncommon signals).
> - As a preventative measure to mitigate any network surveillance if leaving the device connected, using VPN, Tor, or a similar tool that encrypts your web browsing, as well as communication tools with end-to-end encryption.
> - As a preventative measure to mitigate potential audio/video recording if leaving the device connected, isolating physically the device so it won't capture any valuable audio or video.

If you believe there are other potential threats listed at the beginning that you want to check, or want to look for help, click Next.

 - [Next](#pre-closure)
 - [I think I've solved my issues](#resolved_end)

### leaked-internet-info-intro

> This branch of the workflow will cover data leaked from internet services and activity. For information leaked from phone communications, please refer to the [section on leaked confidential information shared via phone calls](#leaked-phone-info-intro).
>
> In general, leaks from internet-based services might happen because of three reasons:
>
> 1. The information was public in the first place. In this case it might be useful to check out the workflow on "[doxing](../../../doxing)".
> 2. The information was leaked directly from the account (more common to "smaller" adversaries closer to the victim).
> 3. The information was leaked from the platform it was uploaded on in the first place (more common to "big" adversaries like governments and big companies).
>
> We will cover the last two cases.

Do you share devices or accounts with others?

 - [Yes](#shared-devices-or-accounts)
 - [No](#leaked-internet-info2)

### shared-devices-or-accounts

> In some cases, sharing account access with others can give unintended parties access to sensitive information, which can be then leaked easily. Also, generally when we share accounts, we need to make the process practical, often affecting the security of the account access mechanisms, like making the passwords weaker to be shared easily and disabling multifactor authentication (MFA or 2FA) to allow many people to log into the same account.
>
> One first piece of advice would be assessing who should have access to the relevant accounts or resources (like documents in cloud storage or collaboration platforms) and limit this to as few people as possible. Another useful principle is to have one account for each person, allowing everyone to configure their access settings in the most secure way possible, the granting of permissions according to each person's roles, and accountability by means of system's logs recording each user's actions.
>
> It is also recommended to check the [section on getting security messages and alerts from trusted services](#security-alerts-intro), where you can find tips to harden the accounts in case of suspicious access.

Click Next to explore your situation more in detail.

 - [Next](#leaked-internet-info2)

### leaked-internet-info2

Can your adversary control or access the online services you use and store the information leaked? (*This usally apply to governments, law enforcement agencies, or the providers themselves*)

 - [Yes](#control-of-online-services)
 - [No](#leaked-internet-info3)

### control-of-online-services

> Sometimes the platforms we use are easily accessed or even controlled by adversaries. Some common questions you can ask yourself are:
>
> - Who owns the service?
> - Is it an adversary?
> - Does the adversary have access to the servers or data through legal data enquiries?
>
> In that case, it is advisable to move the information to a different platform not controlled by the adversary to avoid future leaks.
>
> Other useful indicators could be:
>
> - login activity (including geographic locations), if available
> - Strange behaviour of read and unread elements
> - Configurations affecting what is done to messages or information, like redirections, rules to bounce emails, etc., when available
>
> You can also check if your messages are read by someone unintended by using Canary tokens, like the ones we can generate in [canarytokens.org](https://canarytokens.org/generate)

Click Next to explore your situation more in detail.

 - [Next](#leaked-internet-info3)

### leaked-internet-info3

Does your adversary have access to surveillance equipment and capacity to deploy it close to you?

 - [Yes](#leaked-internet-info4)
 - [No](#pre-closure)

### leaked-internet-info4

> More powerful adversaries may have the capacity to deploy specialized surveillance equipment around their victims, like planting tracking devices or infecting your own devices with spyware.

Do you think you might be tracked by a tracking device or your computer or mobile may have been infected with malware to spy on you?

 - [I would like to learn more about tracking devices](#tracking-device-intro)
 - [I'd like to learn more about the possibility of my device being compromised with malware](#device-behaviour-intro)

### leaked-phone-info-intro

> As described in the introduction of this workflow, massive phone surveillance is an ubiquitous threat in most (if not all) regions and countries. As a baseline, we need to be aware that it is really easy for carriers (and anyone with control or access to them) to check information like:
>
> - Historic location (every time the phone is on and connected to the network), data from the mobile phone towers the phone connected to
> - Call logs (who calls whom, when, and for how long)
> - SMS (both logs and the actual content of the messages)
> - Internet traffic (this is less common and less useful for adversaries, but it would be possible to infer from this which applications are used when, which websites are consulted, etc.)
>
> Another scenario that is still observed in some contexts, while uncommon, is the use of physical interception devices called stingrays or IMSI-Catchers. These devices impersonate a legitimate cell network tower to redirect through them the phone traffic of people in a close physical area of reach. While the vulnerabilities enabling these devices are being resolved or mitigated with newer protocols (4G/5G), it is still possible to jam the signal to make the phones believe the only available protocol is 2G (the most widely abused by these devices). For this case, there are some indicators with some reliability, especially being "downgraded" to 2G in areas where there should be 4G or 5G or where other people around are connected to the newer protocols without issues. For more information you can check the [FADe Project website](https://fadeproject.org/?page_id=36). Also, you can ask yourself if your adversary has proven access to this kind of device, among other things.
>
> As a general recommendation for these scenarios, it is advisable for at-risk individuals to migrate communications to encrypted channels relying on internet instead of conventional phone calls and SMSs. Same with using VPN or Tor for sensitive internet traffic (including applications). The big unresolved aspect would be the tracking of the user location, which is unavoidable while staying connected to the phone network.
>
> Regarding massive data collection through carriers, in some jurisdictions users can request which data about them was shared in the past. you can research if this applies to you and if that information will be reliable to detect potential surveillance. Keep in mind that this involves usually a legal inquiry.

If you would like to check other potential threats or have a more complex case and want to look for help, click Next.

 - [Next](#pre-closure)


### peer-compromised-intro

> In case any person or organization related to you has been a victim of surveillance, we recommend you to try to understand the techniques that were used on them, and select in the previous list the closest ones, so you can receive more relevant context and decide if you also could be a victim of similar techniques.

What would you like to do?

 - [Take me to the previous step](#start)
 - [I'm not sure](#pre-closure)

### other-intro

> The list that you saw previously included the most common cases seen in the civil space by helpers, but your situation may not be covered there. If that is your case, click "I need help"; otherwise, you can go back to the list of most common cases and pick the closest scenario applicable to you to get more specific guidance.

What would you like to do?

 - [Take me to the list of potential threats](#start)
 - [I need help](#help_end)

### pre-closure

> We hope this workflow has been helpful so far. You can choose to go back to the initial list of scenarios to check other threats you might be concerned about, or go to the list of organizations that you might contact to help you with more complex cases.

What would you like to do?

 - [I would like to learn about other surveillance techniques](#start)
 - [I think my need isn't covered in this section or it is too complex](#help_end)
 - [I think I have a clear idea of what is happening to me](#resolved_end)


### help_end

> If you need additional help in dealing with surveillance cases, you can reach out to the organizations listed below.
>
> *Note: when you contact any organization to look for help, please share the path you followed here and any indicator you found to make easier any next step.*

:[](organisations?services=legal&services=vulnerabilities_malware&services=forensic)

### legal_end

> If you need support to sue someone for illegally spy on you, you can reach out to one of the organizations below.

:[](organisations?services=legal)

### resolved_end

Hopefully this troubleshooting guide was useful. Please give feedback [via email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

- Migrate any communications from unsecured channels like phone calls and SMS to encrypted communication channels relying on internet, like end-to-end encrypted messenger apps and websites protected through HTTPS.
- If you fear your internet traffic is being monitored and you believe this can be a security risk, consider connecting to the internet through an encryption tool like a VPN or Tor.
- Keep track of all the devices present in your spaces, especially those connected to the internet, and try to know what they do.
- Avoid sharing sensitive information through servicea controlled by an adversary.
- Minimize sharing your accounts or devices as much as you can.
- Be aware of suspicious messages, websites, downloads, and applications.

#### resources

- [Apple’s Android App to Scan for AirTags is a Necessary Step Forward, But More Anti-Stalking Mitigations Are Needed](https://www.eff.org/es/deeplinks/2021/12/apples-android-app-scan-airtags-necessary-step-forward-more-anti-stalking).
- [Privacy for Students](https://ssd.eff.org/module/privacy-students): With references of surveillance techniques in schools that can apply to many other scenarios.
- [Security in a Box: Protect the privacy of your online communication](https://securityinabox.org/en/communication/private-communication/)
- [Security in a Box: Visit blocked websites and browse anonymously](https://securityinabox.org/en/internet-connection/anonymity-and-circumvention/)
- [Security in a Box: Recommendations for encrypted chat tools](https://securityinabox.org/en/communication/tools/#more-secure-text-voice-and-video-chat-applications)
