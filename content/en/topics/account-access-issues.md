---
layout: page
title: "I cannot access my account"
author: RaReNet
language: en
summary: "Are you having a problem accessing an email, social media or web account? Does an account show activity that you do not recognize? There are many things you can do to mitigate this problem."
date: 2023-05
permalink: /en/topics/account-access-issues/
parent: /en/
---

# I lost access to my account

Social media and communications accounts are widely used by civil society members to communicate, share knowledge and advocate their causes. As a consequence, these accounts are highly targeted by malicious actors, who often try to compromise these accounts, causing harm to civil society members and their contacts.

This guide is here to help you in case you have lost access to one of your accounts because it was compromised.

Here is a questionnaire to identify the nature of your problem and find possible solutions.

## Workflow

### Password-Typo

> Sometimes you might not be able to log into your account because you are mistyping the password, or because your keyboard language setting is not the one you usually use or you have CapsLock on.
>
> Try writing your username and password in a text editor and copying them from the editor to paste them into the log-in form.

Did the above suggestion help you log into your account?

- [Yes](#resolved_end)
- [No](#account-disabled)

### account-disabled

> Sometimes you might not be able to log into your account because it has been blocked or disabled by the platform due to violations to Terms of Service or platform rules. This can happen when your account is massively reported, or when the platform's reporting and support mechanisms are abused with the intention of censoring content online.
>
> If you are seeing a message of your account being locked, restricted, disabled or suspended, and you believe this is a mistake, follow any appeal mechanism that is provided with the message. You can find information on how to submit appeals at the following links:
>
> - [Facebook](https://www.facebook.com/help/185747581553788)
> - [Instagram](https://help.instagram.com/366993040048856)
> - [Twitter](https://help.twitter.com/en/forms/account-access/appeals/redirect)
> - [Youtube](https://support.google.com/youtube/answer/2802168)

Did the above suggestion help you log into your account?

- [Yes](#resolved_end)
- [No](#what-type-of-account-or-service)

### what-type-of-account-or-service

What type of account or service have you lost access to?

- [Facebook](#Facebook)
- [Facebook Page](#Facebook-Page)
- [Twitter](#Twitter)
- [Google/Gmail](#Google)
- [Yahoo](#Yahoo)
- [Hotmail/Outlook/Live](#Hotmail)
- [ProtonMail](#ProtonMail)
- [Instagram](#Instagram)
- [TikTok](#Tiktok)
  <!--- - [AddOtherServiceLink](#service-Name) -->

### Facebook-Page

Does the page have other admins?

- [Yes](#Other-admins-exist)
- [No](#Facebook-Page-recovery-form)

### Other-admins-exist

Do the other admin(s) have the same issue?

- [Yes](#Facebook-Page-recovery-form)
- [No](#Other-admin-can-help)

### Other-admin-can-help

> Please ask other admins to add you to the page admins again.

Did this fix the issue?

- [Yes](#Fb-Page_end)
- [No](#account_end)

### Facebook-Page-recovery-form

> Please log into Facebook and use [Facebook's form to recover the page](https://www.facebook.com/help/contact/164405897002583). If you can not log into your Facebook account, please go through the [Facebook account recovery workflow](#Facebook)
>
> Please note that it might take some time to receive a response to your requests. Save this page in your bookmarks and come back to this workflow in a few days.

Has the recovery procedure worked?

- [Yes](#resolved_end)
- [No](#account_end)

<!---=========================================================
//GoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogle
=========================================================-->

### Google

Do you have access to the connected recovery email/mobile?

- [Yes](#I-have-access-to-recovery-email-google)
- [No](#Recovery-Form-google)

### I-have-access-to-recovery-email-google

> Check the inbox of your recovery email to see if you received a "Critical security alert for your linked Google Account" email or SMS from Google.
>
> When checking for emails always be wary of phishing attempts. If you are unsure of the legitimacy of a message, please review the [Suspicious Messages Workflow](../../../suspicious-messages/).

 Did you receive a "Critical security alert for your linked Google Account" email or SMS from Google?

- [Yes](#Email-received-google)
- [No](#Recovery-Form-google)

### Email-received-google

Once you have have verified the legitimacy of the message, review the information provided in the email. Check if there is a "recover your account" link. Is it there?

- [Yes](#Recovery-Link-Found-google)
- [No](#Recovery-Form-google)

### Recovery-Link-Found-google

> Please use the "recover your account" link to recover your account. When you follow the link, double-check that the URL you are visiting is in fact a "google.com" address.

Were you able to recover your account?

- [Yes](#resolved_end)
- [No](#Recovery-Form-google)

### Recovery-Form-google

> Please try following the instructions in ["How to recover your Google Account or Gmail"](https://support.google.com/accounts/answer/7682439?hl=en).
>
> Please note that it might take some time to receive a response to your requests. Save this page in your bookmarks and come back to this workflow in a few days.

Has the recovery procedure worked?

- [Yes](#resolved_end)
- [No](#account_end)

<!---=========================================================
//YahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahoo
=========================================================-->

### Yahoo

Do you have access to the connected recovery email/mobile?

- [Yes](#I-have-access-to-recovery-email-yahoo)
- [No](#Recovery-Form-Yahoo)

### I-have-access-to-recovery-email-yahoo

> Check the recovery email inbox to see if you received a "Password change for your Yahoo account" email from Yahoo.
>
> When checking for emails always be wary of phishing attempts. If you are unsure of the legitimacy of a message, please review the [Suspicious Messages Workflow](../../../suspicious-messages/).

 Did you receive a "Password change for your Yahoo account" email from Yahoo?

- [Yes](#Email-received-yahoo)
- [No](#Recovery-Form-Yahoo)

### Email-received-yahoo

Once you have have verified the legitimacy of the message, review the information provided in the email. Please check if there is a "Recover your account here" link. Is it there?

- [Yes](#Recovery-Link-Found-Yahoo)
- [No](#Recovery-Form-Yahoo)

### Recovery-Link-Found-Yahoo

> Please use the "Recover your account here" link to recover your account.

Were you able to recover your account?

- [Yes](#resolved_end)
- [No](#Recovery-Form-Yahoo)

### Recovery-Form-Yahoo

> Please follow the instructions in ["Fix problems signing into your Yahoo account"](https://help.yahoo.com/kb/account/fix-problems-signing-yahoo-account-sln2051.html?impressions=true) to recover your account.
>
> Please note that it might take some time to receive a response to your requests. Save this page in your bookmarks and come back to this workflow in a few days.

Has the recovery procedure worked?

- [Yes](#resolved_end)
- [No](#account_end)

<!---=========================================================
TwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitter
//========================================================= -->

### Twitter

Do you have access to the connected recovery email/mobile?

- [Yes](#I-have-access-to-recovery-email-Twitter)
- [No](#Recovery-Form-Twitter)

### I-have-access-to-recovery-email-Twitter

> Check the recovery email inbox to see if you received a "Your Twitter password has been changed" email from Twitter.
>
> When checking for emails always be wary of phishing attempts. If you are unsure of the legitimacy of a message, please review the [Suspicious Messages Workflow](../../../suspicious-messages/).

Did you receive a "Your Twitter password has been changed" email from Twitter?

- [Yes](#Email-received-Twitter)
- [No](#Recovery-Form-Twitter)

### Email-received-Twitter

Once you have verified the legitimacy of the message, review the information provided in the email. Please check if the message contains a "recover your account" link. Is it there?

- [Yes](#Recovery-Link-Found-Twitter)
- [No](#Recovery-Form-Twitter)

### Recovery-Link-Found-Twitter

> Please use the "recover your account" link to recover your account.

Were you able to recover your account?

- [Yes](#resolved_end)
- [No](#Recovery-Form-Twitter)

### Recovery-Form-Twitter

> If you believe your Twitter account is compromised, try following the steps in [Help with my compromised account](https://help.twitter.com/en/safety-and-security/twitter-account-compromised).
>
> If your account is not compromised, or you have other account access issues, you can follow the steps in ["Request help restoring your account"](https://help.twitter.com/forms/restore).
>
> Please note that it might take some time to receive a response to your requests. Save this page in your bookmarks and come back to this workflow in a few days.

Has the recovery procedure worked?

- [Yes](#resolved_end)
- [No](#account_end)

<!---=========================================================
//Protonmail
//========================================================= -->

### ProtonMail

> Please follow [the instructions for resetting your password](https://protonmail.com/support/knowledge-base/reset-password/) to recover your account.
>
> Please note that if you reset your password, you won’t be able to read your existing emails and contacts, since those are encrypted with a key that is protected with the password. Old data can be recovered if you have access to a recovery file or recovery phrase by following the steps in [Recover Encrypted Message and Files](https://proton.me/support/recover-encrypted-messages-files).

Has the recovery procedure worked?

- [Yes](#resolved_end)
- [No](#account_end)

<!---==================================================================
//MicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlook
//================================================================== -->

### Hotmail

Do you have access to the connected recovery email/mobile?

- [Yes](#I-have-access-to-recovery-email-Hotmail)
- [No](#Recovery-Form-Hotmail)

### I-have-access-to-recovery-email-Hotmail

> Check if you received a "Microsoft account password change" email from Microsoft.
>
> When checking for emails always be wary of phishing attempts. If you are unsure of the legitimacy of a message, please review the [Suspicious Messages Workflow](../../../suspicious-messages/).

 Did you receive a "Microsoft account password change" email from Microsoft?

- [Yes](#Email-received-Hotmail)
- [No](#Recovery-Form-Hotmail)

### Email-received-Hotmail

Once you have verified the legitimacy of the message, review the information provided in the email. Please check if the message contains a "Reset your password" link. Is it there?

- [Yes](#Recovery-Link-Found-Hotmail)
- [No](#Recovery-Form-Hotmail)

### Recovery-Link-Found-Hotmail

> Please use the "Reset your password" link to set a new password and recover your account.

Were you able to recover your account with "Reset your password" link?

- [Yes](#resolved_end)
- [No](#Recovery-Form-Hotmail)

### Recovery-Form-Hotmail

> Please try [the "Sign In Helper Tool"](https://go.microsoft.com/fwlink/?linkid=2214157). Follow the instructions on this tool, including adding the account you are trying to recover and responding to the questions about the information available to recover.
>
> Please note that it might take some time to receive a response to your requests via web forms. Save this page in your bookmarks and come back to this workflow in a few days.

Has the recovery procedure worked?

- [Yes](#resolved_end)
- [No](#account_end)

<!---==================================================================
//FacebookMetaFacebookMetaFacebookMetaFacebookMetaFacebookMetaFacebook
//================================================================== -->


### Facebook

Do you have access to the connected recovery email/mobile?

- [Yes](#I-have-access-to-recovery-email-Facebook)
- [No](#Recovery-Form-Facebook)

### I-have-access-to-recovery-email-Facebook

> Check if you received a "Did you just reset your password?" email from Facebook.
>
> When checking for emails always be wary of phishing attempts. If you are unsure of the legitimacy of a message, please review the [Suspicious Messages Workflow](../../../suspicious-messages/).

 Did you receive a "Did you just reset your password?" email from Facebook?

- [Yes](#Email-received-Facebook)
- [No](#Recovery-Form-Facebook)

### Email-received-Facebook

Once you have verified the legitimacy of the message, review the information provided in the email. Does the email contain a message saying "This wasn't me" with a link?

- [Yes](#Recovery-Link-Found-Facebook)
- [No](#Recovery-Form-Facebook)

### Recovery-Link-Found-Facebook

> Please use the "This wasn't me" link in the message to recover your account.

Were you able to recover your account by clicking on the link?

- [Yes](#resolved_end)
- [No](#Recovery-Form-Facebook)

### Recovery-Form-Facebook

> Please try [the form to recover your account](https://www.facebook.com/login/identify).
>
> Please note that it might take some time to receive a response to your requests via web forms. Save this page in your bookmarks and come back to this workflow in a few days.

Has the recovery procedure worked?

- [Yes](#resolved_end)
- [No](#account_end)

<!--- ==================================================================
InstagramInstagramInstagramInstagramInstagramInstagramInstagramInstagram
//================================================================== -->

### Instagram

Do you have access to the connected recovery email/mobile?

- [Yes](#I-have-access-to-recovery-email-Instagram)
- [No](#Recovery-Form-Instagram)

### I-have-access-to-recovery-email-Instagram

> Check if you received a "Your Instagram password has been changed" email from Instagram.
>
> When checking for emails always be wary of phishing attempts. If you are unsure of the legitimacy of a message, please review the [Suspicious Messages Workflow](../../../suspicious-messages).

 Did you receive a "Your Instagram password has been changed" email from Instagram?

- [Yes](#Email-received-Instagram)
- [No](#Recovery-Form-Instagram)

### Email-received-Instagram

Once you have verified the legitimacy of the message, review the information provided in the email. Please check if there is a "secure your account here" link. Is it there?

- [Yes](#Recovery-Link-Found-Instagram)
- [No](#Recovery-Form-Instagram)

### Recovery-Link-Found-Instagram

> Please use the "secure your account here" link to recover your account.

Were you able to recover your account?

- [Yes](#resolved_end)
- [No](#Recovery-Form-Instagram)

### Recovery-Form-Instagram

> Please try following the instructions in ["I think my Instagram account has been hacked"](https://help.instagram.com/149494825257596?helpref=search&sr=1&query=hacked) to recover your account.
>
> Please note that it might take some time to receive a response to your requests via web forms. Save this page in your bookmarks and come back to this workflow in a few days.

Has the recovery procedure worked?

- [Yes](#resolved_end)
- [No](#account_end)

<!--- ==================================================================
TiktokTiktokTiktokTiktokTiktokTiktokTiktokTiktokTiktokTiktokTiktokTiktok
//================================================================== -->

### Tiktok

Do you have access to the connected recovery email/mobile?

- [Yes](#I-have-access-to-recovery-email-Tiktok)
- [No](#Recovery-Form-Tiktok)

### I-have-access-to-recovery-email-Tiktok

> If you have access to the recovery email, please try resetting your password by following the [Tiktok Password Reset process](https://www.tiktok.com/login/email/forget-password).

Were you able to recover your account?

- [Yes](#resolved_end)
- [No](#Recovery-Form-Tiktok)

### Recovery-Form-Tiktok

> Please try following the instructions in ["My account has been hacked"](https://support.tiktok.com/en/log-in-troubleshoot/log-in/my-account-has-been-hacked) to recover your account.

Has the recovery procedure worked?

- [Yes](#resolved_end)
- [No](#account_end)


### Fb-Page_end

It's wonderful that your problem is solved! Please read these recommendations to help you minimize the possibility of losing access to your page in the future:

- Activate 2-factor-authentication for all admins on the page.
- Assign admin roles only to people you trust and who are responsive.
- Consider having at least another admin for the page besides you, if there is someone you can trust. Keep in mind you should activate 2FA for all admin accounts.  
- Regularly review privileges and permissions on the page. Always assign the minimum level of privilege necessary for the user to conduct their work.


### account_end

If the procedures suggested in this workflow haven't helped you recover access to your account, you can reach out to the following organizations to ask for further help:

:[](organisations?services=account)

### resolved_end

Hopefully this DFAK guide was useful. Please give feedback [via email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

Please read these recommendations to help you minimize the possibility of losing access to your accounts in the future.

- It is always a good idea to turn on two-factor authentication (2FA) for your accounts on all platforms that support it.
- Never use the same password for more than one account. If you are currently doing so, you should change them, using a unique password for each of your accounts.
- Using a password manager will help you create and remember unique and strong passwords for all your accounts.
- Be cautious when using open public untrusted Wi-Fi networks, and possibly use a VPN or Tor when connecting through them.

#### resources

- [Access Now Helpline Community Documentation: Recommendations on Team Password Managers](https://communitydocs.accessnow.org/295-Password_managers.html)
- [Security Self-Defense: Protecting Yourself on Social Networks](https://ssd.eff.org/en/module/protecting-yourself-social-networks)​​​​​​​
- [Security Self-Defense: Creating Strong Passwords Using Password Managers](https://ssd.eff.org/en/module/creating-strong-passwords#0)

<!--- Edit the following to add another service recovery workflow:
#### service-name

Do you have access to the connected recovery email/mobile?

- [Yes](#I-have-access-to-recovery-email-google)
- [No](#Recovery-Form-google)

### I-have-access-to-recovery-email-google

Check if you received a "[Password Change Email Subject]" email from service_name. Did you receive it?

When checking for emails always be wary of phishing attempts. If you are unsure of the legitimacy of a message, please review the [Suspicious Messages Workflow](https://digitalfirstaid.org/en/topics/suspicious-messages/)

- [Yes](#Email-received-service-name)
- [No](#Recovery-Form-service-name

### Email-received-service-name

> Please check if there is a "recover your account" link. Is it there?

- [Yes](#Recovery-Link-Found-service-name)
- [No](#Recovery-Form-service-name)

### Recovery-Link-Found-service-name

> Please use the [Recovery Link Description](URL) link to recover your account.

Were you able to recover your account with "[Recovery Link Description]" link?

- [Yes](#resolved_end)
- [No](#Recovery-Form-service-name)

### Recovery-Form-service-name

> Please try this recovery form to recover this account: [Link to the standard recovery form].
>
> Please note that it might take some time to receive a response to your requests. Save this page in your bookmarks and come back to this workflow in a few days.

Has the recovery procedure worked?

- [Yes](#resolved_end)
- [No](#account_end)

-->
