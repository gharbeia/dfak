---
layout: page
title: "Мои данные пропали"
author: Abir Ghattas, Alexandra Hache, Ramy Raoof
language: ru
summary: "Что делать, если данные пропали"
date: 2023-04
permalink: /ru/topics/lost-data
parent: Home
---

# Мои данные пропали

Цифровые данные бывают нестабильными и обладают свойством пропадать. Есть много способов потерять информацию. Причиной может оказаться физическое повреждение устройства, окончание срока действия аккаунта, обновление программы, некорректное удаление или прекращение работы программы. Бывает, человек толком не представляет, как работает его система резервного копирования и работает ли вообще. Случается, пользователь просто забывает пароль или путь к сохранённой резервной копии.

В этом разделе Digital First Aid Kit мы пройдём по основным шагам и определим, как теряются данные и как их можно пытаться восстановить.

*Здесь мы будем в основном говорить о данных, которые хранятся на устройствах. Об онлайновых данных, логинах и паролях см. [раздел Digital First Aid Kit о проблемах с доступом к аккаунтам](../account-access-issues).*

## Workflow

### entry-point

> Здесь мы будем в основном говорить о данных, которые хранятся на устройствах. В случаях с онлайновыми данными, логинами и паролями мы будем ссылаться на другие разделы Digital First Aid Kit.
>
> Под устройствами мы подразумеваем компьютеры, мобильные устройства, внешние жёсткие диски, USB-флешки, SD-карты.

Какие данные потеряны?

- [Онлайновые](../../../account-access-issues)
- [Пароль и/или логин](../../../account-access-issues)
- [Данные на устройстве](#content-on-device)

### content-on-device

> Часто "потерянные" данные — те данные, которые просто удалили, случайно или намеренно. Проверьте папку "Корзина" ("Recycle Bin", "Trash") на своём компьютере. На устройствах Android можно найти утраченные данные в папке LOST.DIR. Если ни в какой "корзине" исчезнувших файлов нет, возможно, они были не удалены, а лишь перемещены в другое место. Воспользуйтесь функцией поиска вашей операционной системы.
>
> Попробуйте искать точное название потерянного файла. Если это не сработает, или вы не уверены в точном названии, можете использовать поиск по маске. К примеру, если вы потеряли файл docx, но не уверены насчёт названия, попробуйте искать `*.docx`. Результатом такого поиска будут файлы с расширением .docx. Отсортируйте список файлов по дате изменения, и вы определите самые свежие файлы.
>
> В дополнение к поиску пропавших файлов на своем устройстве постарайтесь вспомнить, есть ли у вас резервная копия этих данных. Может, вы отправляли файлы кому-нибудь по email, делились ими с кем-нибудь? Пересылали их почтой сами себе? Сохранили в облаке? Иногда поиск в таких местах позволяет найти потерянный файл или хотя бы какую-то из его версий.
>
> Можете повысить шансы на восстановление данных. Для этого сразу же прекратите редактировать и записывать данные на устройство. Чтобы найти и восстановить данные, подключите диск как внешнее устройство (например, по USB) к отдельному, заведомо надёжному компьютеру. Только после этого выполняйте поиск и восстановление данных. Запись на диск (в том числе скачивание данных и создание новых документов) может снизить шансы на восстановление.
>
> Проверьте также скрытые папки и файлы. Потерянная информация может оказаться там. Вот как вы можете сделать это на [Mac](https://www.macworld.co.uk/how-to/mac-software/show-hidden-files-mac-3520878/) и [Windows](https://support.microsoft.com/ru-ru/windows/%D0%BF%D1%80%D0%BE%D1%81%D0%BC%D0%BE%D1%82%D1%80-%D1%81%D0%BA%D1%80%D1%8B%D1%82%D1%8B%D1%85-%D1%84%D0%B0%D0%B9%D0%BB%D0%BE%D0%B2-%D0%B8-%D0%BF%D0%B0%D0%BF%D0%BE%D0%BA-%D0%B2-windows-10-97fbc472-c603-9d90-91d0-1166d1d9f4b5). Если у вас Linux, откройте файловый менеджер, зайдите в домашнюю папку и нажмите Ctrl + H.

Как вы потеряли данные?

- [Устройство, где хранились данные, было физически повреждено](#tech-assistance_end)
- [Устройство, где хранились данные, было украдено/потеряно/изъято](#device-lost-theft_end)
- [Данные были удалены](#where-is-data)
- [Данные исчезли после обновления программы](#software-update)
- [Система или какая-то программа внезапно закрылась, и данные пропали](#where-is-data)


### software-update

> При обновлении программы или всей операционной системы иногда случаются непредвиденные проблемы. Это может помешать нормальной работе программ или операционной системы. Возможны и другие неприятности, включая повреждение данных. Если ваши данные пропали сразу после одного из таких обновлений, подумайте о том, чтобы "откатиться" до предыдущего состояния. Такой "откат" (восстановление системы) вполне может решить вашу проблему. Программы (база данных) будут приведены к тому "здоровому" состоянию, которое они имели до возникновения неприятностей.
>
> Всегда ли возможно восстановление программы до предыдущего состояния? Это зависит от самой программы. Иногда восстановление оказывается простым делом, иногда это невозможно, а иногда приходится поработать. Есть смысл поискать ответ в интернете: насколько легко "откатить" конкретную программу. В операционных системах вроде Windows и Mac есть функционал восстановления до предыдущей версии.
>
> - Если у вас компьютер Mac, посетите [Официальную службу поддержки Apple](https://support.apple.com/ru-ru) и попробуйте поискать информацию о возврате к нужной вам версии macOS.
> - На компьютерах Windows решение зависит от того, хотите ли вы отказаться от какого-то конкретного обновления или "откатить назад" всю систему. Для обеих ситуаций инструкции можно найти в [Службе поддержки Microsoft](https://support.microsoft.com/ru-ru). Например, для Windows 11 зайдите на [эту страницу](https://support.microsoft.com/ru-ru/help/12373/windows-update-faq) и поищите в списке пункт "Как удалить установленное обновление".

"Откатились", помогло?

- [Да](#resolved_end)
- [Нет](#where-is-data)

### where-is-data

> Регулярное резервное копирование — хорошая практика. Иногда человек забывает, что у него включено автоматическое резервное копирование, так что лучше проверить настройки и точно выяснить, включена эта функция или нет. Если вы не делаете резервную копию прямо сейчас, задумайтесь о том, чтобы составить план регулярного создания резервных копий. О том, как настраивать резервное копирование, см. наши советы [ближе к концу этого раздела](#resolved_end).

Хотите узнать, есть ли у вас резервная копия потерянных данных? Давайте сначала вспомним, где хранились эти данные.

- [На внешних носителях (внешнем жестком диске, USB-флешке, SD-карте)](#storage-devices_end)
- [На компьютере](#computer)
- [На мобильном устройстве](#mobile)

### computer

Какая операционная система установлена на вашем компьютере?

- [macOS](#macos-computer)
- [Windows](#windows-computer)
- [Gnu/Linux](#linux-computer)

### macos-computer

> Узнать, включена ли на устройстве macOS опция создания резервных копий (и применить её для восстановления данных), можно, если проверить настройки [iCloud](https://support.apple.com/ru-ru/HT208682) или [Time Machine](https://support.apple.com/ru-ru/HT201250). Посмотрите, есть ли там доступные резервные копии.
>
> Как вариант, можно поискать в "Недавно использованных объектах". Там хранится история всех приложений, файлов и серверов, которые вы открывали во время нескольких последних сессий работы на компьютере. Чтобы найти файл и открыть его, зайдите в меню с изображением яблока (в левом верхнем углу), выберите "Недавно использованные объекты" и посмотрите список файлов. Подробнее можно прочесть в [официальном руководстве пользователя macOS](https://support.apple.com/ru-ru/guide/mac-help/mchlp2305/mac).

Вам удалось обнаружить свои данные и восстановить их?

- [Да](#resolved_end)
- [Нет](#macos-restore)

### macos-restore

> Иногда найти и восстановить потерянные материалы можно с помощью бесплатных программ с открытым кодом. Их функционал может быть ограничен. Большинство известных программ восстановления данных — платные.
>
> Примеры: [EaseUS Data Recovery Wizard](https://www.easeus.com/mac-data-recovery-software/drw-mac-free.htm), [CleverFiles Disk Drill Data Recovery Software](https://www.cleverfiles.com/).

Помогла ли эта информация восстановить ваши данные (полностью или частично)?

- [Да](#resolved_end)
- [Нет](#tech-assistance_end)

### windows-computer

> О том, как удостовериться, что создание резервных копий включено на вашем Windows-компьютере, написано на сайте техподдержки Microsoft в материале ["Резервное копирование и восстановление в Windows"](https://support.microsoft.com/ru-ru/windows/%D1%80%D0%B5%D0%B7%D0%B5%D1%80%D0%B2%D0%BD%D0%BE%D0%B5-%D0%BA%D0%BE%D0%BF%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5-%D0%B8-%D0%B2%D0%BE%D1%81%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5-%D0%B2-windows-10-352091d2-bb9d-3ea3-ed18-52ef2b88cbef).
>
> Windows имеет функцию **Timeline** ("Временная шкала"). Она задумана для повышения продуктивности работы. Шкала включает историю использованных вами файлов, посещённых сайтов и прочих действий на компьютере. Если не можете вспомнить, куда сохранили документ, нажмите на значок "Представление задач" в панели задач Windows 10. Вам откроется список, упорядоченный по дате. Найдите нужный объект и выберите его. Так можно, в частности, отыскать переименованный файл. Если функция "Временной шкалы" включена, некоторые ваши действия — к примеру, файлы, которые вы редактировали в Microsoft Office — также могут синхронизироваться с вашим мобильным устройством или другим вашим компьютером. Таким образом, вы будете иметь резервную копию на другом устройстве. Подробнее о "Временной шкале" и о том, как её использовать, рассказано в [официальном руководстве](https://support.microsoft.com/ru-ru/windows/get-help-with-timeline-febc28db-034c-d2b0-3bbe-79aa0c501039).

Вам удалось обнаружить свои данные и восстановить их?

- [Да](#resolved_end)
- [Нет](#windows-restore)


### windows-restore

> В некоторых случаях помогают бесплатные программы с открытым кодом, которые умеют находить утраченные данные и восстанавливать их. Порой их функционал ограничен. Большинство программ в этом классе — платные.
>
> Например, можете попробовать [PhotoRec](https://www.cgsecurity.org/wiki/PhotoRec_RU) (см. [пошаговую инструкцию](https://www.cgsecurity.org/wiki/PhotoRec_Шаг_за_шагом)), [EaseUS Data Recovery Wizard](https://www.easeus.com/datarecoverywizard/free-data-recovery-software.htm), [CleverFiles Disk Drill Data Recovery](https://www.cleverfiles.com/ru/data-recovery-software.html) или [Recuva](https://www.ccleaner.com/recuva).

Была ли эта информация полезна для полного или частичного восстановления данных?

- [Да](#resolved_end)
- [Нет](#tech-assistance_end)


### linux-computer

> В некоторых популярных сборках Linux, таких как Ubuntu, есть встроенная программа создания резервных копий. Пример такой программы для Ubuntu — [Déjà Dup](https://wiki.gnome.org/action/show/Apps/DejaDup). Если программа резервного копирования встроена в операционную систему, при первом запуске компьютера вам могут предложить настроить автоматическое резервное копирование. Поищите в интернете данные о том, есть ли у вашей сборки Linux встроенная программа резервного копирования, как убедиться, что она включена, и как восстановить данные из резервных копий.
>
> Чтобы узнать, включено ли на вашем компьютере автоматическое резервное копирование, загляните в статью [Ubuntu & Deja Dup — Get up, backup](https://www.dedoimedo.com/computers/ubuntu-deja-dup.html. О том, как восстановить потерянные данные из резервной копии, рассказывается в материале ["Полное восстановление системы с помощью Déjà Dup"](https://wiki.gnome.org/Apps/DejaDup/Help/Restore/Full).

Эта информация помогла восстановить ваши данные (полностью или частично)?

- [Да](#resolved_end)
- [Нет](#tech-assistance_end)


### mobile

Какая операционная система на вашем устройстве?

- [iOS](#ios-phone)
- [Android](#android-phone)


### ios-phone

> Возможно, автоматическое резервное копирование включено у вас через iCloud или iTunes? В статье ["Восстановление данных устройства iPhone, iPad или iPod touch из резервной копии"](https://support.apple.com/ru-ru/HT204184) рассказывается, как проверить, есть ли у вас резервные копии. И восстановить данные, если нужно.

Вы нашли резервную копию и восстановили данные?

- [Да](#resolved_end)
- [Нет](#phone-which-data)

###  phone-which-data

Что именно вы потеряли?

- [Данные приложений (контакты, новости и др.)](#app-data-phone)
- [Пользовательские данные (фото, видео, аудио, заметки)](#ugd-phone)

### app-data-phone

> Мобильное приложение — программа, которая работает на мобильном устройстве. К большинству приложений, впрочем, можно получить доступ и из десктопного браузера. Если вы заметили исчезновение созданных приложениями данных в вашем мобильном телефоне, попробуйте добраться до них с настольного компьютера (зайдите в веб-версию приложения, используя логин и пароль для мобильного приложения). Иногда так можно отыскать и вернуть потерянные данные.

Эта информация помогла восстановить ваши данные (полностью или частично)?

- [Да](#resolved_end)
- [Нет](#tech-assistance_end)

### ugd-phone

> Пользовательские данные создаются вами, когда вы работаете в том или ином приложении. Если данные пропали, есть смысл проверить приложение: возможно, в нём включена функция автосохранения. Если так, это поможет восстановить данные. К примеру, вы используете WhatsApp на мобильном телефоне. Внезапно вы видите, что сообщения чата пропали, а может, произошла какая-то иная ошибка. У вас получится восстановить исчезнувшие сообщения, если вы включили соответствующую опцию WhatsApp или параллельно делали заметки на важные или персональные темы в другом приложении. Иногда бывает, что опция создания резервных копий включена по умолчанию, а вам об этом специально не сообщается.

> Большинство пользовательских данных, которые созданы в приложениях и хранились на телефоне, будет включено в резервную копию (на локальном диске или в облаке). См. советы по тому, как делать резервную копию устройства, в конце этого раздела.

Эта информация помогла восстановить ваши данные (полностью или частично)?

- [Да](#resolved_end)
- [Нет](#tech-assistance_end)


### android-phone

> У Google есть встроенный в Android сервис под названием Android Backup Service. По умолчанию этот сервис создаёт резервные копии нескольких типов данных и связывает их с сервисом Google, доступ к которому можно получить из браузера. В настройках системы найдите пункт Google (или подобный), выберите свой адрес Gmail, и далее настройки синхронизации. Если вы потеряете данные, синхронизированные с вашим аккаунтом Google, то, возможно, вам удастся зайти в этот аккаунт с помощью браузера.

Эта информация помогла восстановить ваши данные (полностью или частично)?

- [Да](#resolved_end)
- [Нет](#phone-which-data)


### tech-assistance_end

> Если потеря данных возникла из-за физического повреждения (к примеру, устройство упало на пол или в воду, через него пропустили ток высокого напряжения или что-то подобное), скорее всего, понадобится восстанавливать данные на этом устройстве. Не знаете, как подступиться к этой задаче? Обратитесь к IT-специалисту, у которого есть необходимое оборудование. Решение может зависеть от обстоятельств. В частности, от того, насколько важными являются данные. Иногда не стоит торопиться в ремонтную мастерскую. Разве что там работает ваш знакомый, которому можно доверять.


### device-lost-theft_end

> Если устройство изъято, потеряно или украдено, постарайтесь побыстрее сменить пароли. В нашем руководстве есть некоторые дополнительные рекомендации на случай [утраты устройства](../../../lost-device).
>
> Если вы гражданский активист и нуждаетесь в помощи для покупки нового устройства вместо потерянного, можете связаться с организациями, оказывающими такого рода помощь.

:[](organisations?services=equipment_replacement)

### storage-devices_end

> Когда вы сталкиваетесь с задачей восстановления данных, помните: время играет большую роль. Файл, который вы случайно удалили несколько часов назад, обычно больше шансов восстановить, чем тот, который пропал несколько месяцев назад.
>
> Попытаться восстановить потерянные (удаленные или поврежденные) данные можно с помощью программы типа [PhotoRec](https://www.cgsecurity.org/wiki/PhotoRec_RU) (для Windows, Mac или Linux — см. [пошаговую инструкцию](https://www.cgsecurity.org/wiki/PhotoRec_Шаг_за_шагом)), [EaseUS Data Recovery Wizard](https://www.easeus.com/data-recovery-software/) (для Windows, Mac, Android и iOS), [CleverFiles Disk Drill Data Recovery Software](https://www.cleverfiles.com/ru/data-recovery-software.html) (для Windows или Mac) или [Recuva](https://www.ccleaner.com/recuva) (для Windows). Имейте в виду: эти программы не гарантируют успех. Возможно, ваша операционная система уже записала какие-то данные поверх нужной информации. Вот почему так важно минимизировать действия с компьютером после того, как файл потерян, и до попытки его восстановить с помощью одной из упомянутых программ.
>
> Чтобы повысить шансы восстановления данных, немедленно прекратите использовать свои устройства. Если вы продолжите запись на жёсткий диск, шансы найти и восстановить данные могут уменьшиться.
>
> Ничего не сработало? Возможно, придется восстанавливать данные с жёсткого диска. Не знаете, как подступиться к этой задаче? Обратитесь к IT-специалисту, у которого есть необходимое оборудование. Впрочем, решение может зависеть от ситуации, в частности, от того, насколько деликатными являются данные. Иногда не стоит торопиться в ремонтную мастерскую. Разве что там работает ваш знакомый, которому можно доверять.


### resolved_end

Надеемся, DFAK был вам полезен. Пожалуйста, потратьте немного времени на обратную связь по [email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com).


### final_tips

- Делать резервные копии всегда правильно. Копии разных данных, много копий, и чтобы они хранились не там же, где оригинальные данные! В зависимости от обстоятельств рассмотрите варианты хранения резервных копий в облаке или на физическом внешнем накопителе, который не будет соединён с компьютером, когда тот подключается к Интернету.
- Какой бы вариант резервного копирования вы ни выбрали, копии нужно защищать шифрованием. Делайте периодические инкрементные резервные копии самых важных данных. Убедитесь, что у вас есть доступ к этим резервным копиям. Протестируйте собственную способность восстанавливать данные, прежде чем обновлять программы или операционную систему.
- Создайте структуру папок. У каждого человека свой способ организовывать данные. Единого стандарта нет. Тем не менее, важно, чтобы у вас была удобная система папок. С такой системой вы сильно упростите себе жизнь. Вам будет легче представлять, например, для каких папок и файлов нужно часто делать резервные копии, где находится важная рабочая информация, где — персональные данные (в том числе деликатного характера) о вас и тех, с кем вы работаете, и так далее. Засучите рукава: вам понадобится некоторое время, чтобы оценить, с какими данными и как вы намереваетесь работать в дальнейшем, какая система папок поможет наилучшим образом привести их в порядок.


#### resources

- [Теплица социальных технологий: "Делаем резервные копии"](https://te-st.org/2022/03/23/making-backups/)
- [Access Now Helpline Community Documentation: "Secure back up"](https://communitydocs.accessnow.org/182-Secure_Backup.html)
- [Security in a Box: "Back-up Tactics"](https://securityinabox.org/en/files/backup/)
- [Официальная страница о резервном копировании на Mac](https://support.apple.com/ru-ru/mac-backup)
- [Резервное копирование и восстановление в Windows](https://support.microsoft.com/ru-ru/windows/%D1%80%D0%B5%D0%B7%D0%B5%D1%80%D0%B2%D0%BD%D0%BE%D0%B5-%D0%BA%D0%BE%D0%BF%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5-%D0%B8-%D0%B2%D0%BE%D1%81%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5-%D0%B2-windows-352091d2-bb9d-3ea3-ed18-52ef2b88cbef)
- [Как регулярно делать резервные копии на iPhone](https://support.apple.com/ru-ru/HT203977)
- [Как создать резервную копию данных или восстановить их на устройстве Android](https://support.google.com/android/answer/2819582?hl=ru)
- [Securily backup your data](https://johnopdenakker.com/securily-backup-your-data/)
- [How to Back Up Your Digital Life](https://www.wired.com/story/how-to-back-up-your-digital-life/)
- [Wikipedia, о восстановлении данных](https://ru.wikipedia.org/wiki/%D0%92%D0%BE%D1%81%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5_%D0%B4%D0%B0%D0%BD%D0%BD%D1%8B%D1%85)