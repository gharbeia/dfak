---
layout: page.pug
title: "О проекте"
language: ru
summary: "О DFAK."
date: 2023-05
permalink: /ru/about/
parent: Home
---

Комплект экстренной цифровой помощи (Digital First Aid Kit, DFAK) — совместный проект [RaReNet (Rapid Response Network)](https://www.rarenet.org/) и [CiviCERT](https://www.civicert.org/).

<iframe src="https://archive.org/embed/dfak-tech-demo" width="640" height="480" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

Сеть быстрого реагирования (Rapid Response Network) — международное сообщество тех, кто оказывает оперативную помощь по вопросам цифровой безопасности, и продвинутых пользователей. Мы объединяем Access Now, Amnesty Tech, Center for Digital Resilience, CIRCL, EFF, Freedom House, Front Line Defenders, Global Voices, Greenhost, Hivos & the Digital Defenders Partnership, Internews, La Labomedia, Open Technology Fund, Virtualroad. Сеть включает и отдельных экспертов, которые работают в сфере цифровой безопасности и оперативной поддержки.

Некоторые из упомянутых организаций и частных лиц входят в CiviCERT. Это международная сеть помощников по цифровой безопасности и провайдеров. CiviCERT занимается главным образом поддержкой активистских групп и организаций, работающих ради социальной справедливости и защиты прав человека (в том числе цифровых прав). CiviCERT — профессиональный лейбл для разнообразных инициатив CERT (Компьютерной группы реагирования на чрезвычайные ситуации, Computer Emergency Response Team). CiviCERT аккредитована Trusted Introducer, Европейской сетью компьютерных групп реагирования на чрезвычайные ситуации.

Digital First Aid Kit — [проект с открытым исходным кодом и принимает пожертвования на развитие](https://gitlab.com/rarenet/dfak).

Мы благодарны [Metamorphosis Foundation](https://metamorphosis.org.mk) за [албанскую локализацию DFAK](https://digitalfirstaid.org/sq/), а [EngageMedia](https://engagemedia.org/) за [бирманскую](https://digitalfirstaid.org/my/), [индонезийскую](https://digitalfirstaid.org/id/) и [тайскую](https://digitalfirstaid.org/th/) версии.

Если вы хотите использовать DFAK в условиях ограниченного интернета или его полного отсутствия, загрузите [офлайновую версию](https://digitalfirstaid.org/dfak-offline.zip).

Комментарии, предложения и вопросы о Digital First Aid Kit можно отправить сюда: dfak @ digitaldefenders . org

Отпечаток GPG: 1759 8496 25C1 56EC 1EB4 1F06 6CC1 888F 5D75 706B
