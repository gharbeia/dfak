---
name: Nothing2Hide
website: https://nothing2hide.org
logo: nothing2hide.png
languages: Français, English
services: in_person_training, org_security, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, forensic, individual_care, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso, land
hours: понедельник — пятница, 9:00 — 18:00 CET
response_time: 1 день
contact_methods: email, pgp, signal, web_form, wire
web_form: https://vault.tech4press.org/#/ (защищённая онлайн-форма на основе Globaleaks)
email: help@tech4press.org
pgp_key:  http://tech4press.org/fr/contact-mail/
pgp_key_fingerprint: 2DEB 9957 8F91 AE85 9915 DE94 1B9E 0F13 4D46 224B
signal: +33 7 81 37 80 08 <http://tech4press.org/fr/#signal>
wire: tech4press
---

Nothing2Hide (N2H) — ассоциация, которая ставит целью предоставить журналистам, юристам, правозащитникам и просто людям средства защиты данных и коммуникаций. Для этого N2H подыскивает подходящие технические решения и проводит тренинги. Наша миссия — сделать так, чтобы технологии служили для распространения и защиты информации ради укрепления демократий по всему миру.
