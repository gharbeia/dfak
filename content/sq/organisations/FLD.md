---
name: Front Line Defenders
website: https://www.frontlinedefenders.org/emergency-contact
logo: FrontLineDefenders.jpg
languages: Español, English, Русский, فارسی, Français, Português, Türkçe , العربية, 中文
services: grants_funding, in_person_training, org_security, digital_support, relocation, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, harassment, forensic, legal, individual_care, advocacy, censorship
beneficiaries: hrds, hros
hours: emergency 24/7, global; regular work Monday-Friday at office hours, IST (UTC+1), staff are located in various time zones in different regions
response_time: same or next day in case of emergency
contact_methods: web_form, phone, skype, email
web_form: https://www.frontlinedefenders.org/secure/comment.php
phone: +353-1-210-0489 for emergencies; +353-1-212-3750 office phone
skype: front-line-emergency?call
email: info@frontlinedefenders.org for comments or questions
initial_intake: yes
---

Front Line Defenders është një organizatë ndërkombëtare me seli në Irlandë
që punon për mbrojtjen e integruar të mbrojtësve të të drejtave të njeriut në
rrezik. Front Line Defenders ofron mbështetje të shpejtë dhe praktike për mbrojtësit e të drejtave të njeriut në rrezik të menjëhershëm përmes granteve të sigurisë, trajnimit për sigurinë fizike dhe digjitale, avokimit dhe zhvillimit të fushatave.

Front Line Defenders operon një linjë telefonike të mbështetjes emergjente 24/7
+353-121-00489 për mbrojtësit e të drejtave të njeriut në rrezik të menjëhershëm në arabisht,
anglisht, frëngjisht, rusisht ose spanjisht. Kur mbrojtësit e të drejtave të njeriut përballen me një kërcënim të menjëhershëm për jetën e tyre, Front Line Defenders mund të ndihmojë me
zhvendosje e përkohshme. Front Line Defenders ofron trajnime për sigurinë fizike dhe sigurinë digjitale. Front Line Defenders publikon gjithashtu rastet e mbrojtësve të të drejtave të njeriut në rrezik dhe zhvillon fushata dhe avokon në nivel ndërkombëtar duke përfshirë BE-në, OKB-në, mekanizmat ndërrajonalë dhe drejtpërdrejt me qeveritë për mbrojtjen e tyre.