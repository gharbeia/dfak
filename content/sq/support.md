---
layout: page.pug
language: sq
permalink: /sq/support
type: support
---

Këtu është lista e organizatave që ofrojnë lloje të ndryshme mbështetjesh. Klikoni në secilën organizatë për të zgjeruar informacionin e plotë në lidhje me shërbimet e tyre dhe si t'i kontaktoni ato.
