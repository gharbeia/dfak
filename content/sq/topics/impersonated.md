---
layout: page
title: "Dikush po më imiton në internet"
author: Floriana Pagano, Alexandra Hache
language: sq
summary: "Dikush po më imiton (shtiret sikur jam unë) përmes një llogarie të mediave sociale, adresës së e-mailit, çelësit PGP, ueb-faqes së rreme ose aplikacionit të rremë."
date: 2023-05
permalink: /sq/topics/impersonated
parent: Home
---

# Dikush po më imiton (shtiret sikur jam unë) në internet

Një kërcënim me të cilin përballen shumë aktivistë, mbrojtës të të drejtave të njeriut, OJQ, media të pavarura dhe blogues është shtirja sikur janë ata nga kundërshtarët që do të krijojnë profile të rreme, faqe interneti ose e-maile në emrat e tyre. Ndonjëherë, kjo ka për qëllim të krijojë fushata njollosëse, informacione mashtruese ose inxhinieri sociale, ose për të vjedhur identitetin e dikujt për të krijuar zhurmë, çështje besimi dhe dëmtim të të dhënave që ndikojnë në reputacionin e individëve ose kolektivëve që imitohen. Në raste të tjera, një kundërshtar mund të imitojë identitetin e dikujt në internet për motivime financiare si p.sh. për mbledhjen e fondeve, për të vjedhur kredencialet për pagesa, pranimin e pagesave, etj.

Ky është një problem shqetësues që mund të ndikojë në nivele të ndryshme në aftësinë tuaj për të komunikuar dhe informuar. Mund të ketë gjithashtu shkaqe të ndryshme në varësi të vendit ku dhe si dikush shtiret sikur jeni ju.

Është e rëndësishme të dini se ekzistojnë shumë mënyra për t'u shtirur si dikush tjetër (profile të rreme në mediat sociale, faqe të internetit të klonuara, e-maile të rreme, publikim jokonsensual i fotografive dhe videove personale). Strategjitë mund të variojnë nga dërgimi i njoftimeve për heqje të ueb-faqeve, vërtetimi i pronësisë origjinale, pretendimi i të drejtës së autorit të faqes së internetit ose informacionit origjinal, ose paralajmërimi i rrjeteve dhe kontakteve tuaja personale përmes komunikimeve publike ose konfidenciale. Diagnostifikimi i problemit dhe gjetja e zgjidhjeve të mundshme për imitimin mund të jetë e vështirë. Ndonjëherë do të jetë pothuajse e pamundur të detyroni një kompani të vogël hostingu që të heqë një faqe interneti dhe veprimi juridik mund të bëhet i domosdoshëm. Është praktikë e mirë të vendosni alarme dhe të monitoroni internetin për të zbuluar nëse dikush ju imiton juve ose organizatën tuaj.

Kjo pjesë e Veglërisë së Ndihmës së Parë Digjitale do t'ju njoftojë me disa hapa themelorë për të diagnostikuar mënyrat e mundshme të imitimit dhe strategjitë e mundshme të zbutjes për të hequr llogaritë, faqet e internetit dhe e-mailet që u imitojnë juve (shtiren sikur jeni ju) ose organizatën juaj.

Nëse dikush ju imiton, ndiqni këtë pyetësor për ta identifikuar natyrën e problemit tuaj dhe për të gjetur zgjidhje të mundshme.


## Workflow

### urgent-question

A keni frikë për integritetin fizik apo mirëqenien tuaj?

 - [Po](#physical-sec_end)
 - [Jo](#diagnostic-start1)

### diagnostic-start1

A ndikon imitimi tek ju si individ (dikush e përdor emrin dhe mbiemrin tuaj ligjor, ose pseudonimin mbi të cilin e bazoni reputacionin tuaj) apo si organizatë/kolektiv?

- [Si individ](#individual)
- [Si organizatë](#organization)

### individual

> Nëse jeni prekur si individ, mund të dëshironi të paralajmëroni kontaktet tuaja. Merrni këtë hap duke përdorur një llogari e-maili, një profil ose faqe interneti që është plotësisht nën kontrollin tuaj.

- Pasi të keni informuar kontaktet tuaja se dikush shtiret si ju (ju imiton), vazhdoni në [hapin tjetër](#diagnostic-start2).

### organization

> Nëse jeni prekur si grup, mund të dëshironi të bëni një njoftim publik. Merrni këtë hap duke përdorur një llogari e-maili, një profil ose faqe interneti që është plotësisht nën kontrollin tuaj.

- Pasi të keni informuar komunitetin tuaj se dikush shtiret si ju (ju imiton), vazhdoni në [hapin tjetër](#diagnostic-start2).

### diagnostic-start2

Në çfarë mënyre shtiren si ju (ju imitojnë)?

 - [Një faqe interneti e rreme po imiton mua ose grupin tim](#fake-website)
 - [Përmes një llogarie në rrjetet sociale](#social-network)
 - [Përmes shpërndarjes jo konsensuale të videove ose fotografive](#doxing)
 - [Përmes adresës sime të e-mailit ose një adrese të ngjashme](#spoofed-email1)
 - [Përmes një çelësi PGP të lidhur me adresën time të e-mailit](#PGP)
 - [Përmes një aplikacioni të rremë që po imiton aplikacionin tim](#app1)

### social-network

> Mund të vendosni të raportoni llogarinë ose përmbajtjen që ju imiton në platformën përkatëse ku po ndodh imitimi.
>
> ***Shënim:*** *Gjithmonë [dokumentoni](/../../documentation) përpara se të ndërmerrni veprime të tilla si fshirja e mesazheve ose e regjistrave të bisedave ose bllokimi i profileve. Nëse shqyrtoni mundësinë për veprime ligjore, duhet të konsultoni informacionin mbi [dokumentacionin ligjor](/../../documentation#legal).*

Në cilën platformë të rrjeteve sociale dikush shtiret sikur ju (ju imiton)?

- [Facebook](#facebook)
- [Instagram](#instagram)
- [TikTok](#tiktok)
- [Twitch](#twitch)
- [Twitter](#twitter)
- [YouTube](#youtube)

### facebook

> Ndiqni udhëzimet në ["Si mund të raportoj një llogari ose faqe në Facebook që pretendon se jam unë ose dikush tjetër?"](https://www.facebook.com/help/174210519303259) për të kërkuar që llogaria imituese të fshihet.
>
> Ju lutemi vini re se mund të duhet pak kohë për të marrë përgjigje për kërkesën tuaj. Ruajeni këtë faqe në faqerojtësin (bookmarks) tuaj dhe kthehuni në këtë rrjedhë pune pas disa ditësh.

A funksionoi kjo?

- [Po](#resolved_end)
- [Jo](#account_end)

### twitter

> Ndiqni hapat në ["Raporto një llogari për imitim"](https://help.twitter.com/forms/impersonation) për të kërkuar fshirjen e llogarisë imituese.
>
> Ju lutemi vini re se mund të duhet pak kohë për të marrë përgjigje për kërkesën tuaj. Ruajeni këtë faqe në faqerojtësin (bookmarks) tuaj dhe kthehuni në këtë rrjedhë pune pas disa ditësh.

A funksionoi kjo?

- [Po](#resolved_end)
- [Jo](#account_end)

### instagram

> Ndiqni udhëzimet në ["Llogaritë imituese"](https://help.instagram.com/446663175382270) për të kërkuar fshirjen e llogarisë imituese.
>
> Ju lutemi vini re se mund të duhet pak kohë për të marrë përgjigje për kërkesën tuaj. Ruajeni këtë faqe në faqerojtësin (bookmarks) tuaj dhe kthehuni në këtë rrjedhë pune pas disa ditësh.

A funksionoi kjo?

- [Po](#resolved_end)
- [Jo](#account_end)

### tiktok

> Ndiqni udhëzimet në ["Raportoni një llogari imituese"](https://support.tiktok.com/en/safety-hc/report-a-problem/report-an-impersonation-account) për të kërkuar fshirjen e llogarisë imituese.
>
> Ju lutemi vini re se mund të duhet pak kohë për të marrë përgjigje për kërkesën tuaj. Ruajeni këtë faqe në faqerojtësin (bookmarks) tuaj dhe kthehuni në këtë rrjedhë pune pas disa ditësh.

A funksionoi kjo?

- [Po](#resolved_end)
- [Jo](#account_end)

### youtube

> Ndiqni udhëzimet në ["Raporto videot, kanalet dhe përmbajtjet e tjera të papërshtatshme në YouTube"](https://support.google.com/youtube/answer/2802027) për raportimin e llogarisë imituese. Zgjidhni "Imitimi" midis arsyeve të mundshme për raportim.
>
> Ju lutemi vini re se mund të duhet pak kohë për të marrë përgjigje për kërkesën tuaj. Ruajeni këtë faqe në faqerojtësin (bookmarks) tuaj dhe kthehuni në këtë rrjedhë pune pas disa ditësh.

A funksionoi kjo?

- [Po](#resolved_end)
- [Jo](#account_end)

### twitch

> Ndiqni udhëzimet në ["Si të parashtroni një raport përdoruesi"](https://help.twitch.tv/s/article/how-to-file-a-user-report) për të raportuar llogarinë imituese dhe për të kërkuar që ajo të fshihet. Zgjidhni "Imitimi" midis kategorive të mundshme të përfshira në formular.
>
> Ju lutemi vini re se mund të duhet pak kohë për të marrë përgjigje për kërkesën tuaj. Ruajeni këtë faqe në faqerojtësin (bookmarks) tuaj dhe kthehuni në këtë rrjedhë pune pas disa ditësh.

A funksionoi kjo?

- [Po](#resolved_end)
- [Jo](#account_end)

### fake-website

> Kontrolloni nëse kjo faqe interneti njihet si keqdashëse duke kërkuar URL-në e saj në shërbimet e mëposhtme online:
>
> - [circl.lu/urlabuse](https://circl.lu/urlabuse/)
> - [Virus Total.com](https://www.virustotal.com/)
> - [sitecheck.sucuri.net](https://sitecheck.sucuri.net/)
> - [urlscan.io](https://urlscan.io/)

A dihet se domeni është keqdashës?

 - [Po](#malicious-website)
 - [Jo](#non-malicious-website)

### malicious-website

> Raporto URL-në te "Shfletimi i sigurt i Google" duke plotësuar [formularin "Raporto softuerin keqdashës"](https://safebrowsing.google.com/safebrowsing/report_badware/).
>
> Ju lutemi vini re se mund të duhet pak kohë për t'u siguruar që raporti juaj ishte i suksesshëm. Ndërkohë, mund të vazhdoni në hapin tjetër për dërgimin e një kërkese për heqje të faqes së internetit tek ofruesi i hostingut dhe regjistruesi i domenit, ose ruajeni këtë faqe në faqerojtësin (bookmarks) tuaj dhe kthehuni në këtë rrjedhë pune pas disa ditësh.

A funksionoi kjo?

- [Po](#resolved_end)
- [Jo](#non-malicious-website)


### non-malicious-website

> Mund të provoni të raportoni faqen e internetit te ofruesi i hostingut ose regjistruesi i domenit, duke kërkuar heqjen e përmbajtjes.
>
> ***Shënim:*** *Gjithmonë [dokumentoni](/../../documentation) përpara se të ndërmerrni veprime të tilla si fshirja e mesazheve ose e regjistrave të bisedave ose bllokimi i profileve. Nëse shqyrtoni mundësinë për veprime ligjore, duhet të konsultoni informacionin mbi [dokumentacionin ligjor](/../../documentation#legal).*
>
> Nëse faqja e internetit që dëshironi ta raportoni po përdor përmbajtjen tuaj, një gjë që mund t'ju duhet të dëshmoni është se ju jeni pronari legjitim i përmbajtjes origjinale. Mund ta tregoni këtë duke paraqitur kontratën tuaj origjinale me regjistruesin e domenit dhe/ose ofruesin e hostingut, por gjithashtu mund të bëni një kërkim në [Wayback Machine](https://archive.org/web/), duke kërkuar edhe URL-në e faqes suaj të internetit edhe ueb-faqen e rreme. Nëse faqet e internetit janë indeksuar atje, do të gjeni një histori që mund të bëjë të mundur të tregoni se faqja juaj e internetit ekzistonte përpara se të publikohej faqja e rreme e internetit.
>
> Për të dërguar një kërkesë për heqje të përmbajtjes, do t'ju duhet gjithashtu të grumbulloni informacion për faqen e rreme të internetit:
>
> - Shkoni te [Shërbimi NSLookup i Network Tools](https://network-tools.com/nslookup/) dhe zbuloni adresën (ose adresat) IP të faqes së rreme të internetit duke futur URL-në e saj në formularin e kërkimit.
> - Shkruani adresën ose adresat IP.
> - Shkoni te [Shërbimi Whois Lookup i Domain Tools](https://whois.domaintools.com/) dhe kërkoni si domenin ashtu edhe adresën/at IP të faqes së rreme të internetit.
> - Regjistroni emrin dhe adresën e e-mailit të abuzimit të ofruesit të hostingut dhe shërbimit të domenit. Nëse përfshihet në rezultatet e kërkimit tuaj, regjistroni edhe emrin e pronarit të faqes së internetit.
> - Shkruani ofruesit të hostingut dhe regjistruesit të domenit të faqes së rreme të internetit për të kërkuar heqjen e saj. Në mesazhin tuaj, përfshini informacion mbi adresën IP, URL-në dhe pronarin e faqes së internetit që ju imiton, si dhe arsyet pse është abuzive.
> - Mund të përdorni [Shabllonin e “Access Now Helpline” për të raportuar faqet e klonuara internetit tek ofruesi i hostingut](https://communitydocs.accessnow.org/352-Report_Fake_Domain_Hosting_Provider.html) për t'i shkruar ofruesit të hostingut.
> - Mund të përdorni [Shabllonin e “Access Now Helpline” për të raportuar imitimet ose klonimet tek ofruesi i domenit](https://communitydocs.accessnow.org/343-Report_Domain_Impersonation_Cloning.html) për t'i shkruar regjistruesit të domenit.
>
> Ju lutemi vini re se mund të duhet pak kohë për të marrë një përgjigje ndaj kërkesave tuaja. Ruajeni këtë faqe në faqerojtësin (bookmarks) tuaj dhe kthehuni në këtë rrjedhë pune pas disa ditësh.

A funksionoi kjo?

- [Po](#resolved_end)
- [Jo](#web-protection_end)


### spoofed-email1

> Për arsye teknike, është mjaft e vështirë të vërtetohen e-mailet. Kjo është gjithashtu arsyeja pse është shumë e lehtë të krijohen adresa të falsifikuara dërguesish dhe e-maile të falsifikuara.

A është duke ju imituar dikush përmes adresës tuaj të e-mailit, ose një e-maili të ngjashëm, për shembull me të njëjtin emër përdoruesi, por me domen tjetër?

- [Dikush po më imiton përmes adresës sime të e-mailit](#spoofed-email2)
- [Dikush po më imiton përmes një adrese të ngjashme e-maili](#similar-email)


### spoofed-email2

> Personi që po ju imiton mund të ketë hakuar llogarinë tuaj të e-mailit. Për të përjashtuar këtë mundësi, provoni të ndryshoni fjalëkalimin tuaj.

A jeni në gjendje të ndryshoni fjalëkalimin tuaj?

- [Po](#spoofed-email3)
- [Jo](#hacked-account)

### hacked-account

> Nëse nuk mund ta ndryshoni fjalëkalimin tuaj, llogaria juaj e e-mailit ka të ngjarë të jetë hakuar. Ju mund të ndiqni [rrjedhën e punës "Nuk mund të hyj në llogarinë time"](../../../account-access-issues) për të zgjidhur këtë problem.

A ju ndihmoi kjo për të zgjidhur problemin tuaj?

- [Po](#resolved_end)
- [Jo](#account_end)

### spoofed-email3

> Mashtrimi përmes e-mailit përbëhet nga mesazhe e-maili me një adresë të falsifikuar të dërguesit. Mesazhi duket se e ka origjinën nga dikush ose diku tjetër nga burimi aktual.
>
> Mashtrimi përmes e-mail-it është i zakonshëm në fushatat për vjedhjen e të dhënave të ndjeshme (phishing) dhe spam sepse njerëzit kanë më shumë të ngjarë të hapin një e-mail kur mendojnë se vin nga një burim i ligjshëm.
>
> Nëse dikush e përdor e-mailin tuaj për mashtrim, duhet të informoni kontaktet tuaja për t'i paralajmëruar ata në lidhje me rrezikun e vjedhjes së të dhënave të ndjeshme (phishing) (bëjeni këtë nga një llogari e-maili, profil ose faqe internet që është plotësisht nën kontrollin tuaj).
>
> Nëse mendoni se imitimi kishte për qëllim vjedhjen e të dhënave të ndjeshme (phishing) ose qëllime të tjera keqdashëse, mund të dëshironi të lexoni  gjithashtu seksionin [Kam marrë mesazhe të dyshimta](../../../suspicious_messages).

A ndaluan e-mailet pasi e ndryshuat fjalëkalimin në llogarinë tuaj të e-mailit?

- [Po](#compromised-account)
- [Jo](#secure-comms_end)


### compromised-account

> Ndoshta, llogaria juaj është hakuar nga dikush që e përdori atë për të dërguar e-maile për t'ju imituar. Duke qenë se llogaria juaj është komprometuar, mund të dëshironi të lexoni gjithashtu seksionin për zgjidhjen e problemeve [Kam humbur qasjen në llogaritë e mia](../../../account-access-issues/).

A ju ndihmoi kjo për të zgjidhur problemin tuaj?

- [Po](#resolved_end)
- [Jo](#account_end)


### similar-email

> Nëse imituesi përdor një adresë e-maili që është e ngjashme me tuajën, por me një domen tjetër ose emër tjetër përdoruesi, është ide e mirë të paralajmëroni kontaktet tuaja në lidhje me këtë përpjekje për t'ju imituar (bëjeni këtë nga një llogari e-maili, profil ose faqe interneti që është plotësisht nën kontrollin tuaj).
>
> Ju gjithashtu mund të dëshironi të lexoni seksionin [Kam marrë mesazhe të dyshimta](../../../suspicious-messages), pasi ky imitim mund të ketë për qëllim vjedhjen e të dhënave të ndjeshme (phishing).

A ju ndihmoi kjo për të zgjidhur problemin tuaj?

- [Po](#resolved_end)
- [Jo](#secure-comms_end)

### PGP

A mendoni se çelësi juaj privat PGP mund të jetë komprometuar, për shembull, sepse keni humbur kontrollin e pajisjes ku ishte ruajtur?

- [Po](#PGP i komprometuar)
- [Jo](#PGP-spoofed)

### PGP-compromised

> - Krijoni një çift të ri çelësash dhe le ta nënshkruajnë atë njerëzi të cilëve u besoni.
> - Informoni kontaktet tuaja nëpërmjet një kanali të besuar që e kontrolloni (siç është Signal ose ndonjë tjetër [mjet i koduar nga-skaji-në-skaj](https://www.frontlinedefenders.org/en/resource-publication/guide-secure-group-chat -and-conferencing-tools)) se ata duhet të përdorin çelësin tuaj të ri dhe të mos e përdorin atë vjetrin. Tregojuni atyre se mund ta njohin çelësin tuaj aktual duke u bazuar në gjurmën e gishtit të çelësit tuaj të vërtetë. Ju gjithashtu mund t'u dërgoni atyre drejtpërdrejt çelësin tuaj të ri publik përmes të njëjtit kanal të besuar që e përdorni për t'i informuar ata.

A keni nevojë për më shumë ndihmë për të zgjidhur problemin tuaj?

- [Po](#secure-comms_end)
- [Jo](#resolved_end)

### PGP-spoofed

> - Informoni kontaktet tuaja nëpërmjet një kanali të besuar që e kontrolloni, si p.sh. Signal ose ndonjë tjetër [mjet i koduar nga skaji-në-skaj](https://www.frontlinedefenders.org/en/resource-publication/guide-secure-group-chat -and-conferencing-tools), se dikush po përpiqet t'ju imitojë. Tregojuni atyre se mund ta njohin çelësin tuaj aktual duke u bazuar në gjurmën e gishtit të çelësit tuaj të vërtetë. Ju gjithashtu mund t'u dërgoni atyre drejtpërdrejt çelësin tuaj publik përmes të njëjtit kanal të besuar që e përdorni për t'i informuar ata.

A keni nevojë për më shumë ndihmë për të zgjidhur problemin tuaj?

- [Po](#secure-comms_end)
- [Jo](#resolved_end)

### doxing

> Nëse dikush po ndan informacionin tuaj personal ose videot ose fotografitë tuaja private, ju rekomandojmë të ndiqni rrjedhën e punës të Veglërisë së Ndihmës së Parë Digjitale në [Doxing and Non-Consensual Sharing of Private Media](../../../doxing).

Çfarë do të dëshironit të bënit?

- [Më dërgo në seksionin e Veglërisë së Ndihmës së Parë Digjitale mbi “Doksimin dhe Ndarjen Jo-Konsensuale të Materialeve Mediatike Private](../../../doxing)
- [Kam nevojë për mbështetje për të adresuar problemin tim](#harassment_end)

### app1

> Nëse dikush po përhap një kopje keqdashëse të aplikacionit tuaj ose softuerit tjetër, është ide e mirë të bëni një njoftim publik për të paralajmëruar përdoruesit që të shkarkojnë vetëm versionin e ligjshëm të aplikacionit.
>
> Duhet të raportoni gjithashtu aplikacionin keqdashës dhe të kërkoni heqjen e tij.

Ku shpërndahet kopja keqdashëse e aplikacionit tuaj?

- [Në Github](#github)
- [Në Gitlab.com](#gitlab)
- [Në Google Play Store](#playstore)
- [Në Apple App Store](#apple-store)
- [Në një faqe tjetër interneti](#fake-website)

### github

> Nëse softueri keqdashës është hostuar në Github, lexoni [Udhëzuesin për Parashtrimin e Njoftimit për Heqjen e Përmbajtjes sipas Ligjit të Drejtave të Autorit të Mijëvjeçarit Digjital (DMCA)] të Github-it](https://help.github.com/en/articles/guide-to-submitting-a- dmca-takedown-notice) për heqjen e përmbajtjes që i shkel të drejtat e autorit.
>
> Mund të duhet pak kohë për të marrë një përgjigje ndaj kërkesave tuaja. Ruajeni këtë faqe në faqerojtësin (bookmarks) tuaj dhe kthehuni në këtë rrjedhë pune pas disa ditësh.

A ju ndihmoi kjo për të zgjidhur problemin tuaj?

- [Po](#resolved_end)
- [Jo](#app_end)

### gitlab

> Nëse softueri keqdashës është hostuar në Gitlab.com, lexoni [Kërkesat për heqjen e përmbajtjes të Ligjit të të Drejtave të Autorit të Mijëvjeçarit Digjital (DMCA) të Gitlab-it](https://about.gitlab.com/handbook/dmca/) për heqjen e përmbajtjes që i shkel të drejtat e autorit.
>
> Mund të duhet pak kohë për të marrë një përgjigje ndaj kërkesave tuaja. Ruajeni këtë faqe në faqerojtësin (bookmarks) tuaj dhe kthehuni në këtë rrjedhë pune pas disa ditësh.

A ju ndihmoi kjo për të zgjidhur problemin tuaj?

- [Po](#resolved_end)
- [Jo](#app_end)


### playstore

> Nëse aplikacioni keqdashës ndodhet në Google Play Store, ndiqni hapat në ["Heqja e përmbajtjes nga Google"](https://support.google.com/legal/troubleshooter/1114905) për të hequr përmbajtjen që i shkel të drejtat e autorit.
>
> Mund të duhet pak kohë për të marrë një përgjigje ndaj kërkesave tuaja. Ruajeni këtë faqe në faqerojtësin (bookmarks) tuaj dhe kthehuni në këtë rrjedhë pune pas disa ditësh.

A ju ndihmoi kjo për të zgjidhur problemin tuaj?

- [Po](#resolved_end)
- [Jo](#app_end)

### apple-store

> Nëse aplikacioni keqdashës ndodhet në App Store, plotësoni formularin ["Kontestimi i përmbajtjes në Apple App Store"](https://www.apple.com/legal/internet-services/itunes/appstorenotices/#/contacts) për të hequr përmbajtjen që i shkel të drejtat e autorit.
>
> Mund të duhet pak kohë për të marrë një përgjigje ndaj kërkesave tuaja. Ruajeni këtë faqe në faqerojtësin (bookmarks) tuaj dhe kthehuni në këtë rrjedhë pune pas disa ditësh.

A ju ndihmoi kjo për të zgjidhur problemin tuaj?

- [Po](#resolved_end)
- [Jo](#app_end)

### physical-sec_end

> Nëse keni frikë për mirëqenien tuaj fizike, ju lutemi kontaktoni organizatat e mëposhtme që mund t'ju mbështesin.

:[](organisations?services=physical_sec)

### harassment_end

> Nëse informacioni ose materialet tuaja mediatike private janë publikuar pa pëlqimin tuaj dhe keni nevojë për mbështetje për të zgjidhur problemin tuaj, mund të kontaktoni me organizatat e mëposhtme që mund t'ju mbështesin.
>
> Përpara se të kontaktoni një organizatë, ju rekomandojmë fuqimisht të ndiqni seksionin [për zgjidhjen e problemeve mbi doksimin dhe publikimin jo konsensual të materialeve mediatike private](../../../doxing) të Veglërisë së Ndihmës së Parë Digjitale, pasi kjo do t'ju ndihmojë të kuptoni se çfarë po ndodh saktësisht me ju.

:[](organisations?services=harassment)

### account_end

> Nëse jeni ende duke përjetuar imitim ose llogaria juaj ende është e komprometuar, ju lutemi kontaktoni organizatat e mëposhtme që mund t'ju mbështesin.

:[](organisations?services=account&services=legal)


### app_end

> Nëse aplikacioni i rremë nuk është hequr, ju lutemi kontaktoni organizatat e mëposhtme që mund t'ju mbështesin.

:[](organisations?services=account&services=legal)

### web-protection_end

> Nëse kërkesat tuaja për heqje të përmbajtjes nuk kanë qenë të suksesshme, mund të provoni të kontaktoni organizatat e mëposhtme për mbështetje të mëtejshme.

:[](organisations?services=web_protection)

### secure-comms_end

> Nëse keni nevojë për ndihmë ose rekomandime për phishing, sigurinë e e-mailit dhe enkriptimin, ose për komunikim të sigurt në përgjithësi, mund të kontaktoni këto organizata:

:[](organisations?services=secure_comms)


### resolved_end

> Shpresojmë se ky udhëzues i DFAK ishte i dobishëm. Ju lutemi na jepni komente [përmes e-mailit](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

Për të parandaluar çdo përpjekje të mëtejshme për t'ju imituar, lexoni këshillat e mëposhtme.

### final_tips

- Krijoni fjalëkalime të forta, komplekse dhe unike për të gjitha llogaritë tuaja.
- Konsideroni përdorimin e një softueri për menaxhimin e fjalëkalimeve për krijimin dhe ruajtjen e fjalëkalimeve, në mënyrë që të mund të përdorni një fjalëkalim unik në çdo faqe interneti dhe shërbim, pa pasur nevojë t'i mësoni përmendësh të gjitha.
- Aktivizoni vërtetimin me dy faktorë (2FA) për llogaritë tuaja më të rëndësishme. 2FA ofron siguri më të madhe të llogarisë duke ju kërkuar të përdorni më shumë se një metodë për të hyrë në llogaritë tuaja. Kjo do të thotë që edhe nëse dikush do të merrte fjalëkalimin tuaj kryesor, ai/ajo nuk do të mund të hynte në llogarinë tuaj nëse nuk kishte gjithashtu telefonin tuaj celular ose një mjet tjetër dytësor vërtetimi.
- Verifikoni profilet tuaja në platformat e rrjeteve sociale. Disa platforma ofrojnë një veçori për verifikimin e identitetit tuaj dhe lidhjen e tij me llogarinë tuaj.
- Konfiguroni paralajmërimet e Google. Ju mund të merrni e-mail kur shfaqen rezultate të reja për një temë në "Kërko me Google". Për shembull, mund të merrni informacion në lidhje me përmendjet e emrit tuaj ose të organizatës/emrit të kolektivit tuaj.
- Kapni pamjen e faqes suaj të internetit siç duket tani për ta përdorur si provë në të ardhmen. Nëse faqja juaj e internetit lejon “zvarritës” (“crawlers”), mund të përdorni Wayback Machine, të ofruar nga [Internet Archive](https://archive.org) Vizitoni Wayback Machine të [Internet Archive](https://archive.org/web/), futni emrin e faqes suaj të internetit në fushën nën titullin “Save Page Now” dhe klikoni butonin “Save Page”.

#### resources

[Siguria e vetë-mbrojtjes: Krijoni fjalëkalime të forta dhe unike](https://ssd.eff.org/en/module/creating-strong-passwords)
[Siguria e vetë-mbrojtjes: Pasqyrë e animuar Përdorimi i softuerit për menaxhim të fjalëkalimeve](https://ssd.eff.org/en/module/animated-overview-using-password-managers-stay-safe-online)
- [Vetë-mbrojtja nën mbikëqyrje: Si të aktivizoni vërtetimin me dy faktorë](https://ssd.eff.org/module/how-enable-two-factor-authentication)
- [Archive.org: Arkivoni faqen tuaj të internetit](https://archive.org/web/)
[Siguria e vetë-mbrojtjes: Si të përdorni KeePassXC - Një menaxher i sigurt i fjalëkalimeve me burim të hapur](https://ssd.eff.org/en/module/how-use-keepassxc)
- [Access Now Helpline Community Documentation: Zgjedhja e një menaxheri fjalëkalimesh](https://communitydocs.accessnow.org/295-Password_managers.html)
