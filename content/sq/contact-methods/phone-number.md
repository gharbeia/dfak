---
layout: page
title: Numri i telefonit
author: mfc
language: en
summary: Metodat e kontaktit
date: 2018-09
permalink: /en/contact-methods/phone-number.md
parent: /en/
published: true
---

Komunikimet me telefon celular dhe fiks nuk janë të koduara për marrësit tuaj, kështu që përmbajtja e bisedës suaj dhe informacioni se kë po telefononi është i aksesueshëm nga qeveritë, agjencitë e zbatimit të ligjit ose palët e tjera me pajisjet e nevojshme teknike.