---
layout: page
title: Signal
author: mfc
language: en
summary: Metodat e kontaktit
date: 2018-09
permalink: /en/contact-methods/signal.md
parent: /en/
published: true
---

Përdorimi i aplikacionit Signal do të sigurojë që përmbajtja e mesazhit tuaj të jetë e koduar vetëm për organizatën marrëse dhe vetëm ju dhe marrësi juaj do të dini se komunikimi ka ndodhur. Kini parasysh se Signal përdor numrin tuaj të telefonit si emrin tuaj të përdoruesit, kështu që ju do të ndani numrin tuaj të telefonit me organizatën me të cilën po kontaktoni.

Burimet: [Si të: Përdorni Signal-in për Android](https://ssd.eff.org/en/module/how-use-signal-android), [Si të: Përdorni Signal-in për iOS](https://ssd .eff.org/en/module/how-use-signal-ios), [Si të përdorni Signal-in pa e dhënë numrin tuaj të telefonit](https://theintercept.com/2017/09/28/signal-tutorial-second-phone-number/)