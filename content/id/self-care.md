---
layout: sidebar.pug
title: "Perawatan Diri & Komunitas"
author: Peter Steudtner, Flo Pagano
language: id
summary: "Gangguan daring, ancaman, dan jenis serangan digital lainnya dapat menciptakan perasaan kewalahan dan kondisi emosional yang sangat rapuh: Anda mungkin merasa bersalah, malu, cemas, marah, bingung, tidak berdaya, atau bahkan takut akan kesejahteraan psikologis atau fisik Anda."
date: 2018-09-05
permalink: /id/self-care/
parent: Home
sidebar: >
  <h3>Baca lebih lanjut mengenai cara melindungi diri sendiri dan tim Anda dari perasaan kewalahan:</h3>

  <ul>
    <li><a href="https://www.newtactics.org/conversation/self-care-activists-sustaining-your-most-valuable-resource">Perawatan diri untuk Aktivis: Mempertahankan Sumber Daya Anda yang Paling Berharga</a></li>
    <li><a href="https://www.amnesty.org.au/activism-self-care/">Merawat diri Anda sendiri agar Anda dapat terus membela hak asasi manusia</a></li>
    <li><a href="https://www.frontlinedefenders.org/en/resources-wellbeing-stress-management">Sumber Daya untuk Kesejahteraan & Manajemen Stres</a></li>
    <li><a href="https://iheartmob.org/resources/self_care">Perawatan Diri untuk Orang yang Mengalami Gangguan</a></li>
    <li><a href="https://cyber-women.com/en/self-care/">Modul pelatihan perawatan diri untuk perempuan di dunia maya</a></li>
    <li><a href="https://onlineharassmentfieldmanual.pen.org/self-care/">Kesehatan dan Komunitas</a></li>
    <li><a href="https://www.patreon.com/posts/12240673">Dua puluh cara untuk membantu seseorang yang mengalami perundungan daring</a></li>
    <li><a href="https://www.hrresilience.org/">Proyek Resiliensi Hak Asasi Manusia</a></li>
    <li><a href="https://learn.totem-project.org/courses/course-v1:IWPR+IWPR_AH_EN+001/about">Kursus Pembelajaran Daring Totem-Project: Menjaga kesehatan mental Anda (memerlukan pendaftaran)</a></li>
    <li><a href="https://learn.totem-project.org/courses/course-v1:IWPR+IWPR_PAP_EN+001/about">Kursus Pembelajaran Daring Totem-Project: Pertolongan Pertama Psikologis (memerlukan pendaftaran)</a></li>
    <li><a href="https://holistic-security.org/chapters/prepare/1-2-individual-responses-to-threat.html">Respons Individu terhadap Ancaman</a></li>
    <li><a href="https://holistic-security.org/chapters/prepare/1-4-team-and-peer-responses-to-threat.html">Respons Tim dan Rekan terhadap Ancaman</a></li>
    <li><a href="https://holistic-security.org/chapters/prepare/1-5-communicating-about-threats-in-teams-and-organisations.html">Berkomunikasi dalam Tim dan Organisasi</a></li>
  </ul>
---

# Merasa Kewalahan?

Gangguan daring, ancaman, dan jenis serangan digital lainnya dapat menciptakan perasaan kewalahan dan kondisi emosional yang sangat rentan: Anda mungkin merasa bersalah, malu, cemas, marah, bingung, tidak berdaya, atau bahkan takut akan kesehatan psikologis atau fisik Anda. Dan perasaan yang muncul ini bisa sangat berbeda dalam sebuah tim.

Karena kondisi kerentanan dan makna informasi pribadi bagi seseorang berbeda dari yang satu dengan yang lainnya, tidak ada acuan kemampuan merasakan emosi yang "benar". Emosi apa pun tidak ada yang salah dan Anda tidak perlu khawatir apakah reaksi Anda benar atau tidak.

Hal pertama yang harus Anda ingat adalah menyalahkan diri sendiri atau orang lain dalam tim tidak membantu bereaksi terhadap keadaan darurat. Anda mungkin ingin menghubungi seseorang tepercaya yang dapat mendukung Anda atau tim Anda dalam menangani keadaan darurat dari sudut pandang teknis maupun emosional.

Untuk mengurangi serangan daring, Anda perlu mengumpulkan informasi tentang apa yang telah terjadi, tetapi Anda tidak harus melakukannya sendiri - jika Anda memiliki orang yang Anda percaya, Anda dapat meminta mereka untuk membantu Anda sambil mengikuti instruksi di situs web ini, atau beri mereka akses ke peranti atau akun Anda untuk mengumpulkan informasi untuk Anda. Atau Anda dapat meminta bantuan emosional atau teknis selama proses ini.

Jika Anda atau tim Anda membutuhkan bantuan emosional untuk menghadapi keadaan darurat digital (atau juga sebagai upaya tindak lanjut), [Program Kesehatan Mental dari Team CommUNITY](https://www.communityhealth.team/) menawarkan layanan psikososial dalam berbagai format untuk kasus akut dan jangka panjang serta dapat memberikan bantuan dalam berbagai bahasa dan konteks.