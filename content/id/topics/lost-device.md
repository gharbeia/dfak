---
layout: page
title: "Saya Kehilangan Peranti Saya"
author: Hassen Selmi, Bahaa Nasr, Michael Carbone, past DFAK contributors
language: id
summary: "Saya kehilangan peranti saya, apa yang harus saya lakukan?"
date: 2023-04
permalink: /id/topics/lost-device
parent: Home
---

# Saya kehilangan peranti saya

Apakah peranti Anda hilang? Apakah seseorang telah mencuri atau menyitanya?

Dalam situasi tersebut, penting untuk segera mengambil langkah untuk mengurangi risiko orang lain mengakses akun, kontak, dan informasi pribadi Anda.

Bagian Pertolongan Pertama Darurat Digital ini akan memandu Anda melalui beberapa pertanyaan dasar sehingga Anda dapat melakukan asesmen tentang cara mengurangi kemungkinan bahaya terkait kehilangan peranti.

## Workflow

### question-1

Apakah peranti tersebut masih dalam kondisi hilang?

 - [Ya, perantinya hilang](#device-missing)
 - [Tidak, perantinya telah dikembalikan ke saya](#device-returned)

### device-missing

> Ada baiknya merenungkan perlindungan keamanan apa yang dimiliki peranti tersebut:
>
> * Apakah akses ke peranti tersebut dilindungi oleh kata sandi atau langkah keamanan lainnya?
> * Apakah peranti tersebut memiliki enkripsi peranti yang aktif?
> * Bagaimana keadaan peranti Anda saat hilang - Apakah Anda sedang masuk (*login*)? Apakah peranti menyala tetapi dikunci dengan kata sandi? Apakah sedang dalam mode tidur atau hibernasi? Apakah sudah benar-benar dimatikan?

Dengan berbagai pertimbangan tersebut, Anda dapat lebih memahami seberapa besar kemungkinan orang lain mendapatkan akses ke konten yang ada di peranti Anda.

- [Mari hapus akses peranti tersebut ke akun saya](#accounts)

### accounts

> Daftar semua akun yang dapat diakses oleh peranti ini. Bisa berupa akun surel, media sosial, perpesanan, kencan, dan perbankan, serta akun-akun yang mungkin menggunakan peranti tersebut untuk autentikasi sekunder.
>
> Untuk akun apapun yang bisa diakses oleh peranti tersebut (misalnya akun surel, media sosial, atau web), Anda harus menghapus otorisasi peranti tersebut untuk akun-akun yang tertaut. Hal ini bisa dilakukan dengan cara masuk ke akun Anda dan menghapus peranti tersebut dari daftar peranti yang diizinkan.
>
> * [Akun Google](https://myaccount.google.com/device-activity)
> * [Akun Facebook](https://www.facebook.com/settings?tab=security&section=sessions&view)
> * [Akun iCloud](https://support.apple.com/id-id/HT205064)
> * [Akun Twitter](https://twitter.com/settings/sessions)
> * [Akun Yahoo](https://login.yahoo.com/account/activity)

Setelah Anda selesai menghapus tautan akun-akun Anda, mari amankan kata sandi Anda yang mungkin ada di peranti tersebut.

- [Oke, mari kita urus kata sandinya](#passwords)

### passwords

> Renungkan kata sandi apa pun yang disimpan langsung ke peranti atau *browser* apa pun yang telah memiliki kata sandi tersimpan.
>
> Ubah kata sandi semua akun yang bisa diakses oleh peranti yang hilang tersebut. Jika Anda tidak menggunakan pengelola kata sandi, sangat disarankan untuk menggunakannya demi membuat dan mengelola kata sandi yang kuat dengan lebih baik.

Setelah mengubah kata sandi akun-akun Anda di peranti Anda, pikirkan apakah Anda menggunakan kata sandi yang sama untuk akun lain - jika demikian, silakan ubah juga kata sandi tersebut.

- [Saya menggunakan beberapa kata sandi yang sama di peranti yang hilang untuk akun lainnya](#same-password)
- [Semua kata sandi saya yang mungkin disalahgunakan sudah unik](#2fa)

### same-password

Apakah Anda menggunakan kata sandi yang sama di akun atau peranti lain selain peranti yang hilang? Jika demikian, ubah kata sandi pada akun-akun tersebut karena akun-akun tersebut bisa jadi juga disalahgunakan.

- [Oke, sekarang saya sudah mengubah semua kata sandi yang relevan](#2fa)

### 2fa

> Mengaktifkan autentikasi 2-langkah pada beberapa akun yang menurut Anda berisiko untuk diakses dapat mengurangi kemungkinan akun-akun tersebut dapat diakses oleh orang lain.
>
> Aktifkan autentikasi 2-langkah pada semua akun yang dapat diakses dari peranti tersebut. Harap perhatikan bahwa tidak semua akun mendukung autentikasi 2-langkah. Silakan kunjungi [Two Factor Auth](https://2fa.directory/) untuk melihat daftar layanan dan platform yang mendukung autentikasi 2-langkah.

- [Saya telah mengaktifkan autentikasi 2-langkah pada beberapa akun saya untuk pengamanan lebih lanjut. Saya ingin mencoba mencari atau menghapus peranti saya](#find-erase-device)

### find-erase-device

> Pikirkan untuk apa Anda menggunakan peranti tersebut - apakah ada informasi sensitif di peranti ini, misalnya kontak, lokasi, atau pesan? Apakah kebocoran data ini dapat menimbulkan masalah bagi Anda, pekerjaan Anda, atau orang lain selain diri Anda sendiri?
>
> Dalam beberapa kasus, menghapus data pada peranti milik tahanan dari jarak jauh dapat bermanfaat untuk mencegah data di dalamnya disalahgunakan dan digunakan untuk melawan mereka atau aktivis lainnya. Pada saat yang sama, hal ini mungkin menjadi masalah bagi pihak yang ditahan (terutama dalam situasi di mana penyiksaan dan penganiayaan bisa terjadi) - terutama jika pihak yang ditahan dipaksa untuk memberikan akses ke peranti tersebut, data yang tiba-tiba menghilang dari peranti tersebut mungkin membuat otoritas penahanan semakin curiga. Jika Anda menghadapi situasi serupa, baca bagian di panduan ["Seseorang yang saya kenal telah ditangkap"](../../../../arrested).
>
> Penting juga untuk diingat bahwa di beberapa negara penghapusan jarak jauh bisa jadi dianggap bertentangan dengan hukum, karena dapat ditafsirkan sebagai penghancuran barang bukti.
>
> Jika Anda sudah memahami konsekuensi langsung dan hukum bagi semua orang yang terlibat, Anda dapat melanjutkan dengan mencoba menghapus peranti tersebut dari jarak jauh, dengan mengikuti instruksi untuk sistem operasi Anda:
>
> * [peranti Android](https://support.google.com/accounts/answer/6160491?hl=id)
> * [iPhone atau Mac](https://www.icloud.com/#find)
> * [Peranti iOS (iPhone dan iPad) dengan menggunakan iCloud](https://support.apple.com/kb/ph2701)
> * [Peranti Windows](https://support.microsoft.com/id-id/account-billing/menemukan-dan-mengunci-perangkat-windows-yang-hilang-890bf25e-b8ba-d3fe-8253-e98a12f26316)
> * Untuk peranti Windows dan Linux, Anda mungkin sudah memasang perangkat lunak (seperti perangkat lunak anti-pencurian atau anti-virus) yang memungkinkan Anda untuk menghapus data dan riwayat peranti Anda dari jarak jauh. Jika demikian, gunakanlah.

Apakah Anda telah berhasil atau tidak dalam menghapus informasi pada peranti dari jarak jauh, ada baiknya untuk memberi tahu kontak Anda.

- [Lanjutkan ke langkah berikutnya untuk menemukan tip tentang cara memberi tahu kontak Anda mengenai hilangnya peranti Anda.](#inform-network)

### inform-network

> Selain akun Anda sendiri, peranti Anda mungkin juga memiliki informasi tentang orang lain di dalamnya. Hal ini bisa termasuk kontak, komunikasi dengan orang lain, dan grup perpesanan.
>
> Saat mempertimbangkan memberi tahu jaringan dan komunitas tentang peranti Anda yang hilang, gunakan [prinsip pengurangan bahaya](../../../../arrested#harm-reduction) untuk memastikan bahwa dengan menghubungi orang lain Anda tidak menempatkan mereka pada risiko yang lebih besar.

Beri tahu jaringan Anda mengenai peranti yang hilang tersebut. Hal ini bisa dilakukan secara pribadi dengan kontak utama yang berisiko tinggi, atau membuat posting secara publik daftar akun yang berpotensi disalahgunakan di situs web Anda atau di akun media sosial, jika Anda merasa pantas untuk melakukannya.

- [Saya telah memberi tahu kontak saya](#review-history)


### review-history

> Jika memungkinkan, tinjau riwayat koneksi/aktivitas akun dari semua akun yang terhubung ke peranti tersebut. Periksa apakah akun Anda digunakan pada saat Anda sedang tidak daring, atau jika akun Anda diakses dari lokasi atau alamat IP yang tidak dikenal.
>
> Anda dapat meninjau aktivitas Anda di beberapa penyedia layanan populer di bawah ini:
>
> * [Akun Google](https://myaccount.google.com/device-activity)
> * [Akun Facebook](https://www.facebook.com/settings?tab=security&section=sessions&view)
> * [Akun iCloud](https://support.apple.com/id-id/HT205064)
> * [Akun Twitter](https://twitter.com/settings/sessions)
> * [Akun Yahoo](https://login.yahoo.com/account/activity)

Apakah Anda sudah meninjau riwayat koneksi Anda?

- [Sudah, dan tidak ada yang mencurigakan](#check-settings)
- [Sudah, dan ada beberapa aktivitas mencurigakan](../../../account-access-issues/)

### check-settings

> Periksa pengaturan akun dari semua akun yang terhubung dengan peranti tersebut. Apakah ada yang berubah? Untuk akun surel, periksalah fitur penerusan otomatis, kemungkinan perubahan pada cadangan/reset alamat surel atau nomor telepon, sinkronisasi ke peranti berbeda, termasuk telepon, komputer atau tablet, dan izin ke aplikasi atau perizinan akun lainnya.
>
> Ulangi peninjauan riwayat aktivitas akun setidaknya seminggu sekali selama sebulan, untuk memastikan akun Anda tetap menunjukkan aktivitas yang tidak mencurigakan.

- [Riwayat aktivitas akun saya menunjukkan aktivitas yang mencurigakan](../../../account-access-issues/)
- [Riwayat aktivitas akun saya tidak menunjukkan aktivitas mencurigakan apapun, namun saya akan terus meninjaunya dari waktu ke waktu](#resolved_end)


### device-returned

> Jika peranti Anda hilang, diambil oleh pihak ketiga, atau harus diserahkan di area perbatasan, tetapi Anda telah memilikinya kembali, berhati-hatilah karena Anda tidak tahu siapa yang telah memiliki akses ke peranti tersebut. Tergantung pada tingkat risiko yang Anda hadapi, sebaiknya Anda memperlakukan peranti tersebut sebagai peranti yang tak dapat dipercaya atau mungkin disalahgunakan.
>
> Tanyakan pertanyaan berikut kepada diri sendiri dan buat asesmen risiko bahwa peranti Anda telah disalahgunakan:
>
> * Berapa lama peranti tersebut berada di luar kendali Anda?
> * Siapa saja yang berpotensi memiliki akses terhadapnya?
> * Mengapa mereka mau mengaksesnya?
> * Apakah ada tanda-tanda bahwa peranti tersebut telah dirusak secara fisik?
>
> Jika Anda tidak lagi mempercayai peranti Anda, pertimbangkan untuk menghapus dan memasang ulang peranti Anda, atau mendapatkan yang baru.

Apakah Anda memerlukan bantuan untuk mendapatkan peranti pengganti?

- [Ya](#new-device_end)
- [Tidak](#resolved_end)

### new-device_end

Jika Anda membutuhkan dukungan keuangan eksternal untuk mendapatkan peranti pengganti, silakan hubungi organisasi yang dapat membantu Anda di bawah ini.

:[](organisations?services=equipment_replacement)

### resolved_end

Kami harap panduan Pertolongan Pertama Darurat Digital ini bermanfaat. Silakan berikan masukan [via surel](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

Berikut adalah serangkaian tip untuk menanggulangi risiko kebocoran data dan akses tanpa izin ke akun dan informasi Anda:

- Jangan pernah tinggalkan peranti Anda tanpa pengawasan. Jika Anda tidak bisa membawanya bersama Anda, matikan dan simpan di tempat yang aman.
- Aktifkan enkripsi *full-disk*.
- Gunakan kata sandi yang kuat untuk mengunci peranti Anda.
- Aktifkan fitur *Find/Erase My Phone* bila memungkinkan, tapi perlu diingat bahwa fitur ini bisa digunakan untuk melacak Anda atau menghapus peranti Anda, jika akun Anda yang terkait (Gmail/iCloud) telah disalahgunakan.

#### Resources

* [Dokumentasi Komunitas Saluran Bantuan Access Now: Tip tentang cara mengaktifkan enkripsi *full-disk* (dalam bahasa Inggris)](https://communitydocs.accessnow.org/166-Full-Disk_Encryption.html)
* [Security in a Box: Taktik untuk mengamankan berkas sensitif (dalam bahasa Inggris)](https://securityinabox.org/en/files/secure-file-storage/)
* Pertimbangkan menggunakan perangkat lunak anti-pencurian seperti [Prey](https://preyproject.com)
