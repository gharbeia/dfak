﻿---
layout: page
title: "Saya menjadi target kampanye pencemaran nama baik"
author: Inés Binder, Florencia Goldsman, Erika Smith, Gus Andrews
language: id
summary: "Apa yang harus dilakukan ketika seseorang mencoba merusak reputasi Anda secara daring."
date: 2023-04
permalink: /id/topics/defamation
parent: Home
---

# Saya menjadi target kampanye pencemaran nama baik

Ketika seseorang dengan sengaja membuat dan menyebarkan informasi yang palsu, dimanipulasi, atau menyesatkan untuk menyebabkan kerugian dengan merusak reputasi seseorang, hal tersebut adalah wujud dari kampanye pencemaran nama baik. Kampanye ini dapat ditargetkan pada individu atau organisasi dengan semacam paparan publik. Hal tersebut dapat diatur oleh aktor siapa saja, baik pemerintah atau swasta.

Meskipun ini bukan fenomena baru, lingkungan digital mendorong penyebaran pesan tersebut dengan cepat, meningkatkan skala dan kecepatan serangan serta memperdalam dampaknya.

Ada dimensi gender dalam kampanye pencemaran nama baik ketika tujuannya adalah untuk mengecualikan perempuan dan kelompok LGBTQIA+ dari kehidupan publik, menyebarkan informasi yang "mengeksploitasi ketidaksetaraan gender, mempromosikan heteronormativitas, dan memperdalam perpecahan sosial" [[1]](#note-1).

Bagian Pertolongan Pertama Darurat Digital ini akan memandu Anda melalui beberapa langkah dasar untuk merencanakan cara menanggapi kampanye pencemaran nama baik. Ikuti kuesioner berikut untuk mengidentifikasi sifat masalah Anda dan menemukan solusi yang memungkinkan.

<a name="note-1"></a>
[1] [Melawan Disinformasi](https://counteringdisinformation.org/node/13/)

## Workflow

### physical-wellbeing

Apakah Anda mengkhawatirkan keselamatan atau kesejahteraan fisik Anda?

- [Ya](#physical-risk_end)
- [Tidak](#no-physical-risk)

### no-physical-risk

> Kampanye pencemaran nama baik bertujuan untuk menyerang reputasi Anda dengan cara mempertanyakan, meragukan, menyalahkan target melalui bukti palsu, menuduh melalui kebohongan, atau mengungkap kontradiksi untuk mengikis kepercayaan publik.
>
> Serangan semacam itu terutama memengaruhi orang-orang yang terpapar publik yang pekerjaannya bergantung pada prestise dan kepercayaan yang dimiliki: aktivis dan pembela hak asasi manusia, politisi, jurnalis, dan orang-orang yang menyediakan data untuk publik, artis, dll.

Apakah serangan tersebut mencoba merusak reputasi Anda?

 - [Ya](#perpetrators)
 - [Tidak](#no-reputational-damage)

### no-reputational-damage

> Jika Anda menghadapi serangan yang tidak ditujukan untuk merusak reputasi Anda, Anda mungkin menghadapi keadaan darurat yang sifatnya berbeda dari kampanye pencemaran nama baik.

 Apakah Anda ingin mengecek pertanyaan berikut untuk mendiagnosis gangguan daring?

 - [Ya, saya mungkin menghadapi gangguan daring](../../../harassed-online)
 - [Tidak, saya masih merasa itu adalah kampanye pencemaran nama baik](#perpetrators)
 - [Saya membutuhkan bantuan untuk memahami masalah yang saya hadapi](/../../support)


### perpetrators

Apakah Anda tahu siapa yang di balik kampanye pencemaran nama baik?

 - [Ya](#respond-defamation)
 - [Tidak](#analyze-messages)

### respond-defamation

> Ada banyak cara untuk menanggapi kampanye pencemaran nama baik, tergantung pada dampak dan penyebaran pesan, aktor yang terlibat, serta motivasi mereka. Jika kampanye memiliki dampak dan penyebaran yang rendah, sebaiknya abaikan saja. Sebaliknya, jika itu memiliki dampak dan penyebaran yang tinggi, Anda dapat mempertimbangkan untuk melaporkan dan menghapus konten, meluruskan catatan, dan membungkamnya.
>
> Berhati-hatilah untuk tidak memperkuat pesan fitnah saat mencoba menangani kampanye pencemaran tersebut. Mengutip sebuah pesan, meski hanya untuk mengekspos aktor atau motivasi di baliknya, malah dapat meningkatkan penyebarannya. Pertimbangkan [teknik *truth sandwich*](https://en.wikipedia.org/wiki/Truth_sandwich) untuk menutupi informasi palsu tanpa secara tidak sengaja menyebarkannya lebih jauh. Teknik ini "mengharuskan menyajikan kebenaran tentang subjek sebelum menutupi informasi yang salah, lalu mengakhiri cerita dengan menyajikan kembali kebenaran".
>
> Pilih strategi yang menurut Anda tepat untuk menahan serangan, membangun kembali kepercayaan, dan membangun kembali kredibilitas di komunitas Anda. Ingatlah bahwa apa pun strategi yang Anda pilih, [perawatan diri](/../../self-care/) Anda harus menjadi prioritas utama. Pertimbangkan juga untuk [mendokumentasikan](/../../documentation) konten atau profil yang menyerang Anda sebelum merespons hal tersebut.

Bagaimana Anda menanggapi kampanye pencemaran nama baik?

 - [Saya ingin memberi tahu platform media sosial dan menghapus konten yang memfitnah](#notify-take-down)
 - [Saya ingin mengabaikannya dan membisukan notifikasi](#ignore-silence)
 - [Saya ingin meluruskan catatan](#set-record-straight)
 - [Saya ingin mengajukan komplain hukum](#legal-complaint)
 - [Saya perlu menganalisis pesan fitnah lebih lanjut sehingga saya bisa membuat keputusan](#analyze-messages)

### analyze-messages

> Saat Anda atau organisasi Anda menjadi target kampanye pencemaran nama baik, memahami metode standar yang digunakan untuk menyebarkan disinformasi dapat membantu Anda menimbang risiko dan mengarahkan langkah selanjutnya. Menganalisis pesan yang Anda terima selama serangan semacam ini dapat memberi Anda informasi lebih lanjut tentang pelaku, motivasi, dan sumber daya yang digunakan untuk merusak reputasi Anda. Teknik serangan melibatkan serangkaian alat dan koordinasi daring.
>
> Anda mungkin ingin terlebih dahulu melakukan asesmen tingkat risiko yang Anda hadapi. Di dalam [Peralatan Disinformasi Interaksi](https://www.interaction.org/documents/disinformation-toolkit/), Anda akan menemukan Alat Asesmen Risiko untuk membantu Anda mengevaluasi kerentanan lingkungan media Anda.
>
> Kampanye pencemaran nama baik di media sosial sering kali menggunakan tagar untuk mendapatkan minat yang lebih besar, tetapi ini juga berguna untuk memantau serangan karena Anda dapat mencari berdasarkan tagar dan melakukan asesmen pelaku dan pesan. Untuk mengukur pengaruh tagar di media sosial, Anda mungkin ingin mencoba alat-alat berikut:
>
> - [Track my hashtag](https://https://www.trackmyhashtag.com/)
> - [Brand mentions](https://brandmentions.com/hashtag-tracker) mengumpulkan sebutan (*mention*) pada tagar dari Twitter, Instagram dan Facebook menjadi satu aliran data.
> - [InVid Project](https://www.invid-project.eu/) adalah platform verifikasi pengetahuan untuk mendeteksi berita yang muncul dan menilai keandalan serta kelayakan pemberitaan berkas video dan konten yang disebarkan melalui media sosial.
> - [Metadata2go](https://www.metadata2go.com/view-metadata ) menemukan metadata di balik berkas yang Anda analisis.
> - [YouTube DataViewer](https://citizenevidence.amnestyusa.org/) dapat digunakan untuk melakukan pencarian balik video Amnesty International untuk melihat apakah video tersebut lebih tua dan apakah sudah dimodifikasi.
>
>**Serangan Manual vs Otomatis**
>
> Aktor negara dan musuh lainnya sering menggunakan botnet terkoordinasi yang tidak terlalu mahal atau sulit secara teknis untuk disiapkan. Menyadari bahwa rentetan pesan penyerangan bukan berasal dari lusinan atau ratusan orang tetapi merupakan bot otomatis dapat mengurangi kecemasan dan membuat dokumentasi lebih mudah, dan akan membantu Anda memutuskan bagaimana Anda ingin membela diri. Gunakan [Botsentinel](https://botsentinel.com/) untuk memeriksa sifat akun yang menyerang Anda. Beberapa pesan bot dapat diambil dan disebarluaskan lebih lanjut oleh individu juga, jadi Anda mungkin menemukan bahwa skor akan memverifikasi beberapa akun yang mengirimkan pesan disinformasi, misal mereka memiliki pengikut, gambar profil dan konten unik, dan indikator lain yang berarti kemungkinannya kecil kalau mereka adalah akun bot. Alat lain yang bisa Anda gunakan adalah [Pegabot](https://es.pegabot.com.br/).
>
> **Informasi Internal vs Informasi Publik**
>
> Disinformasi yang terampil dapat mendasarkan konten pada inti kebenaran atau membuat konten tampak jujur. Merefleksikan jenis informasi yang dibagikan dan sumbernya dapat menentukan arah tindakan yang berbeda.
>
> Apakah informasinya pribadi atau dari sumber internal? Pertimbangkan untuk mengubah kata sandi Anda dan memasang autentikasi dua-langkah untuk memastikan bahwa tidak ada orang lain yang mengakses akun Anda selain Anda. Jika informasi ini dirujuk dalam obrolan, atau percakapan atau berkas bersama, dorong teman dan kolega untuk meninjau pengaturan keamanan mereka. Pertimbangkan untuk membersihkan dan menutup obrolan lama agar informasi dari masa lalu tidak mudah diakses di peranti Anda atau di peranti kolega dan teman.
>
> Musuh dapat membajak akun individu atau organisasi untuk menyebarkan [informasi palsu](https://en.wikipedia.org/wiki/Misinformation) dan [disinformasi](https://en.wikipedia.org/wiki/Disinformation) dari akun tersebut, yang dapat memberikan informasi yang dibagikan lebih banyak keabsahan. Untuk mendapatkan kembali kendali atas akun Anda, lihat bagian Pertolongan Pertama Darurat Digital [Saya tidak bisa mengakses akun saya](../../../account-access-issues/questions/What-Type-of-Account-or-Service/).
>
> Apakah informasi yang disebarluaskan bersumber dari informasi publik yang tersedia secara daring? Mungkin bermanfaat untuk mengikuti bagian Pertolongan Pertama Darurat Digital [Saya telah di-*doxx* atau seseorang membagikan media pribadi saya tanpa persetujuan](../../../doxing) untuk mencari dan mempersempit di mana informasi ini tersedia dan apa yang mungkin dilakukan.
> <br />
>
> **Pembuatan Material Lebih Canggih** (Video YouTube, *deepfake*, dll.)
>
> Terkadang serangan tersebut lebih tepat sasaran dan pelaku telah menginvestasikan waktu dan upaya untuk membuat citra dan berita palsu di video YouTube atau [*deepfake*](https://en.wikipedia.org/wiki/Deepfake) untuk mendukung alur cerita palsu mereka. Penyebaran materi semacam itu dapat mengungkap pelaku dan memberikan lebih banyak umpan untuk kecaman hukum dan publik atas serangan tersebut dan alasan yang jelas untuk permintaan penghapusan. Ini juga dapat berarti bahwa serangan tersebut sangat dipersonalisasi dan meningkatkan perasaan rentan dan risiko seseorang. Seperti ketika menghadapi serangan apapun, [perawatan diri](/../../self-care/) dan keselamatan pribadi adalah prioritas utama.
>
> Contoh lain adalah penggunaan domain palsu di mana musuh membuat situs web atau profil jejaring sosial yang mirip dengan akun aslinya. Jika Anda menghadapi masalah seperti itu, lihat bagian Pertolongan Pertama Darurat Digital di [peniruan identitas](../../../impersonated/).
>
> Jenis serangan ini juga dapat menunjukkan bahwa ada organisasi, logistik, pembiayaan, dan waktu yang lebih besar yang dihabiskan oleh penyerang, hal-hal tersebut memberikan wawasan yang lebih besar mengenai motivasi serangan dan kemungkinan siapa si pelaku.

Apakah Anda sekarang memiliki gagasan lebih baik tentang siapa yang menyerang Anda dan bagaimana mereka melakukannya?

 - [Ya, sekarang saya ingin mempertimbangkan strategi untuk merespons](#respond-defamation)
 - [Tidak, saya perlu bantuan untuk mengetahui cara merespons](#defamation_end)

### notify-take-down

> ***Catatan:*** *Pertimbangkan untuk [mendokumentasikan serangan](/../../documentation) sebelum meminta agar platform menghapus konten yang memfitnah. Jika Anda sedang mempertimbangkan tindakan hukum, Anda harus berkonsultasi tentang informasi [dokumentasi hukum](/../../documentation#legal).*
>
> Ketentuan layanan dari berbagai platform akan menjelaskan kapan permintaan penghapusan akan dianggap sah, misal konten yang melanggar hak cipta, yang mana Anda harus menjadi pemiliknya. Kepemilikan hak cipta muncul secara bawaan sejak Anda membuat karya orisinal: ambil gambar, tulis teks, buat lagu, rekam video, dll. Tidak cukup bagi Anda untuk tampil dalam karya tersebut; harus Anda yang membuatnya.

Apakah Anda memiliki hak atas konten yang digunakan dalam kampanye pencemaran nama baik?

 - [Ya](#copyright)
 - [Tidak](#report-defamatory-content)

### set-record-straight

> Versi fakta Anda selalu penting. Meluruskan catatan mungkin lebih mendesak jika kampanye pencemaran nama baik terhadap Anda tersebar luas dan sangat merusak reputasi Anda. Namun, dalam kasus ini, Anda harus mengevaluasi dampak kampanye tersebut terhadap Anda untuk menilai apakah respons publik merupakan strategi terbaik Anda.
>
> Selain melaporkan konten dan aktor jahat ke platform, Anda mungkin ingin membuat kontra-narasi, menyanggah disinformasi, mencela pelaku secara publik, atau sekadar mengangkat kebenaran. Bagaimana Anda merespons juga akan bergantung pada komunitas dukungan yang Anda miliki untuk membantu Anda dan evaluasi Anda sendiri atas risiko yang mungkin Anda hadapi dalam mengambil sikap publik.

Apakah Anda memiliki tim untuk mendukung Anda?

 - [Ya](#campaign-team)
 - [Tidak](#self-campaign)

### legal-complaint

> Saat menghadapi proses hukum, mengumpulkan, mengatur, dan menghadirkan bukti ke pengadilan, Anda harus mengikuti protokol tertentu agar dapat diterima oleh hakim sebagai barang bukti. Saat menyajikan bukti serangan daring, mengambil tangkapan layar saja tidaklah cukup. Lihat bagian Pertolongan Pertama Darurat Digital di [cara mendokumentasikan serangan untuk dihadirkan dalam proses hukum](/../../documentation#legal) untuk mempelajari lebih lanjut tentang proses ini sebelum menghubungi pengacara pilihan Anda.


Apa yang ingin Anda lakukan?

 - [Rekomendasi ini sangat membantu - saya tahu apa yang harus dilakukan sekarang](#resolved_end)
 - [Rekomendasi ini sangat membantu, tetapi saya ingin mempertimbangkan strategi lain](#respond-defamation)
 - [Saya membutuhkan bantuan untuk melanjutkan komplain hukum saya](#legal_end)

### ignore-silence

> Ada kalanya, terutama dalam kasus di mana kampanye pencemaran nama baik memiliki dampak atau penyebaran yang kecil, ketika Anda mungkin lebih memilih untuk mengabaikan si pelaku dan hanya membungkam pesan tersebut. Ini juga bisa menjadi langkah penting untuk perawatan diri, dan teman atau sekutu dapat didaftarkan untuk memantau konten guna mengingatkan Anda jika ada tindakan lain yang pantas dilakukan. Langkah ini tidak menghilangkan konten, jika nanti Anda ingin mendokumentasikan atau meninjau informasi yang disebarkan untuk merugikan Anda.

Di mana konten fitnah yang ingin Anda bungkam disebarluaskan?

 - [Surel](#silence-email)
 - [Facebook](#silence-facebook)
 - [Instagram](#silence-instagram)
 - [Reddit](#silence-reddit)
 - [TikTok](#silence-tiktok)
 - [Twitter](#silence-twitter)
 - [Whatsapp](#silence-whatsapp)
 - [YouTube](#silence-youtube)

### copyright

> Anda dapat menggunakan peraturan hak cipta, seperti [Digital Millennium Copyright Act (DMCA)](https://en.wikipedia.org/wiki/Digital_Millennium_Copyright_Act), untuk menghapus konten. Sebagian besar platform media sosial menyediakan formulir untuk melaporkan pelanggaran hak cipta dan mungkin lebih responsif terhadap jenis permintaan ini daripada pertimbangan penghapusan lainnya, karena implikasi hukumnya bagi mereka.
>
> ***Catatan:*** *Selalu [dokumentasikan](/../../documentation) sebelum meminta untuk menghapus konten. Jika Anda sedang mempertimbangkan tindakan hukum, Anda harus berkonsultasi tentang informasi [dokumentasi hukum](/../../documentation#legal).*
>
> Anda dapat menggunakan tautan ini untuk mengirim permintaan penghapusan karena pelanggaran hak cipta ke platform jejaring sosial utama:
>
> - [Archive.org](https://help.archive.org/help/how-do-i-request-to-remove-something-from-archive-org/)
> - [Facebook](https://www.facebook.com/help/1020633957973118/?helpref=hc_fnav&cms_id=1020633957973118)
> - [Instagram](https://help.instagram.com/contact/552695131608132)
> - [TikTok](https://www.tiktok.com/legal/report/Copyright?lang=id)
> - [Twitter](https://help.twitter.com/id/forms/ipi)
> - [YouTube](https://support.google.com/youtube/answer/2807622?sjid=1561215955637134274-SA&hl=id)
>
> Harap perhatikan bahwa mungkin perlu beberapa saat untuk menerima respons atas permintaan Anda. Simpan halaman ini di *bookmark* Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah konten tersebut telah dihapus?

 - [Ya](#resolved_end)
 - [Tidak, saya butuh bantuan hukum](#legal_end)
 - [Tidak, saya memerlukan bantuan untuk menghubungi platform](#harassment_end)

### report-defamatory-content

> Tidak semua platform memiliki proses khusus untuk melaporkan konten yang memfitnah, dan beberapa memang memerlukan proses hukum sebelumnya. Anda dapat meninjau jalur yang ditetapkan setiap platform untuk melaporkan konten yang memfitnah di bawah.
>
> - [Archive.org](https://help.archive.org/help/how-do-i-request-to-remove-something-from-archive-org/)
> - [Facebook](https://www.facebook.com/help/contact/430253071144967)
> - [Instagram](https://help.instagram.com/contact/653100351788502)
> - [TikTok](https://www.tiktok.com/legal/report/feedback?lang=id)
> - [Twitch](https://help.twitch.tv/s/article/how-to-file-a-user-report?language=en_US)
> - [Twitter](https://help.twitter.com/id/rules-and-policies/twitter-report-violation)
> - [Whatsapp](https://faq.whatsapp.com/1142481766359885?helpref=search&cms_platform=android&cms_id=1142481766359885&draft=false)
> - [YouTube](https://support.google.com/youtube/answer/6154230)
>
> Harap perhatikan bahwa mungkin perlu beberapa saat untuk menerima respons atas permintaan Anda. Simpan halaman ini di *bookmark* Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah konten telah dilaporkan dan dianalisis oleh penyedia layanan untuk dihapus?

 - [Ya](#resolved_end)
 - [Tidak, saya butuh bantuan hukum](#legal_end)
 - [Tidak, saya memerlukan bantuan untuk menghubungi platform](#harassment_end)

### silence-email

> Sebagian besar klien surel menawarkan opsi untuk menyaring pesan dan melabelinya secara otomatis, mengarsipkannya, atau menghapusnya sehingga tidak muncul di kotak masuk Anda. Biasanya Anda dapat membuat aturan untuk menyaring pesan menurut pengirim, penerima, tanggal, ukuran, lampiran, subjek, atau kata kunci.
>
> Pelajari cara membuat saringan di surel web:
>
> - [Gmail](https://support.google.com/mail/answer/6579?hl=id&sjid=15644708183416197925-AP#zippy=%2Ccreate-a-filter)
> - [Protonmail](https://proton.me/support/email-inbox-filters)
> - [Yahoo](https://help.yahoo.com/kb/SLN28071.html)
>
> Pelajari cara membuat saringan di klien surel:
>
> - [MacOS Mail](https://support.apple.com/id-id/guide/mail/mlhl1f6cf15a/mac)
> - [Microsoft Outlook](https://support.microsoft.com/id-id/office/menyiapkan-aturan-di-outlook-75ab719a-2ce8-49a7-a214-6d62b67cbd41)
> - [Thunderbird](https://support.mozilla.org/en-US/kb/organize-your-messages-using-filters)

Apakah Anda ingin membungkam konten yang memfitnah di platform lain?

 - [Ya](#ignore-silence)
 - [Tidak](#damage-control)

### silence-twitter

> Jika Anda tidak ingin terpapar pesan tertentu, Anda dapat memblokir pengguna atau membungkam pengguna dan pesan. Ingatlah bahwa jika Anda memilih untuk memblokir seseorang atau beberapa konten, Anda tidak akan memiliki akses ke konten yang memfitnah tersebut untuk [mendokumentasikan serangan](/../../dokumentasi). Dalam hal ini, Anda mungkin lebih memilih mengaksesnya melalui akun lain atau hanya membungkamnya.
>
> - [Bisukan akun agar tidak muncul di linimasa Anda](https://help.twitter.com/id/using-twitter/twitter-mute)
> - [Bisukan kata, frasa, nama pengguna, emoji, tagar, dan notifikasi untuk percakapan](https://help.twitter.com/id/using-twitter/advanced-twitter-mute-options)
> - [Tunda notifikasi untuk pesan langsung (*direct message*) selamanya](https://help.twitter.com/id/using-twitter/direct-messages#snooze)

Apakah Anda ingin membungkam konten yang memfitnah di platform lain?

 - [Ya](#ignore-silence)
 - [Tidak](#damage-control)

### silence-facebook

> Meskipun Facebook tidak mengizinkan Anda untuk membisukan pengguna atau konten, ia memungkinkan Anda untuk memblokir profil dan halaman pengguna.
>
> - [Blokir profil Facebook](https://www.facebook.com/help/)
> - [Blokir pesan dari profil di Facebook](https://www.facebook.com/help/1682395428676916)
> - [Blokir Halaman Facebook](https://www.facebook.com/help/395837230605798)
> - [Larang atau blokir profil dari Halaman Facebook Anda](https://www.facebook.com/help/185897171460026/)

Apakah Anda ingin membungkam konten yang memfitnah di platform lain?

 - [Ya](#ignore-silence)
 - [Tidak](#damage-control)

### silence-instagram

> Berikut daftar tip dan alat untuk membungkam pengguna dan percakapan di Instagram:
>
> - [Matikan atau nyalakan pembisuan seseorang di Instagram](https://help.instagram.com/469042960409432)
> - [Matikan atau nyalakan pembisuan *story* Instagram seseorang](https://help.instagram.com/290238234687437)
> - [Batasi atau batalkan pembatasan seseorang](https://help.instagram.com/2638385956221960)
> - [Matikan saran akun untuk profil Instagram Anda](https://help.instagram.com/530450580417848)
> - [Sembunyikan posting yang disarankan di umpan Instagram Anda](https://help.instagram.com/423267105807548)

Apakah Anda ingin membungkam konten yang memfitnah di platform lain?

 - [Ya](#ignore-silence)
 - [Tidak](#damage-control)

### silence-youtube

> Jika Anda mengakses YouTube dari *browser* desktop, Anda dapat menghapus video, saluran, bagian, dan daftar putar dari beranda.
>
> - [Sesuaikan rekomendasi YouTube Anda](https://support.google.com/youtube/answer/6342839?hl=id&sjid=933524529428204051-AP)
>
> Anda juga dapat membuat daftar blokir di profil YouTube Anda untuk memblokir penonton lain di *live chat* YouTube Anda.
>
> - YouTube: [myaccount.google.com/blocklist](https://myaccount.google.com/blocklist)
> - [Blokir penonton lain di *live chat* YouTube](https://support.google.com/youtube/answer/7663906?hl=id&sjid=13217126582708921844-AP)

Apakah Anda ingin membungkam konten yang memfitnah di platform lain?

 - [Ya](#ignore-silence)
 - [Tidak](#damage-control)

### silence-whatsapp

> WhatsApp adalah platform umum untuk kampanye pencemaran nama baik. Pesan berjalan cepat karena hubungan yang tepercaya antara pengirim dan penerima.
>
> - [Arsipkan atau batalkan pengarsipan obrolan atau grup](https://faq.whatsapp.com/1426887324388733/?cms_platform=web&cms_id=1426887324388733&draft=true)
> - [Keluar dan hapus grup](https://faq.whatsapp.com/498814665492149/?cms_platform=web&cms_id=498814665492149&draft=true)
> - [Blokir kontak](https://faq.whatsapp.com/1142481766359885/?cms_platform=android&cms_id=1142481766359885&draft=true)
> - [Bisukan atau batalkan pembisuan notifikasi grup](https://faq.whatsapp.com/694350718331007/?cms_platform=web&cms_id=694350718331007&draft=true)

Apakah Anda ingin membungkam konten yang memfitnah di platform lain?

 - [Ya](#ignore-silence)
 - [Tidak](#damage-control)

### silence-tiktok

> TikTok memungkinkan Anda memblokir pengguna, mencegah pengguna mengomentari video Anda, membuat saringan untuk komentar di video Anda serta menghapus, membisukan, dan menyaring pesan langsung (*direct message*).
>
> - [Memblokir pengguna di TikTok](https://support.tiktok.com/id/using-tiktok/followers-and-following/blocking-the-users)
> - [Pilih siapa saja yang dapat mengomentari video Anda di pengaturan](https://support.tiktok.com/id/using-tiktok/messaging-and-notifications/comments)
> - [Cara mengaktifkan saringan komentar untuk video TikTok Anda](https://support.tiktok.com/id/using-tiktok/messaging-and-notifications/comments#3)
> - [Kelola pengaturan privasi komentar untuk semua video Tik Tok Anda](https://support.tiktok.com/id/using-tiktok/messaging-and-notifications/comments#4)
> - [Kelola pengaturan privasi komentar untuk salah satu video TikTok Anda](https://support.tiktok.com/id/using-tiktok/messaging-and-notifications/comments#5)
> - [Cara menghapus, membisukan, dan menyaring pesan langsung](https://support.tiktok.com/id/account-and-privacy/account-privacy-settings/direct-message#6)
> - [Cara mengelola siapa saja yang dapat mengirimi Anda pesan langsung](https://support.tiktok.com/id/account-and-privacy/account-privacy-settings/direct-message#7)

Apakah Anda ingin membungkam konten yang memfitnah di platform lain?

 - [Ya](#ignore-silence)
 - [Tidak](#damage-control)

### silence-reddit

> Jika pencemaran nama baik beredar di Reddit, Anda dapat memilih untuk mengecualikan komunitas tempat kejadian ini untuk muncul di notifikasi, umpan Beranda (termasuk rekomendasi), dan umpan Populer di situs desktop Reddit atau reddit.com Anda. Jika Anda seorang moderator, Anda juga dapat melarang atau membisukan pengguna yang berulang kali melanggar aturan komunitas Anda.
>
> - [Pembisuan Komunitas Reddit](https://reddit.zendesk.com/hc/en-us/articles/9810475384084-What-is-community-muting-)
> - [Melarang dan Membisukan Anggota Komunitas](https://mods.reddithelp.com/hc/en-us/articles/360009161872-User-Management-banning-and-muting)

Apakah Anda ingin membungkam konten yang memfitnah di platform lain?

 - [Ya](#ignore-silence)
 - [Tidak](#damage-control)


### campaign-team

> Jika kampanye untuk melawan Anda menghabiskan waktu dan sumber daya, hal ini menunjukkan bahwa pelaku memiliki motivasi yang dalam untuk menyerang Anda. Anda mungkin perlu menggunakan semua strategi yang disebutkan dalam alur kerja ini untuk mengurangi kerusakan dan membuat kampanye publik untuk membela Anda. Teman dan sekutu dapat bermanfaat dan pusatkan perawatan diri Anda jika mereka dapat membantu dengan analisis pesan, dokumentasi serangan, serta pelaporan konten dan profil.
>
> Tim kampanye dapat bekerja sama dalam memutuskan cara terbaik untuk membantah narasi yang digunakan untuk melawan Anda. Pesan disinformasi itu berbahaya, dan kita sering diberi tahu untuk tidak mengulangi pesan si penyerang. Menemukan keseimbangan antara mengangkat cerita Anda dan menyangkal klaim palsu itu penting.
>
> Sangat penting untuk melakukan [analisis asesmen risiko](https://www.interaction.org/documents/disinformation-toolkit/) untuk diri sendiri serta setiap individu dan organisasi pendukung sebelum memulai kampanye publik.
>
> Selain menganalisis pesan yang menyerang, Anda dan tim Anda mungkin ingin memulai dengan [memetakan organisasi dan sekutu](https://holistic-security.tacticaltech.org/exercises/explore/visual-actor-mapping-part-1.html) yang mengenali latar belakang Anda dan akan menambah narasi positif tentang pekerjaan Anda, atau membuat "suara gembira" yang cukup untuk membelokkan atau menenggelamkan kampanye kotor.
>
> Anda dan organisasi Anda mungkin bukan satu-satunya yang diserang. Pertimbangkan untuk membangun kepercayaan dengan organisasi dan komunitas lain yang telah didiskriminasi atau menjadi sasaran kampanye disinformasi. Ini mungkin berarti mempersiapkan anggota komunitas untuk menangani pesan kontroversial atau berbagi sumber daya sehingga orang lain dapat membela diri mereka.
>
> Luangkan waktu dalam tim Anda untuk mengidentifikasi apa yang ingin diprovokasi oleh pesan dan jenis konten Anda - juga berdasarkan apakah Anda menyangkal kampanye atau mengangkat pesan positif tanpa mengacu pada taktik pencemaran nama baik. Anda mungkin ingin orang lebih terampil untuk membedakan rumor dari fakta. Pesan berbasis nilai sederhana sangat efektif dalam kampanye publik apa pun.
>
> Selain mengeluarkan pernyataan publik atau pers, Anda mungkin ingin memilih dan menyiapkan juru bicara yang berbeda untuk kampanye Anda, hal tersebut juga membantu membelokkan serangan yang dipersonalisasi yang umum terjadi dalam kampanye kotor, terutama dalam kasus disinformasi gender.
>
> Membangun sistem pemantauan media untuk melacak kampanye perpesanan Anda dan penyerang Anda akan membantu Anda melihat pesan mana yang paling tepat untuk menyempurnakan kampanye Anda.
>
> Jika konten yang memfitnah juga diterbitkan di koran atau majalah, tim Anda mungkin ingin menuntut [Hak Jawab](#self-strategies) atau aktifkan kasus Anda dengan [situs pengecekan fakta](#self-strategies).

Apakah Anda pikir Anda telah mampu mengendalikan kerusakan reputasi Anda?

 - [Ya](#resolved_end)
 - [Tidak](#defamation_end)

### self-campaign

> Jika Anda tidak memiliki tim dukungan, Anda harus mempertimbangkan keterbatasan kapasitas atau sumber daya yang Anda miliki saat merencanakan langkah selanjutnya, dengan memprioritaskan [perawatan diri Anda](/../../self-care). Anda mungkin ingin memublikasikan pernyataan di profil media sosial atau situs web Anda. Pastikan Anda memiliki [kendali penuh](../../../account-access-issues) atas akun-akun tersebut sebelum Anda melakukannya. Dalam pernyataan Anda, Anda mungkin ingin merujuk orang ke jalur Anda yang sudah mapan dan sumber tepercaya yang mencerminkan jalur tersebut. [Analisis kampanye pencemaran nama baik](#analyze-messages) akan memberikan wawasan tentang jenis pernyataan yang ingin Anda buat. Misalnya, Anda dapat meninjau apakah ada sumber berita yang direferensikan dalam serangan tersebut.

Apakah konten yang memfitnah juga dimuat di surat kabar atau majalah?

 - [Ya](#self-strategies)
 - [Tidak](#damage-control)

### self-strategies

> Jika informasi yang memfitnah dipublikasikan, kebijakan editorial atau bahkan undang-undang di negara tertentu dapat memberi Anda [hak jawab atau hak koreksi](https://en.wikipedia.org/wiki/Right_of_reply). Hal ini tidak hanya memberi Anda platform lain untuk memublikasikan pernyataan Anda, di luar media Anda sendiri - ia juga menyediakan tempat untuk menautkan ke pernyataan lain yang Anda sebarkan.
>
> Jika media berita yang memuat informasi fitnah bukanlah sumber berita yang kredibel atau sering menjadi sumber berita yang tidak berdasar, hal ini berguna untuk ditunjukkan dalam pernyataan apa pun.
>
> Ada banyak pengecek fakta lokal, regional, dan global yang dapat membantu Anda menyanggah informasi yang beredar tentang Anda.
>
> - [DW Fact Check](https://www.dw.com/en/fact-check/t-56584214)
> - [France24 LesObservateurs](https://observers.france24.com/fr/)
> - [AFP FactCheck](https://factcheck.afp.com/)
> - [EUvsDisinfo](https://euvsdisinfo.eu/)
> - [Latam Chequea](https://chequeado.com/latamchequea)
> - [Europa fact check ](https://eufactcheck.eu/)

Apakah Anda merasa perlu respons yang lebih besar terhadap kampanye pencemaran nama baik?

 - [Ya](#campaign-team)
 - [Tidak](#damage-control)

### damage-control

> Analisis Anda tentang kampanye pencemaran nama baik akan memberikan wawasan tentang jenis tindakan yang ingin Anda ambil. Misalnya, jika pesan bot digunakan untuk melawan Anda, Anda mungkin ingin menunjukkan hal tersebut dalam pernyataan publik, atau mengemukakan motif tersembunyi dari pelaku di balik kampanye jika Anda mengetahuinya. Namun, membangun lintasan Anda sendiri dan mendapatkan bantuan dari teman dan sekutu untuk mempromosikan dan mengangkat kebenaran Anda mungkin menjadi pilihan Anda, daripada berinteraksi dan bereaksi terhadap konten yang memfitnah.

Apakah Anda pikir Anda telah mampu mengendalikan kerusakan reputasi Anda?

 - [Ya](#resolved_end)
 - [Tidak](#defamation_end)

### physical-risk_end

> Jika Anda memiliki risiko fisik, silakan hubungi organisasi yang dapat membantu Anda di bawah ini.

:[](organisations?services=physical_security)

### defamation_end

> Jika Anda masih mengalami kerusakan reputasi akibat kampanye pencemaran nama baik, silakan hubungi organisasi yang dapat membantu Anda di bawah ini.

:[](organisations?services=advocacy&services=individual_care&services=legal)

### harassment_end

> Jika Anda memerlukan bantuan untuk menghapus konten, Anda dapat menghubungi organisasi yang dapat membantu Anda di bawah ini.

:[](organisations?services=harassment)

### legal_end

> Jika Anda masih mengalami kerusakan reputasi akibat kampanye pencemaran nama baik, dan Anda memerlukan bantuan hukum, silakan hubungi organisasi di bawah ini.

:[](organisations?services=legal)

### resolved_end

Semoga panduan penyelesaian masalah ini bermanfaat. Silakan beri masukan [via surel](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

Cara penting untuk mengontrol bahaya dari rencana pencemaran nama baik adalah dengan memiliki praktik keamanan yang baik untuk menjaga keamanan akun Anda dari pelaku jahat serta memantau arus informasi yang beredar tentang Anda atau organisasi Anda. Anda mungkin ingin mempertimbangkan beberapa tip pencegahan di bawah ini.

- Petakan keberadaan daring Anda. *Self-doxing* terdiri dari mengeksplorasi kecerdasan buatan sumber terbuka pada diri sendiri untuk mencegah para aktor jahat menemukan dan menggunakan informasi tersebut untuk meniru identitas Anda.
- Atur peringatan Google. Anda bisa mendapatkan surel saat hasil baru untuk suatu topik muncul di Penelusuran Google. Misalnya, Anda bisa mendapatkan informasi tentang penyebutan nama Anda atau nama organisasi/kolektif Anda.
- Aktifkan autentikasi 2-langkah (2FA) untuk akun-akun terpenting Anda. 2FA menawarkan keamanan akun yang lebih baik dengan mengharuskan untuk menggunakan lebih dari satu metode untuk masuk ke akun Anda. Hal ini berarti bahwa meski seseorang mendapatkan kata sandi utama Anda, mereka tidak bisa mengakses akun Anda kecuali mereka juga memiliki ponsel Anda atau sarana autentikasi sekunder lainnya.
- Verifikasi profil Anda di platform jejaring sosial. Beberapa platform menawarkan fitur untuk memverifikasi identitas Anda dan menghubungkannya dengan akun Anda.
- Ambil tangkapan layar halaman web Anda seperti yang terlihat sekarang untuk digunakan sebagai bukti di masa depan. Jika situs web Anda mengizinkan bot perayap web(*crawlers*), Anda bisa menggunakan the Wayback Machine yang ditawarkan oleh archive.org. Kunjungi Arsip Internet Wayback Machine, masukkan nama situs web Anda di bidang bagian bawah "Simpan Laman Sekarang", dan klik tombol "Simpan Laman Sekarang".

#### resources

- [Serikat Ilmuwan Peduli: Cara Melawan Disinformasi: Strategi Komunikasi, Praktik Terbaik, dan Jebakan yang Harus Dihindari (dalam bahasa Inggris)](https://www.ucsusa.org/resources/how-counter-disinformation)
- [Panduan Cara Mengatasi Misogini Daring dan Disinformasi Gender, oleh NDI (dalam bahasa Inggris)](https://www.ndi.org/sites/default/files/Addressing%20Gender%20%26%20Disinformation%202%20%281%29.pdf)
- [Buku Pegangan Verifikasi Untuk Disinformasi Dan Manipulasi Media](https://datajournalism.com/read/handbook/verification-3)
- [Panduan untuk *Prebunking*: Cara menjanjikan untuk melakukan inokulasi terhadap informasi palsu (dalam bahasa Inggris)](https://firstdraftnews.org/articles/a-guide-to-prebunking-a-promising-way-to-inoculate-against-misinformation/)
- [OverZero: Berkomunikasi Selama Masa Perdebatan: Anjuran dan Larangan untuk Mengatasi Kegaduhan (dalam bahasa Inggris)](https://overzero.ghost.io/communicating-during-contentious-times-dos-and-donts-to-rise-above-the-noise/)
- [Pemetaan aktor visual (dalam bahasa Inggris)](https://holistic-security.tacticaltech.org/exercises/explore/visual-actor-mapping-part-1.html)
- [Peralatan untuk disinformasi (dalam bahasa Inggris)](https://www.interaction.org/documents/disinformation-toolkit/)
- [Buku Kerja tentang Menjinakkan Kebencian (dalam bahasa Inggris)](https://www.ushmm.org/m/pdfs/20160229-Defusing-Hate-Workbook-3.pdf)
