---
layout: page
title: Nomor telepon
author: mfc
language: id
summary: Metode kontak
date: 2018-09
permalink: /id/contact-methods/phone-number.md
parent: /id/
published: true
---

Komunikasi seluler dan telepon rumah tidak dienkripsi ke penerima Anda, sehingga konten percakapan Anda dan informasi tentang siapa yang Anda telepon dapat diakses oleh pemerintah, lembaga penegak hukum, atau pihak lain dengan peralatan teknis yang diperlukan. 
